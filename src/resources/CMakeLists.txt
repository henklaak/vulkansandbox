file( GLOB GLB_FILES *.glb)
file( GLOB WAV_FILES *.wav)
file( GLOB FONT_FILES *.ttf)

#add_custom_target(Symlink resources ALL
#    COMMENT "Symlinking resources"
#    VERBATIM
#    COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/src/resources $<TARGET_FILE_DIR:VulkanGlSandbox>/resources
#)

