#pragma once
#include <memory>
#include <string>

#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <tiny_gltf.h>

#include "../core/rendermodel.h"
#include "../core/device.h"
#include "../core/buffer.h"
#include "../core/image.h"

class GlRenderModel;
typedef std::shared_ptr<GlRenderModel> GlRenderModelPtr;

class GlRenderModel : public RenderModel
{
public:
    GlRenderModel( DevicePtr device );
    virtual ~GlRenderModel();

    void renderToCommandBuffer( VkCommandBuffer commandBuffer,
                                VkPipelineLayout pipelineLayout );

    VkDescriptorSetLayout sceneDstLayout() const
    {
        return m_sceneSetup.descriptorSetLayoutScene;
    }
    VkDescriptorSetLayout localDstLayout() const
    {
        return m_localDstLayout;
    }
    VkDescriptorSetLayout textureDstLayout() const
    {
        return m_textureDstLayout;
    }
    std::vector<VkVertexInputBindingDescription> vertexBindingDescriptions() const;
    std::vector<VkVertexInputAttributeDescription> vertexInputAttributeDescriptions() const;

    void addPrimitive( const glm::mat4 &localTransform,
                       const std::vector<RawVertex> &vertices,
                       const std::vector<uint16_t> &indices,
                       const RawTexture &albedoTexture,
                       const RawTexture &normalTexture,
                       const RawTexture &ambientOcclusionTexture,
                       const RawTexture &emissiveTexture ) override;

    void setCamera( const glm::mat4 &localTransform,
                    double yFov,
                    double aspectRatio,
                    double zNear,
                    double zFar ) override;

    void addPointLight( const glm::mat4 &localTransform,
                        const glm::vec3 &color,
                        double intensity ) override;

private:
    void initCameraLightsDst();
    void initLocalDstLayout();
    void initTextureDstLayout();

    void generateMipMaps( VkImage image,
                          VkFormat format,
                          uint32_t width,
                          uint32_t height );

    DevicePtr m_pDevice;
    VkDescriptorPool m_descriptorPool = VK_NULL_HANDLE;
    VkSampler m_defaultSampler;

    struct MVP
    {
        glm::mat4 projection = glm::mat4( 1.0 );
        glm::mat4 view = glm::mat4( 1.0 );
        glm::mat4 model = glm::mat4( 1.0 );
        glm::mat4 lightspace = glm::mat4( 1.0 );
    };

    struct PrimitiveUniform
    {
        glm::mat4 model = glm::mat4( 1.0 );
    };

    struct Light
    {
        glm::vec4 position = glm::vec4( 5.0f, 5.0f, 5.0f, 1.0f );
        glm::vec3 color = glm::vec3( 1.0f, 1.0f, 1.0f );
        float power = 0.0f;
    };

    struct Illumination
    {
        Light lights[8];
    };

    struct Scene
    {
        uint32_t nrLights = 0;

        MVP cameraMvp;
        BufferPtr pCameraMvpBuffer;

        Illumination illumination;
        BufferPtr pIlluminationBuffer;

        VkDescriptorSet descriptorSetScene = VK_NULL_HANDLE;
        VkDescriptorSetLayout descriptorSetLayoutScene = VK_NULL_HANDLE;

    } m_sceneSetup;

    VkDescriptorSetLayout m_textureDstLayout = VK_NULL_HANDLE;
    VkDescriptorSetLayout m_localDstLayout = VK_NULL_HANDLE;

    struct TextureMap
    {
        ImagePtr pImage;
    };

    struct Primitive
    {
        uint32_t nrIndices = 0;
        BufferPtr pIndexBuffer;
        BufferPtr pVertexBuffer;
        BufferPtr pUboBuffer;

        TextureMap albedoMap{};
        TextureMap normalMap{};
        TextureMap ambientOcclusionMap{};
        VkDescriptorSet texturesDescriptorSet = VK_NULL_HANDLE;

        VkDescriptorSet uboDescriptorSet = VK_NULL_HANDLE;
    };

    std::vector<Primitive> m_primitives;
};
