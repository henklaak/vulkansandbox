#pragma once
#include <vulkan/vulkan.hpp>
#include <imgui/imgui.h>
#include <glm/glm.hpp>
#include "../core/renderer.h"

#include "glrendermodel.h"
#include "offscreenpass.h"
#include "swapchainpass.h"

class GlRenderer;
typedef std::shared_ptr<GlRenderer> GlRendererPtr;

class GlRenderer : public Renderer
{
public:
    GlRenderer( DevicePtr device,
                SwapchainPtr swapchain,
                GlRenderModelPtr renderModel );
    ~GlRenderer();

    void render( uint32_t frameNr );

private:
    GlRenderModelPtr m_pRenderModel;
    OffscreenPassPtr m_pOffscreenPass;
    SwapchainPassPtr m_pSwapchainPass;

    VkSampleCountFlagBits m_msaa =  VK_SAMPLE_COUNT_2_BIT;
};
