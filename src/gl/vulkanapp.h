#pragma once
#include <memory>
#include "../core/instance.h"
#include "../core/window.h"
#include "../core/camera.h"
#include "../core/physicaldevice.h"
#include "../core/device.h"
#include "../core/surface.h"
#include "../core/swapchain.h"
#include "glrenderer.h"
#include "glrendermodel.h"

class App;
typedef std::shared_ptr<App> AppPtr;

class App
{
public:
    App();
    virtual ~App();
    void run();

private:
    bool m_exitRequested = false;

    CameraPtr m_pCamera;
    WindowPtr m_pWindow;
    InstancePtr m_pInstance;
    SurfacePtr m_pSurface;
    PhysicalDevicePtr m_pPhysicalDevice;
    DevicePtr m_pDevice;
    SwapchainPtr m_pSwapchain;

    GlRendererPtr m_pGlRenderer;
    GlRenderModelPtr m_pGlRenderModel;

    uint32_t m_currentFrame = 0;
};
