#pragma once
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>
#include "../core/device.h"
#include "../core/swapchain.h"

class SwapchainPass;
typedef std::shared_ptr<SwapchainPass> SwapchainPassPtr;

class SwapchainPass
{
public:
    SwapchainPass( DevicePtr pDevice,
                   SwapchainPtr pSwapchain,
                   const VkImageView &inputView );
    virtual ~SwapchainPass();


    void update();
    void render( VkSemaphore waitSemaphore, uint32_t currentFrame );

private:
    void initRenderPass();
    void initFrameBuffer();
    void initPipeline();
    void initCommandBuffers();
    void initFences();
    void initSemaphores();

    DevicePtr m_pDevice;
    SwapchainPtr m_pSwapchain;

    VkImageView m_inputView = VK_NULL_HANDLE;

    VkRenderPass m_renderPass  = VK_NULL_HANDLE;
    VkDescriptorSetLayout m_descriptorSetLayout = VK_NULL_HANDLE;
    VkDescriptorSet m_descriptorSet = VK_NULL_HANDLE;
    VkPipelineLayout m_pipelineLayout = VK_NULL_HANDLE;
    VkPipeline m_pipeline = VK_NULL_HANDLE;
    VkSampler m_sampler = VK_NULL_HANDLE;
    VkDescriptorPool m_descriptorPool = VK_NULL_HANDLE;

    std::vector<VkFramebuffer> m_frameBuffers;
    std::vector<VkFence> m_commandFences;
    std::vector<VkCommandBuffer> m_commandBuffers;
};
