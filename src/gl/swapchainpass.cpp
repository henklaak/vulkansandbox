#include "swapchainpass.h"
#include "../core/vulkancore.h"
#include "../core/device.h"
#include "../core/image.h"
#include "../core/swapchain.h"

/**************************************************************************************************/
SwapchainPass::SwapchainPass( DevicePtr pDevice,
                              SwapchainPtr pSwapchain,
                              const VkImageView &inputView )
    : m_pDevice( pDevice )
    , m_pSwapchain( pSwapchain )
    , m_inputView( inputView )
{
    initRenderPass();
    initFrameBuffer();
    initPipeline();
    initCommandBuffers();
    initFences();
    initSemaphores();
}

/**************************************************************************************************/
SwapchainPass::~SwapchainPass()
{
    for( size_t i = 0; i < m_pSwapchain->nrImages(); ++i )
    {
        vkDestroyFence( m_pDevice->device(), m_commandFences[i], nullptr );
    }
    vkDestroyPipelineLayout( m_pDevice->device(), m_pipelineLayout, nullptr );
    vkDestroyPipeline( m_pDevice->device(), m_pipeline, nullptr );
    vkDestroyRenderPass( m_pDevice->device(), m_renderPass, nullptr );
    vkDestroyDescriptorSetLayout( m_pDevice->device(), m_descriptorSetLayout, nullptr );
    vkDestroySampler( m_pDevice->device(), m_sampler, nullptr );
    vkDestroyDescriptorPool( m_pDevice->device(), m_descriptorPool, nullptr );
    for( size_t i = 0; i < m_pSwapchain->nrImages(); ++i )
    {
        vkDestroyFramebuffer( m_pDevice->device(), m_frameBuffers[i], nullptr );
    }
}

/**************************************************************************************************/
void SwapchainPass::initRenderPass()
{
    VkAttachmentDescription colorAttachment{};
    colorAttachment.format = m_pSwapchain->swapchainImageFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentReference colorAttachmentRef{};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpassDescription{};
    subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDescription.colorAttachmentCount = 1;
    subpassDescription.pColorAttachments = &colorAttachmentRef;
    subpassDescription.pDepthStencilAttachment = nullptr;
    subpassDescription.pResolveAttachments = nullptr;

    std::vector<VkAttachmentDescription> attachmentDescriptions =
    {
        colorAttachment,
    };

    std::array<VkSubpassDependency, 2> dependencies;

    dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[0].dstSubpass = 0;
    dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                                    VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    dependencies[1].srcSubpass = 0;
    dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                                    VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    VkRenderPassCreateInfo renderPassCI{};
    renderPassCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassCI.attachmentCount = static_cast<uint32_t>( attachmentDescriptions.size() );
    renderPassCI.pAttachments = attachmentDescriptions.data();
    renderPassCI.subpassCount = 1;
    renderPassCI.pSubpasses = &subpassDescription;
    renderPassCI.dependencyCount = dependencies.size();
    renderPassCI.pDependencies = dependencies.data();

    VK_CHECK_RESULT( vkCreateRenderPass( m_pDevice->device(),
                                         &renderPassCI,
                                         nullptr,
                                         &m_renderPass ) );
}
/**************************************************************************************************/
void SwapchainPass::initFrameBuffer()
{
    m_frameBuffers.resize( m_pSwapchain->nrImages() );

    for( size_t i = 0; i < m_pSwapchain->nrImages(); i++ )
    {
        std::vector<VkImageView> attachments =
        {
            m_pSwapchain->m_swapchainImages[i]->imageView()
        };
        VkFramebufferCreateInfo framebufferCI{};
        framebufferCI.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferCI.renderPass = m_renderPass;
        framebufferCI.attachmentCount = attachments.size();
        framebufferCI.pAttachments = attachments.data();
        framebufferCI.width = m_pSwapchain->width();
        framebufferCI.height = m_pSwapchain->height();
        framebufferCI.layers = 1;

        VK_CHECK_RESULT( vkCreateFramebuffer(
                             m_pDevice->device(),
                             &framebufferCI,
                             nullptr,
                             &m_frameBuffers[i] ) );
    }
}

/**************************************************************************************************/
void SwapchainPass::initPipeline()
{
    std::array<VkDescriptorSetLayoutBinding, 1> setLayoutBindings = {};
    setLayoutBindings[0].binding = 0;
    setLayoutBindings[0].descriptorCount = 1;
    setLayoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    setLayoutBindings[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    VkDescriptorSetLayoutCreateInfo descriptorLayoutCI{};
    descriptorLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorLayoutCI.bindingCount = setLayoutBindings.size();
    descriptorLayoutCI.pBindings = setLayoutBindings.data();

    VK_CHECK_RESULT( vkCreateDescriptorSetLayout( m_pDevice->device(),
                     &descriptorLayoutCI,
                     nullptr,
                     &m_descriptorSetLayout ) );

    // Pipeline layout
    VkPipelineLayoutCreateInfo pipelineLayoutCI{};
    pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCI.setLayoutCount = 1;
    pipelineLayoutCI.pSetLayouts = &m_descriptorSetLayout;
    pipelineLayoutCI.pushConstantRangeCount = 0;
    pipelineLayoutCI.pPushConstantRanges = nullptr;

    VK_CHECK_RESULT( vkCreatePipelineLayout(
                         m_pDevice->device(),
                         &pipelineLayoutCI,
                         nullptr,
                         &m_pipelineLayout ) );

    auto vertShaderCode = m_pDevice->readFile( "swapchain.vert.spv" );
    auto fragShaderCode = m_pDevice->readFile( "swapchain.frag.spv" );

    VkShaderModule vertShaderModule = m_pDevice->createShaderModule( vertShaderCode );
    VkShaderModule fragShaderModule = m_pDevice->createShaderModule( fragShaderCode );

    VkPipelineShaderStageCreateInfo vertShaderStageCI{};
    vertShaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageCI.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageCI.module = vertShaderModule;
    vertShaderStageCI.pName = "main";

    VkPipelineShaderStageCreateInfo fragShaderStageCI{};
    fragShaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageCI.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageCI.module = fragShaderModule;
    fragShaderStageCI.pName = "main";

    VkPipelineShaderStageCreateInfo shaderStageCI[] =
    {
        vertShaderStageCI,
        fragShaderStageCI
    };

    VkPipelineVertexInputStateCreateInfo vertexInputStateCI{};
    vertexInputStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStateCI.vertexBindingDescriptionCount = 0;
    vertexInputStateCI.vertexAttributeDescriptionCount = 0;
    vertexInputStateCI.pVertexBindingDescriptions = nullptr;
    vertexInputStateCI.pVertexAttributeDescriptions = nullptr;

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCI{};
    inputAssemblyStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssemblyStateCI.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
    inputAssemblyStateCI.primitiveRestartEnable = VK_FALSE;

    VkViewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = ( float ) m_pSwapchain->width();
    viewport.height = ( float ) m_pSwapchain->height();
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor{};
    scissor.offset = {0, 0};
    scissor.extent.width = m_pSwapchain->width();
    scissor.extent.height = m_pSwapchain->height();

    VkPipelineViewportStateCreateInfo viewportStateCI{};
    viewportStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportStateCI.viewportCount = 1;
    viewportStateCI.pViewports = &viewport;
    viewportStateCI.scissorCount = 1;
    viewportStateCI.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizationStateCI{};
    rasterizationStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizationStateCI.depthClampEnable = VK_FALSE;
    rasterizationStateCI.rasterizerDiscardEnable = VK_FALSE;
    rasterizationStateCI.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizationStateCI.lineWidth = 1.0f;
    rasterizationStateCI.cullMode = VK_CULL_MODE_NONE;
    rasterizationStateCI.frontFace = VK_FRONT_FACE_CLOCKWISE;
    rasterizationStateCI.depthBiasEnable = VK_FALSE;

    VkPipelineMultisampleStateCreateInfo multisampleStateCI{};
    multisampleStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampleStateCI.sampleShadingEnable = VK_FALSE;
    multisampleStateCI.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    std::array<VkPipelineColorBlendAttachmentState, 1> colorBlendAttachments{};
    colorBlendAttachments[0].colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachments[0].blendEnable = VK_FALSE;

    VkPipelineDepthStencilStateCreateInfo depthStencil{};
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable = VK_FALSE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.stencilTestEnable = VK_FALSE;

    std::vector<VkDynamicState> dynamicStateEnables =
    {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };
    VkPipelineDynamicStateCreateInfo dynamicStateCI{};
    dynamicStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicStateCI.dynamicStateCount = dynamicStateEnables.size();
    dynamicStateCI.pDynamicStates = dynamicStateEnables.data();

    VkPipelineColorBlendStateCreateInfo colorBlendStateCI{};
    colorBlendStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlendStateCI.logicOpEnable = VK_FALSE;
    colorBlendStateCI.logicOp = VK_LOGIC_OP_COPY;
    colorBlendStateCI.attachmentCount = colorBlendAttachments.size();
    colorBlendStateCI.pAttachments = colorBlendAttachments.data();
    colorBlendStateCI.blendConstants[0] = 0.0f;
    colorBlendStateCI.blendConstants[1] = 1.0f;
    colorBlendStateCI.blendConstants[2] = 0.0f;
    colorBlendStateCI.blendConstants[3] = 0.0f;

    VkGraphicsPipelineCreateInfo pipelineCI{};
    pipelineCI.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCI.stageCount = 2;
    pipelineCI.pStages = shaderStageCI;
    pipelineCI.pVertexInputState = &vertexInputStateCI;
    pipelineCI.pInputAssemblyState = &inputAssemblyStateCI;
    pipelineCI.pViewportState = &viewportStateCI;
    pipelineCI.pRasterizationState = &rasterizationStateCI;
    pipelineCI.pMultisampleState = &multisampleStateCI;
    pipelineCI.pDepthStencilState = &depthStencil;
    pipelineCI.pDynamicState = &dynamicStateCI;
    pipelineCI.pColorBlendState = &colorBlendStateCI;
    pipelineCI.layout = m_pipelineLayout;
    pipelineCI.renderPass = m_renderPass;
    pipelineCI.subpass = 0;
    pipelineCI.basePipelineHandle = VK_NULL_HANDLE;

    VK_CHECK_RESULT( vkCreateGraphicsPipelines(
                         m_pDevice->device(),
                         VK_NULL_HANDLE,
                         1,
                         &pipelineCI,
                         nullptr,
                         &m_pipeline ) );

    vkDestroyShaderModule( m_pDevice->device(), vertShaderModule, nullptr );
    vkDestroyShaderModule( m_pDevice->device(), fragShaderModule, nullptr );
}

/**************************************************************************************************/
void SwapchainPass::initCommandBuffers()
{
    // Create sampler to sample from the color attachments
    VkSamplerCreateInfo samplerCI{};
    samplerCI.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerCI.magFilter = VK_FILTER_NEAREST;
    samplerCI.minFilter = VK_FILTER_NEAREST;
    samplerCI.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerCI.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    samplerCI.addressModeV = samplerCI.addressModeU;
    samplerCI.addressModeW = samplerCI.addressModeU;
    samplerCI.maxAnisotropy = 1.0f;
    samplerCI.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
    VK_CHECK_RESULT( vkCreateSampler( m_pDevice->device(),
                                      &samplerCI,
                                      nullptr,
                                      &m_sampler ) );

    std::array<VkDescriptorPoolSize, 1> poolSizes{};
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[0].descriptorCount = 1;

    VkDescriptorPoolCreateInfo descriptorPoolCI{};
    descriptorPoolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCI.maxSets = 1;
    descriptorPoolCI.poolSizeCount = poolSizes.size();
    descriptorPoolCI.pPoolSizes = poolSizes.data();

    VK_CHECK_RESULT( vkCreateDescriptorPool( m_pDevice->device(),
                     &descriptorPoolCI,
                     nullptr,
                     &m_descriptorPool ) );

    VkDescriptorSetAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = m_descriptorPool;
    allocInfo.pSetLayouts = &m_descriptorSetLayout;
    allocInfo.descriptorSetCount = 1;

    VK_CHECK_RESULT( vkAllocateDescriptorSets( m_pDevice->device(),
                     &allocInfo,
                     &m_descriptorSet ) );

    // Image descriptors for the offscreen color attachments
    VkDescriptorImageInfo texDescriptorAlbedo{};
    texDescriptorAlbedo.sampler = m_sampler;
    texDescriptorAlbedo.imageView = m_inputView;
    texDescriptorAlbedo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    std::array<VkWriteDescriptorSet, 1> writeDescriptorSets{};
    writeDescriptorSets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescriptorSets[0].descriptorCount = 1;
    writeDescriptorSets[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writeDescriptorSets[0].dstBinding = 0;
    writeDescriptorSets[0].pImageInfo = &texDescriptorAlbedo;
    writeDescriptorSets[0].dstSet = m_descriptorSet;
    vkUpdateDescriptorSets( m_pDevice->device(),
                            writeDescriptorSets.size(),
                            writeDescriptorSets.data(),
                            0,
                            nullptr );

    //
    m_commandBuffers.resize( m_pSwapchain->nrImages() );

    VkCommandBufferAllocateInfo allocateInfo{};
    allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocateInfo.commandPool = m_pDevice->commandPool();
    allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocateInfo.commandBufferCount = m_pSwapchain->nrImages();

    VK_CHECK_RESULT( vkAllocateCommandBuffers(
                         m_pDevice->device(),
                         &allocateInfo,
                         m_commandBuffers.data() ) );

    for( size_t i = 0; i < m_pSwapchain->nrImages(); ++i )
    {
        VkCommandBuffer &commandBuffer = m_commandBuffers[i];

        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

        VK_CHECK_RESULT( vkBeginCommandBuffer(
                             commandBuffer,
                             &beginInfo ) );

        std::array<VkClearValue, 2> clearValues{};
        clearValues[0].color = {{0.0f, 0.0f, 0.0f, 1.0f}};
        clearValues[1].depthStencil = {1.0f, 0};

        VkRenderPassBeginInfo renderPassBeginInfo{};
        renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassBeginInfo.renderPass = m_renderPass;
        renderPassBeginInfo.framebuffer = m_frameBuffers[i];
        renderPassBeginInfo.renderArea.offset = {0, 0};
        renderPassBeginInfo.renderArea.extent.width = m_pSwapchain->width();
        renderPassBeginInfo.renderArea.extent.height = m_pSwapchain->height();
        renderPassBeginInfo.clearValueCount = clearValues.size();
        renderPassBeginInfo.pClearValues = clearValues.data();

        vkCmdBeginRenderPass( commandBuffer,
                              &renderPassBeginInfo,
                              VK_SUBPASS_CONTENTS_INLINE );

        VkViewport viewport{};
        VkRect2D scissor{};
        viewport.width = m_pSwapchain->width();
        viewport.height = m_pSwapchain->height();
        viewport.minDepth = 0.0;
        viewport.maxDepth = 1.0;
        scissor.extent.width = m_pSwapchain->width();
        scissor.extent.height = m_pSwapchain->height();
        vkCmdSetViewport( commandBuffer, 0, 1, &viewport );
        vkCmdSetScissor( commandBuffer, 0, 1, &scissor );

        vkCmdBindPipeline( commandBuffer,
                           VK_PIPELINE_BIND_POINT_GRAPHICS,
                           m_pipeline );
        vkCmdBindDescriptorSets( commandBuffer,
                                 VK_PIPELINE_BIND_POINT_GRAPHICS,
                                 m_pipelineLayout,
                                 0,
                                 1,
                                 &m_descriptorSet,
                                 0,
                                 nullptr );

        vkCmdDraw( commandBuffer, 3, 1, 0, 0 );

        //drawUI( commandBuffer );

        vkCmdEndRenderPass( commandBuffer );
        VK_CHECK_RESULT( vkEndCommandBuffer( commandBuffer ) );
    }
}

/**************************************************************************************************/
void SwapchainPass::initFences()
{
    m_commandFences.resize( m_pSwapchain->nrImages() );
    for( size_t i = 0; i < m_pSwapchain->nrImages(); ++i )
    {
        VkFenceCreateInfo fenceCI{};
        fenceCI.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fenceCI.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        vkCreateFence( m_pDevice->device(),
                       &fenceCI, nullptr,
                       &m_commandFences[i] );
    }
}
/**************************************************************************************************/
void SwapchainPass::initSemaphores() {}



/**************************************************************************************************/
void SwapchainPass::update()
{

}

/**************************************************************************************************/
void SwapchainPass::render( VkSemaphore waitSemaphore,
                            uint32_t currentFrame )
{
    // Swapchain pass
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT};
    std::array<VkSemaphore, 1> signalSemaphores{m_pSwapchain->renderFinishedSemaphore};

    VkSubmitInfo submitInfo{};
    std::array<VkCommandBuffer, 1> cmdBuffers{m_commandBuffers[currentFrame]};

    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &waitSemaphore;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &m_pSwapchain->renderFinishedSemaphore;
    submitInfo.commandBufferCount = cmdBuffers.size();
    submitInfo.pCommandBuffers = cmdBuffers.data();

    VK_CHECK_RESULT( vkWaitForFences( m_pDevice->device(), 1, &m_commandFences[currentFrame], VK_TRUE,
                                      UINT64_MAX ) );
    VK_CHECK_RESULT( vkResetFences( m_pDevice->device(), 1, &m_commandFences[currentFrame] ) );

    VK_CHECK_RESULT( vkQueueSubmit( m_pDevice->graphicsQueue(),
                                    1,
                                    &submitInfo,
                                    m_commandFences[currentFrame] ) );
}
