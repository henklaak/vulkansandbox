file(GLOB SOURCES *.cpp)
file(GLOB HEADERS *.h)
file(GLOB GL_SHADER_SOURCES *.vert *.frag)

add_executable(VulkanGlSandbox ${SOURCES} ${HEADERS} ${GL_SHADER_SOURCES})
target_link_libraries(VulkanGlSandbox PUBLIC
    VulkanCore)


set(GL_SHADER_BINS)
foreach(GL_SHADER_SOURCE ${GL_SHADER_SOURCES})
    get_filename_component(GL_SHADER_NAME ${GL_SHADER_SOURCE} NAME)
    set(GL_SHADER_BIN ${CMAKE_CURRENT_BINARY_DIR}/${GL_SHADER_NAME}.spv)

    list(APPEND GL_SHADER_BINS ${GL_SHADER_BIN})

    add_custom_command(
      OUTPUT ${GL_SHADER_BIN}
      COMMAND ${GLSLC} ${GL_SHADER_SOURCE} -V -o ${GL_SHADER_BIN} --target-env vulkan1.1
      DEPENDS ${GL_SHADER_SOURCE}
    )
endforeach()

add_custom_target(build_gl_shaders
    DEPENDS ${GL_SHADER_BINS}
)
add_dependencies(VulkanGlSandbox build_gl_shaders)

