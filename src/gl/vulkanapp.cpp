// https://docs.blender.org/manual/en/2.92/addons/import_export/scene_gltf2.html

#include "vulkanapp.h"
#include <unistd.h>
#include <glm/gtx/io.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../core/instance.h"
#include "../core/camera.h"
#include "../core/window.h"
#include "../core/device.h"
#include "../core/surface.h"
#include "../core/swapchain.h"
#include "../core/modelloader.h"
#include "../core/vrloader.h"
#include "glrenderer.h"
#include "glrendermodel.h"

std::string modelFilename = "resources/cube_textured.glb" ;

/**************************************************************************************************/
App::App()
{
    m_pCamera = std::make_shared<Camera>();
    m_pWindow = std::make_shared<Window>( m_pCamera );
    std::vector< std::string > requiredInstanceExtensions = m_pWindow->getRequiredInstanceExtensions();
    m_pInstance = std::make_shared<Instance>( requiredInstanceExtensions );
    m_pSurface = std::make_shared<Surface>( m_pInstance, m_pWindow );
    m_pPhysicalDevice = std::make_shared<PhysicalDevice>( m_pInstance, m_pSurface ) ;
    m_pDevice = std::make_shared<Device>( m_pInstance, m_pPhysicalDevice ) ;
    m_pSwapchain = std::make_shared<Swapchain>( m_pDevice, m_pSurface );

    /*********/

    m_pGlRenderModel = std::make_shared<GlRenderModel>( m_pDevice );

    ModelLoader modelLoader( modelFilename );
    modelLoader.load( m_pGlRenderModel );

    //VrLoader vrLoader;
    //vrLoader.load( m_pGlRenderModel );

    m_pGlRenderer = std::make_shared<GlRenderer>( m_pDevice, m_pSwapchain, m_pGlRenderModel ) ;
}

/**************************************************************************************************/
App::~App()
{
}

/**************************************************************************************************/
void App::run()
{
    while( !glfwWindowShouldClose( m_pWindow->window() ) && !m_exitRequested )
        //for( size_t counter = 0; counter < NR_FRAMES; ++counter )
    {
        glfwPollEvents();

        m_currentFrame = m_pSwapchain->acquireNext();
        m_pGlRenderer->render( m_currentFrame );
        m_pSwapchain->present( m_currentFrame );
    }
    vkDeviceWaitIdle( m_pDevice->device() );
}

/**************************************************************************************************/
int main( const int, const char *[] )
{
    AppPtr app = std::make_shared<App>();
    app->run();
}


/*

https://github.com/KhronosGroup/GLSL/tree/master/extensions
https://iorange.github.io/p02/TeapotAndBunny.html
https://github.com/iOrange/rtxON/blob/7b618576125ecb30a2444c278351c27a96873504/src/rtxApp.cpp
https://github.com/iOrange/rtxON/blob/740f7bdd151a6ac0dfc7b2add74fdc0194e126c0/src/shared_with_shaders.h

*/
