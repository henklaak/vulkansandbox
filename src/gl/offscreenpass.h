#pragma once

#include <memory>
#include <vector>

#include <vulkan/vulkan.h>

#include "../core/device.h"
#include "../core/image.h"
#include "../core/swapchain.h"
#include "glrendermodel.h"

class OffscreenPass;

typedef std::shared_ptr<OffscreenPass> OffscreenPassPtr;

class OffscreenPass
{
public:
    OffscreenPass( DevicePtr pDevice,
                   SwapchainPtr pSwapchain,
                   GlRenderModelPtr pRenderModel );
    virtual ~OffscreenPass();

    void update();
    VkSemaphore render( VkSemaphore waitSemaphore );
    ImagePtr  albedoResolved()
    {
        return m_albedoResolved;
    };

private:
    void initRenderPass();
    void initFrameBuffer();
    void initPipeline();
    void initCommandBuffers();
    void initFences();
    void initSemaphores();

    DevicePtr m_pDevice;
    SwapchainPtr m_pSwapchain;
    GlRenderModelPtr m_pRenderModel;

    uint32_t m_width = 16;
    uint32_t m_height = 16;
    VkSampleCountFlagBits m_msaa =  VK_SAMPLE_COUNT_2_BIT;

    ImagePtr  m_albedo;
    ImagePtr  m_depth;
    ImagePtr  m_albedoResolved;

    VkPipelineLayout m_pipelineLayout = VK_NULL_HANDLE;
    VkPipeline m_pipeline = VK_NULL_HANDLE;

    VkFramebuffer m_frameBuffer  = VK_NULL_HANDLE;
    VkRenderPass m_renderPass  = VK_NULL_HANDLE;
    VkCommandPool m_commandPool = VK_NULL_HANDLE;
    VkCommandBuffer m_commandBuffer = VK_NULL_HANDLE;
    VkFence m_commandFence = VK_NULL_HANDLE;
    VkSemaphore m_finishedSemaphore = VK_NULL_HANDLE;
};
