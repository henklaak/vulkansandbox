set(TARGET VulkanXrSandbox)

file(GLOB SOURCES *.cpp)
file(GLOB HEADERS *.h)
file(GLOB XR_SHADER_SOURCES *.vert *.frag)

add_executable(${TARGET} ${SOURCES} ${HEADERS} ${XR_SHADER_SOURCES})

target_compile_definitions(${TARGET} PUBLIC
    XR_USE_GRAPHICS_API_VULKAN
    VK_USE_PLATFORM_XCB_KHR
)

target_link_libraries(${TARGET} PUBLIC
    VulkanCore
    ${OPENXR_LIBRARIES}
    ${VULKAN_LIBRARIES}
)

set(GL_SHADER_BINS)
foreach(XR_SHADER_SOURCE ${XR_SHADER_SOURCES})
    get_filename_component(XR_SHADER_NAME ${XR_SHADER_SOURCE} NAME)
    set(XR_SHADER_BIN ${CMAKE_CURRENT_BINARY_DIR}/${XR_SHADER_NAME}.spv)

    list(APPEND XR_SHADER_BINS ${XR_SHADER_BIN})

    add_custom_command(
      OUTPUT ${XR_SHADER_BIN}
      COMMAND ${GLSLC} ${XR_SHADER_SOURCE} -V -o ${XR_SHADER_BIN} --target-env vulkan1.1
      DEPENDS ${XR_SHADER_SOURCE}
    )
endforeach()

add_custom_target(build_xr_shaders
    DEPENDS ${XR_SHADER_BINS}
)

add_dependencies(${TARGET} build_xr_shaders)

install(TARGETS ${TARGET}
    DESTINATION ${BINDIR})

install(FILES
    ${GL_SHADER_BINS}
    DESTINATION ${BINDIR})
