#version 450
#extension GL_GOOGLE_include_directive : require
#include "shader_constants.h"

layout (location = LOC_VERTEX_POSITION) in vec3 inPos;
layout (location = LOC_VERTEX_NORMAL) in vec3 inNormal;
layout (location = LOC_VERTEX_TANGENT) in vec4 inTangent;
layout (location = LOC_VERTEX_UV) in vec2 inUV;

layout (set = SET_WORLD, binding = BINDING_WORLD_TRANSFORM) uniform WorldTransforms
{
    mat4 projection;
    mat4 view;
    mat4 model;
    mat4 lightSpace;
} world;

layout (set = SET_MODEL, binding = BINDING_MODEL_TRANSFORM) uniform ModelTransforms
{
    mat4 model;
} local;

layout (location = LOC_FRAG_POSITION) out vec3 outPos;
layout (location = LOC_FRAG_NORMAL) out vec3 outNormal;
layout (location = LOC_FRAG_UV) out vec2 outUV;

const mat4 biasMat = mat4(
    0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.5, 0.5, 0.0, 1.0 );

void main() {
    vec4 tmpPos = vec4(inPos, 1.0);

    // Screen position
    gl_Position = world.projection *
                  world.view *
                  world.model *
                  local.model *
                  tmpPos;

    // World position
    outPos = vec3(world.model *
                  local.model *
                  tmpPos);

    mat3 mNormal = transpose(inverse(mat3(world.model * local.model)));

    outNormal = mNormal * normalize(inNormal);

    outUV = inUV;
}
