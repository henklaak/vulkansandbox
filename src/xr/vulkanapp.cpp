#include "vulkanapp.h"
#include <iostream>
#include "xr.h"

// https://docs.blender.org/manual/en/2.92/addons/import_export/scene_gltf2.html

//#include <unistd.h>
//#include <glm/gtx/io.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>

//#include "../core/instance.h"
//#include "../core/camera.h"
//#include "../core/window.h"
//#include "../core/device.h"
//#include "../core/swapchain.h"
//#include "../core/modelloader.h"
//#include "../core/vrloader.h"
//#include "glrenderer.h"
//#include "glrendermodel.h"

//std::string modelFilename = "cube_textured.glb" ;


/**************************************************************************************************/
App::App()
{
    m_pXr.reset( new Xr );
//    m_pInstance.reset( new Instance );
//    m_pCamera.reset( new Camera );
//    m_pWindow.reset( new Window( m_pInstance, m_pCamera ) );
//    m_pPhysicalDevice.reset( new PhysicalDevice( m_pInstance,
//                             m_pWindow ) );
//    m_pDevice.reset( new Device( m_pInstance,
//                                     m_pPhysicalDevice ) );
//    m_pSwapchain.reset( new Swapchain( m_pDevice,
//                                           m_pWindow ) );

//    /*********/

//    m_pGlRenderModel.reset( new GlRenderModel( m_pDevice ) );

//    ModelLoader modelLoader( modelFilename );
//    modelLoader.load( m_pGlRenderModel );

//    VrLoader vrLoader;
//    vrLoader.load( m_pGlRenderModel );

//    m_pGlRenderer.reset(
//        new GlRenderer( m_pDevice,
//                            m_pSwapchain,
//                            m_pGlRenderModel ) );
}

/**************************************************************************************************/
App::~App()
{
//    vkDeviceWaitIdle( m_pDevice->device() );
}

/**************************************************************************************************/
void App::run()
{
    m_pXr->run();
//    while( !glfwWindowShouldClose( m_pWindow->window() ) && !m_exitRequested )
//        //for( size_t counter = 0; counter < NR_FRAMES; ++counter )
//    {
//        glfwPollEvents();

//        m_currentFrame = m_pSwapchain->acquireNext();
//        m_pGlRenderer->render( m_currentFrame );
//        m_pSwapchain->present( m_currentFrame );
//    }
}

/**************************************************************************************************/
int main( const int, const char *[] )
{
    App app;
    app.run();
    std::cout << "Done" << std::endl;
}


/*

https://github.com/KhronosGroup/GLSL/tree/master/extensions
https://iorange.github.io/p02/TeapotAndBunny.html
https://github.com/iOrange/rtxON/blob/7b618576125ecb30a2444c278351c27a96873504/src/rtxApp.cpp
https://github.com/iOrange/rtxON/blob/740f7bdd151a6ac0dfc7b2add74fdc0194e126c0/src/shared_with_shaders.h

*/
