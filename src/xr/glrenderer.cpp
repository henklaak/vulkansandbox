#include "glrenderer.h"
#include "../core/vulkancore.h"
#include "../core/device.h"
#include "../core/buffer.h"
#include "../core/image.h"
#include "../core/swapchain.h"
#include "../core/renderer.h"
#include "glrendermodel.h"
#include "offscreenpass.h"
#include "swapchainpass.h"

/**************************************************************************************************/
GlRenderer::GlRenderer( std::shared_ptr<Device> pDevice,
                                std::shared_ptr<Swapchain> pSwapchain,
                                std::shared_ptr<GlRenderModel> pRenderModel )
    : Renderer( pDevice, pSwapchain )
    , m_pRenderModel( pRenderModel )
{
    m_msaa = pDevice->getMaxMsaa();

    m_pOffscreenPass.reset( new OffscreenPass(
                                m_pDevice,
                                m_pSwapchain,
                                m_pRenderModel ) );

    m_pSwapchainPass.reset( new SwapchainPass(
                                m_pDevice,
                                m_pSwapchain,
                                m_pOffscreenPass->albedoResolved()->view ) );
}

/**************************************************************************************************/
GlRenderer::~GlRenderer()
{
}

/**************************************************************************************************/
void GlRenderer::render( uint32_t currentFrame )
{
    VkSemaphore offscreenFinishedSemaphore;

    m_pOffscreenPass->update();
    offscreenFinishedSemaphore = m_pOffscreenPass->render( m_pSwapchain->imageAvailableSemaphore );

    m_pSwapchainPass->update();
    m_pSwapchainPass->render( offscreenFinishedSemaphore, currentFrame );
}
