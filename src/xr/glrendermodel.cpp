#include "glrendermodel.h"
#include <chrono>
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/io.hpp>
#include "../core/vulkancore.h"
#include "../core/device.h"

/**************************************************************************************************/
GlRenderModel::GlRenderModel( std::shared_ptr<Device> device )
    : m_pDevice( device )
{
    const int MAX_NR_PRIMITIVES = 20;

    std::array<VkDescriptorPoolSize, 2> poolSizes{};
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount = MAX_NR_PRIMITIVES;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[1].descriptorCount = 2 * MAX_NR_PRIMITIVES;

    VkDescriptorPoolCreateInfo descriptorPoolCI{};
    descriptorPoolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCI.maxSets = 3 * MAX_NR_PRIMITIVES;
    descriptorPoolCI.poolSizeCount = poolSizes.size();
    descriptorPoolCI.pPoolSizes = poolSizes.data();

    VK_CHECK_RESULT( vkCreateDescriptorPool( m_pDevice->device(),
                     &descriptorPoolCI,
                     nullptr,
                     &m_descriptorPool ) );

    VkSamplerCreateInfo samplerInfo{};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_FALSE; // TODO
    samplerInfo.maxAnisotropy = 16.0;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;

    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 11.0;
    samplerInfo.mipLodBias = 0.0f;

    if( vkCreateSampler( m_pDevice->device(), &samplerInfo, nullptr, &m_defaultSampler ) != VK_SUCCESS )
    {
        throw std::runtime_error( "failed to create texture sampler!" );
    }

    initCameraLightsDst();
    initLocalDstLayout();
    initTextureDstLayout();
}

/**************************************************************************************************/
GlRenderModel::~GlRenderModel()
{
    vkDestroyDescriptorSetLayout( m_pDevice->device(), m_localDstLayout, nullptr );
    vkDestroyDescriptorSetLayout( m_pDevice->device(), m_textureDstLayout, nullptr );

    vkDestroyDescriptorSetLayout( m_pDevice->device(),
                                  m_sceneSetup.descriptorSetLayoutScene,
                                  nullptr );

    vkDestroySampler( m_pDevice->device(), m_defaultSampler, nullptr );
    vkDestroyDescriptorPool( m_pDevice->device(), m_descriptorPool, nullptr );
}


/**************************************************************************************************/
void GlRenderModel::initCameraLightsDst()
{
    VkDescriptorSetLayoutBinding  dstCameraMvpBinding{};
    dstCameraMvpBinding.binding = 0;
    dstCameraMvpBinding.descriptorCount = 1;
    dstCameraMvpBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    dstCameraMvpBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkDescriptorSetLayoutBinding  dstIlluminationBinding{};
    dstIlluminationBinding.binding = 1;
    dstIlluminationBinding.descriptorCount = 1;
    dstIlluminationBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    dstIlluminationBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    std::array<VkDescriptorSetLayoutBinding, 2> dstLayoutBinding =
    {
        dstCameraMvpBinding,
        dstIlluminationBinding,
    };

    VkDescriptorSetLayoutCreateInfo dstLayoutCI{};
    dstLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    dstLayoutCI.bindingCount = dstLayoutBinding.size();
    dstLayoutCI.pBindings = dstLayoutBinding.data();

    VK_CHECK_RESULT(
        vkCreateDescriptorSetLayout( m_pDevice->device(),
                                     &dstLayoutCI,
                                     nullptr,
                                     &m_sceneSetup.descriptorSetLayoutScene ) );

    VkDescriptorSetAllocateInfo descriptorSetAI{};
    descriptorSetAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAI.descriptorPool = m_descriptorPool;
    descriptorSetAI.descriptorSetCount = 1;
    descriptorSetAI.pSetLayouts = &m_sceneSetup.descriptorSetLayoutScene;

    VK_CHECK_RESULT(
        vkAllocateDescriptorSets( m_pDevice->device(),
                                  &descriptorSetAI,
                                  &m_sceneSetup.descriptorSetScene ) );

    /////

    m_sceneSetup.pCameraMvpBuffer = Buffer::create(
                                        m_pDevice,
                                        nullptr,
                                        sizeof( MVP ),
                                        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                                        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

    m_sceneSetup.pIlluminationBuffer = Buffer::create(
                                           m_pDevice,
                                           nullptr,
                                           sizeof( Illumination ),
                                           VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                           VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

    std::array<VkWriteDescriptorSet, 2> descriptorWrites{};

    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = m_sceneSetup.descriptorSetScene;
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].dstArrayElement = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &m_sceneSetup.pCameraMvpBuffer->descriptor;

    descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[1].dstSet = m_sceneSetup.descriptorSetScene;
    descriptorWrites[1].dstBinding = 1;
    descriptorWrites[1].dstArrayElement = 0;
    descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[1].descriptorCount = 1;
    descriptorWrites[1].pBufferInfo = &m_sceneSetup.pIlluminationBuffer->descriptor;

    vkUpdateDescriptorSets( m_pDevice->device(),
                            descriptorWrites.size(),
                            descriptorWrites.data(),
                            0,
                            nullptr );
}

/**************************************************************************************************/
void GlRenderModel::initTextureDstLayout()
{
    VkDescriptorSetLayoutBinding  dstLayoutBinding1{};
    dstLayoutBinding1.binding = 0;
    dstLayoutBinding1.descriptorCount = 1;
    dstLayoutBinding1.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    dstLayoutBinding1.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    VkDescriptorSetLayoutBinding  dstLayoutBinding2{};
    dstLayoutBinding2.binding = 1;
    dstLayoutBinding2.descriptorCount = 1;
    dstLayoutBinding2.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    dstLayoutBinding2.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    VkDescriptorSetLayoutBinding  dstLayoutBinding3{};
    dstLayoutBinding3.binding = 2;
    dstLayoutBinding3.descriptorCount = 1;
    dstLayoutBinding3.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    dstLayoutBinding3.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    std::array<VkDescriptorSetLayoutBinding, 3> dstLayoutBindings =
    {
        dstLayoutBinding1,
        dstLayoutBinding2,
        dstLayoutBinding3
    };
    VkDescriptorSetLayoutCreateInfo dstLayoutCI{};
    dstLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    dstLayoutCI.bindingCount = dstLayoutBindings.size();
    dstLayoutCI.pBindings = dstLayoutBindings.data();

    VK_CHECK_RESULT(
        vkCreateDescriptorSetLayout( m_pDevice->device(),
                                     &dstLayoutCI,
                                     nullptr,
                                     &m_textureDstLayout ) );
}

/**************************************************************************************************/
void GlRenderModel::initLocalDstLayout()
{
    VkDescriptorSetLayoutBinding  dstLayoutBinding{};
    dstLayoutBinding.binding = 0;
    dstLayoutBinding.descriptorCount = 1;
    dstLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    dstLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkDescriptorSetLayoutCreateInfo dstLayoutCI{};
    dstLayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    dstLayoutCI.bindingCount = 1;
    dstLayoutCI.pBindings = &dstLayoutBinding;

    VK_CHECK_RESULT(
        vkCreateDescriptorSetLayout( m_pDevice->device(),
                                     &dstLayoutCI,
                                     nullptr,
                                     &m_localDstLayout ) );
}

/**************************************************************************************************/
std::vector<VkVertexInputBindingDescription> GlRenderModel::vertexBindingDescriptions()
const
{
    std::vector<VkVertexInputBindingDescription> descs;

    VkVertexInputBindingDescription desc;
    desc.binding = 0;
    desc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    desc.stride = sizeof( RawVertex );

    descs.push_back( desc );
    return descs;
}

/**************************************************************************************************/
std::vector<VkVertexInputAttributeDescription>
GlRenderModel::vertexInputAttributeDescriptions() const
{
    std::vector<VkVertexInputAttributeDescription> descs;

    VkVertexInputAttributeDescription desc;
    desc.binding = 0;
    desc.format =  VK_FORMAT_R32G32B32_SFLOAT; //vec3
    desc.location = 0;
    desc.offset = offsetof( RawVertex, position );
    descs.push_back( desc );

    desc.binding = 0;
    desc.format =  VK_FORMAT_R32G32B32_SFLOAT; //vec3
    desc.location = 1;
    desc.offset = offsetof( RawVertex, normal );
    descs.push_back( desc );

    desc.binding = 0;
    desc.format =  VK_FORMAT_R32G32B32A32_SFLOAT;  //vec
    desc.location = 2;
    desc.offset = offsetof( RawVertex, tangent );
    descs.push_back( desc );

    desc.binding = 0;
    desc.format =  VK_FORMAT_R32G32_SFLOAT;  //vec2
    desc.location = 3;
    desc.offset = offsetof( RawVertex, uv );
    descs.push_back( desc );

    return descs;
}

/**************************************************************************************************/
void GlRenderModel::renderToCommandBuffer(
    VkCommandBuffer commandBuffer,
    VkPipelineLayout pipelineLayout )
{
    vkCmdBindDescriptorSets( commandBuffer,
                             VK_PIPELINE_BIND_POINT_GRAPHICS,
                             pipelineLayout,
                             0,
                             1,
                             &m_sceneSetup.descriptorSetScene,
                             0,
                             nullptr );

    for( auto &primitive : m_primitives )
    {
        vkCmdBindIndexBuffer( commandBuffer,
                              primitive.pIndexBuffer->buffer,
                              0,
                              VK_INDEX_TYPE_UINT16 );

        VkDeviceSize offsets[1] = {};
        vkCmdBindVertexBuffers( commandBuffer,
                                0,
                                1,
                                &( primitive.pVertexBuffer->buffer ),
                                offsets );

        vkCmdBindDescriptorSets( commandBuffer,
                                 VK_PIPELINE_BIND_POINT_GRAPHICS,
                                 pipelineLayout,
                                 1,
                                 1,
                                 &primitive.uboDescriptorSet,
                                 0,
                                 nullptr );
        vkCmdBindDescriptorSets( commandBuffer,
                                 VK_PIPELINE_BIND_POINT_GRAPHICS,
                                 pipelineLayout,
                                 2,
                                 1,
                                 &primitive.texturesDescriptorSet,
                                 0,
                                 nullptr );

        vkCmdDrawIndexed( commandBuffer, primitive.nrIndices, 1, 0, 0, 0 );
    }
}

/**************************************************************************************************/
void GlRenderModel::addPrimitive( const glm::mat4 &localTransform,
                                      const std::vector<RawVertex> &vertices,
                                      const std::vector<uint16_t> &indices,
                                      const RawTexture &albedoTexture,
                                      const RawTexture &normalTexture,
                                      const RawTexture &ambientOcclusionTexture,
                                      const RawTexture &emissiveTexture )
{
    // TODO other textures than albedo

    Primitive primitive{};

    VkDescriptorSetAllocateInfo descriptorSetAI{};
    descriptorSetAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAI.descriptorPool = m_descriptorPool;
    descriptorSetAI.descriptorSetCount = 1;
    descriptorSetAI.pSetLayouts = &m_localDstLayout;

    VK_CHECK_RESULT(
        vkAllocateDescriptorSets( m_pDevice->device(),
                                  &descriptorSetAI,
                                  &primitive.uboDescriptorSet ) );

    PrimitiveUniform localUniform{};
    localUniform.model = localTransform;

    primitive.pUboBuffer = Buffer::create(
                               m_pDevice,
                               ( uint8_t * )( &localTransform ),
                               sizeof( PrimitiveUniform ),
                               VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );


    VkWriteDescriptorSet writeDst{};
    writeDst.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDst.dstSet = primitive.uboDescriptorSet;
    writeDst.dstBinding = 0;
    writeDst.descriptorCount = 1;
    writeDst.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writeDst.pBufferInfo = &primitive.pUboBuffer->descriptor;
    vkUpdateDescriptorSets( m_pDevice->device(), 1, &writeDst, 0, nullptr );

    primitive.nrIndices = indices.size();

    primitive.pVertexBuffer = Buffer::create(
                                  m_pDevice,
                                  ( uint8_t * )( vertices.data() ),
                                  vertices.size() * sizeof( RawVertex ),
                                  VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                                  VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

    primitive.pIndexBuffer = Buffer::create(
                                 m_pDevice,
                                 ( uint8_t * )( indices.data() ),
                                 indices.size() * sizeof( uint16_t ),
                                 VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

    VkDescriptorSetAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = m_descriptorPool;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = &m_textureDstLayout;

    VK_CHECK_RESULT( vkAllocateDescriptorSets( m_pDevice->device(), &allocInfo,
                     &primitive.texturesDescriptorSet ) );

    // albedo
    {
        const uint32_t numLayers = 1;
        const uint32_t numMipLevels = std::floor( std::log2( std::max(
                                          albedoTexture.nrcols,
                                          albedoTexture.nrrows ) ) ) + 1;

        std::shared_ptr<Buffer> pStagingBuffer = Buffer::create(
                    m_pDevice,
                    albedoTexture.rawData,
                    albedoTexture.nrcols * albedoTexture.nrrows * 4,
                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

        primitive.albedoMap.pImage = Image::create(
                                         m_pDevice,
                                         albedoTexture.nrcols,
                                         albedoTexture.nrrows,
                                         numLayers,
                                         numMipLevels,
                                         VK_SAMPLE_COUNT_1_BIT,
                                         VK_FORMAT_R8G8B8A8_SRGB,
                                         VK_IMAGE_TILING_OPTIMAL,
                                         VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                                         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                         VK_IMAGE_ASPECT_COLOR_BIT );

        primitive.albedoMap.pImage->fillFromBuffer( pStagingBuffer->buffer );
        primitive.albedoMap.pImage->generateMipMaps( numMipLevels );

        VkDescriptorImageInfo imageInfo{};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = primitive.albedoMap.pImage->view;
        imageInfo.sampler = m_defaultSampler;

        VkWriteDescriptorSet writeDst{};
        writeDst.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDst.dstSet = primitive.texturesDescriptorSet;
        writeDst.dstBinding = 0;
        writeDst.descriptorCount = 1;
        writeDst.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writeDst.pImageInfo = &imageInfo;

        vkUpdateDescriptorSets( m_pDevice->device(), 1, &writeDst, 0, nullptr );
    }
    // normal
    {
        const uint32_t numLayers = 1;
        const uint32_t numMipLevels = std::floor( std::log2( std::max(
                                          normalTexture.nrcols,
                                          normalTexture.nrrows ) ) ) + 1;

        std::shared_ptr<Buffer> pStagingBuffer = Buffer::create(
                    m_pDevice,
                    normalTexture.rawData,
                    normalTexture.nrcols * normalTexture.nrrows * 4,
                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

        primitive.normalMap.pImage = Image::create(
                                         m_pDevice,
                                         normalTexture.nrcols,
                                         normalTexture.nrrows,
                                         numLayers,
                                         numMipLevels,
                                         VK_SAMPLE_COUNT_1_BIT,
                                         VK_FORMAT_R8G8B8A8_UNORM,
                                         VK_IMAGE_TILING_OPTIMAL,
                                         VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                                         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                         VK_IMAGE_ASPECT_COLOR_BIT );

        primitive.normalMap.pImage->fillFromBuffer( pStagingBuffer->buffer );
        primitive.normalMap.pImage->generateMipMaps( numMipLevels );

        VkDescriptorImageInfo imageInfo{};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = primitive.normalMap.pImage->view;
        imageInfo.sampler = m_defaultSampler;

        VkWriteDescriptorSet writeDst{};
        writeDst.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDst.dstSet = primitive.texturesDescriptorSet;
        writeDst.dstBinding = 2;
        writeDst.descriptorCount = 1;
        writeDst.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writeDst.pImageInfo = &imageInfo;

        vkUpdateDescriptorSets( m_pDevice->device(), 1, &writeDst, 0, nullptr );
    }
    // ambient occlusion
    {

        const uint32_t numLayers = 1;
        const uint32_t numMipLevels = std::floor( std::log2( std::max(
                                          ambientOcclusionTexture.nrcols,
                                          ambientOcclusionTexture.nrrows ) ) ) + 1;

        std::shared_ptr<Buffer> pStagingBuffer = Buffer::create(
                    m_pDevice,
                    ambientOcclusionTexture.rawData,
                    ambientOcclusionTexture.nrcols * ambientOcclusionTexture.nrrows * 4,
                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

        primitive.ambientOcclusionMap.pImage = Image::create(
                m_pDevice,
                ambientOcclusionTexture.nrcols,
                ambientOcclusionTexture.nrrows,
                numLayers,
                numMipLevels,
                VK_SAMPLE_COUNT_1_BIT,
                VK_FORMAT_R8G8B8A8_UNORM,
                VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                VK_IMAGE_ASPECT_COLOR_BIT );
        primitive.ambientOcclusionMap.pImage->fillFromBuffer( pStagingBuffer->buffer );
        primitive.ambientOcclusionMap.pImage->generateMipMaps( numMipLevels );

        VkDescriptorImageInfo imageInfo{};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = primitive.ambientOcclusionMap.pImage->view;
        imageInfo.sampler = m_defaultSampler;

        VkWriteDescriptorSet writeDst{};
        writeDst.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDst.dstSet = primitive.texturesDescriptorSet;
        writeDst.dstBinding = 1;
        writeDst.descriptorCount = 1;
        writeDst.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        writeDst.pImageInfo = &imageInfo;

        vkUpdateDescriptorSets( m_pDevice->device(), 1, &writeDst, 0, nullptr );
    }

    m_primitives.push_back( primitive );
}

/**************************************************************************************************/
void GlRenderModel::setCamera( const glm::mat4 &localTransform,
                                   double yFov,
                                   double aspectRatio,
                                   double zNear,
                                   double zFar )
{
    // Use this before cmdBuffer building
    m_sceneSetup.cameraMvp.model = glm::mat4( 1.0f ); // scene transform
    m_sceneSetup.cameraMvp.view = glm::inverse( localTransform );

    m_sceneSetup.cameraMvp.projection = glm::perspective(
                                            yFov,
                                            aspectRatio,
                                            zNear,
                                            zFar );
    m_sceneSetup.cameraMvp.projection[1][1] *= -1;


    void* data = nullptr;

    data = m_sceneSetup.pCameraMvpBuffer->map();
    memcpy( data, &m_sceneSetup.cameraMvp, sizeof( MVP ) );
    m_sceneSetup.pCameraMvpBuffer->unmap();
}

/**************************************************************************************************/
void GlRenderModel::addPointLight( const glm::mat4 &localTransform,
                                       const glm::vec3 &color,
                                       double intensity )
{
    // Use this before cmdBuffer building
    uint32_t newLightNr = m_sceneSetup.nrLights;
    assertmsg( newLightNr < 8, "Only <=8 point lights allowed" );

    auto &light = m_sceneSetup.illumination.lights[newLightNr];
    light.position = localTransform[3];
    light.color = color;
    light.power = intensity / 16.0;

    void* data = nullptr;
    data = m_sceneSetup.pIlluminationBuffer->map();
    memcpy( data, &m_sceneSetup.illumination, sizeof( m_sceneSetup.illumination ) );
    m_sceneSetup.pIlluminationBuffer->unmap();

    m_sceneSetup.nrLights = newLightNr + 1;
}
