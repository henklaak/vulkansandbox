#include "xr.h"
#include <vector>
#include <sstream>

/**************************************************************************************************/
XRAPI_ATTR XrBool32 VKAPI_CALL debugUtilsMessengerCallback(
    XrDebugUtilsMessageSeverityFlagsEXT messageSeverity,
    XrDebugUtilsMessageTypeFlagsEXT messageType,
    const XrDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* )
{
    // Select prefix depending on flags passed to the callback
    std::string prefix( "" );

    if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT )
    {
        prefix = "VERBOSE: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT )
    {
        prefix = "INFO: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT )
    {
        prefix = "WARNING: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT )
    {
        prefix = "ERROR: ";
    }

    // Display message to default output (console/logcat)
    std::stringstream debugMessage;
    debugMessage << "-----------------------------------------" << std::endl
                 << prefix << std::endl
                 << pCallbackData->messageId << std::endl
                 << pCallbackData->message << std::endl
                 << pCallbackData->functionName;

    if( messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT )
    {
        std::cerr << debugMessage.str() << "\n";
    }
    else
    {
        std::cout << debugMessage.str() << "\n";
    }
    fflush( stdout );

    // The return value of this callback controls whether the Vulkan call that caused the validation message will be aborted or not
    // We return VK_FALSE as we DON'T want Vulkan calls that cause a validation message to abort
    // If you instead want to have calls abort, pass in VK_TRUE and the function will return VK_ERROR_VALIDATION_FAILED_EXT

    return VK_FALSE;
}

/**************************************************************************************************/
VKAPI_ATTR VkBool32 VKAPI_CALL vkDebugUtilsMessengerCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* )
{
    // Select prefix depending on flags passed to the callback
    std::string prefix( "" );

    if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT )
    {
        prefix = "VERBOSE: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT )
    {
        prefix = "INFO: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT )
    {
        prefix = "WARNING: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT )
    {
        prefix = "ERROR: ";
    }


    // Display message to default output (console/logcat)
    std::stringstream debugMessage;
    debugMessage << "-----------------------------------------" << std::endl
                 << prefix << std::endl
                 << pCallbackData->messageIdNumber << std::endl
                 << pCallbackData->pMessageIdName << std::endl
                 << pCallbackData->pMessage;

    if( messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT )
    {
        std::cerr << debugMessage.str() << "\n";
    }
    else
    {
        std::cout << debugMessage.str() << "\n";
    }
    fflush( stdout );

    // The return value of this callback controls whether the Vulkan call that caused the validation message will be aborted or not
    // We return VK_FALSE as we DON'T want Vulkan calls that cause a validation message to abort
    // If you instead want to have calls abort, pass in VK_TRUE and the function will return VK_ERROR_VALIDATION_FAILED_EXT

    return VK_FALSE;
}

/**************************************************************************************************/
Xr::Xr()
{
    XrApplicationInfo application_info{"BasicXrApp", 1, "nullptr", 1, XR_CURRENT_API_VERSION};

    std::vector<char *>apilayers;
    apilayers.push_back( const_cast<char*>( "XR_APILAYER_LUNARG_core_validation" ) );

    std::vector<char *>xr_extensions;
    xr_extensions.push_back( const_cast<char *>( XR_KHR_VULKAN_ENABLE2_EXTENSION_NAME ) );
    xr_extensions.push_back( const_cast<char *>( XR_KHR_COMPOSITION_LAYER_DEPTH_EXTENSION_NAME ) );
    xr_extensions.push_back( const_cast<char *>( XR_EXT_DEBUG_UTILS_EXTENSION_NAME ) );

    XrInstanceCreateInfo instance_create_info{};
    instance_create_info.type = XR_TYPE_INSTANCE_CREATE_INFO;
    instance_create_info.applicationInfo = application_info;
    instance_create_info.enabledApiLayerCount = uint32_t( apilayers.size() );
    instance_create_info.enabledApiLayerNames = apilayers.data();
    instance_create_info.enabledExtensionCount = uint32_t( xr_extensions.size() );
    instance_create_info.enabledExtensionNames = xr_extensions.data();

    XR_CHECK_RESULT( xrCreateInstance( &instance_create_info, &xr_instance ) );

    /********************************/

    XR_CHECK_RESULT( xrGetInstanceProcAddr( xr_instance, "xrCreateDebugUtilsMessengerEXT",
                                            reinterpret_cast<PFN_xrVoidFunction*>( &xrCreateDebugUtilsMessengerEXT ) ) );
    XR_CHECK_RESULT( xrGetInstanceProcAddr( xr_instance, "xrDestroyDebugUtilsMessengerEXT",
                                            reinterpret_cast<PFN_xrVoidFunction*>( &xrDestroyDebugUtilsMessengerEXT ) ) );

    XrDebugUtilsMessengerCreateInfoEXT debugUtilsMessengerCI{};
    debugUtilsMessengerCI.type = XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debugUtilsMessengerCI.messageSeverities =
        XR_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
        XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
        XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
        XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debugUtilsMessengerCI.messageTypes =
        XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
        XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT |
        XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
        XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT;
    debugUtilsMessengerCI.userCallback = debugUtilsMessengerCallback;

    XR_CHECK_RESULT( xrCreateDebugUtilsMessengerEXT( xr_instance, &debugUtilsMessengerCI, &xrDebugUtilsMessenger ) );

    /********************************/

    XrSystemGetInfo system_get_info{};
    system_get_info.type = XR_TYPE_SYSTEM_GET_INFO;
    system_get_info.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;
    XR_CHECK_RESULT( xrGetSystem( xr_instance, &system_get_info, &xr_system_id ) );

    XrInstanceProperties instance_properties{.type = XR_TYPE_INSTANCE_PROPERTIES};
    XR_CHECK_RESULT( xrGetInstanceProperties( xr_instance, &instance_properties ) );

    XrSystemProperties system_properties{.type = XR_TYPE_SYSTEM_PROPERTIES};
    XR_CHECK_RESULT( xrGetSystemProperties( xr_instance, xr_system_id, &system_properties ) );

    /********************************/
    PFN_xrGetVulkanGraphicsRequirements2KHR xrGetVulkanGraphicsRequirements2KHR = nullptr;
    XR_CHECK_RESULT( xrGetInstanceProcAddr( xr_instance, "xrGetVulkanGraphicsRequirements2KHR",
                                            reinterpret_cast<PFN_xrVoidFunction*>( &xrGetVulkanGraphicsRequirements2KHR ) ) );

    XrGraphicsRequirementsVulkanKHR graphics_requirements{.type = XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR};
    XR_CHECK_RESULT( xrGetVulkanGraphicsRequirements2KHR( xr_instance, xr_system_id, &graphics_requirements ) );

    /********************************/
    PFN_xrCreateVulkanInstanceKHR xrCreateVulkanInstanceKHR = nullptr;
    XR_CHECK_RESULT( xrGetInstanceProcAddr( xr_instance, "xrCreateVulkanInstanceKHR",
                                            reinterpret_cast<PFN_xrVoidFunction*>( &xrCreateVulkanInstanceKHR ) ) );

    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "hello_xr";
    appInfo.applicationVersion = 1;
    appInfo.pEngineName = "hello_xr";
    appInfo.engineVersion = 1;
    appInfo.apiVersion = VK_API_VERSION_1_0;

    std::vector<const char*> layers;
    layers.push_back( "VK_LAYER_KHRONOS_validation" );

    std::vector<const char*> extensions;
    extensions.push_back( VK_EXT_DEBUG_UTILS_EXTENSION_NAME );
    extensions.push_back( VK_KHR_SURFACE_EXTENSION_NAME );
    extensions.push_back( VK_KHR_XCB_SURFACE_EXTENSION_NAME );

    VkInstanceCreateInfo instInfo{};
    instInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instInfo.pApplicationInfo = &appInfo;
    instInfo.enabledLayerCount = ( uint32_t )layers.size();
    instInfo.ppEnabledLayerNames = layers.empty() ? nullptr : layers.data();
    instInfo.enabledExtensionCount = ( uint32_t )extensions.size();
    instInfo.ppEnabledExtensionNames = extensions.empty() ? nullptr : extensions.data();


    XrVulkanInstanceCreateInfoKHR createInfo{};
    createInfo.type = XR_TYPE_VULKAN_INSTANCE_CREATE_INFO_KHR;
    createInfo.systemId = xr_system_id;
    createInfo.pfnGetInstanceProcAddr = &vkGetInstanceProcAddr;
    createInfo.vulkanCreateInfo = &instInfo;

    VkResult err = VK_SUCCESS;
    XR_CHECK_RESULT( xrCreateVulkanInstanceKHR( xr_instance, &createInfo, &( vk_instance ), &err ) );
    VK_CHECK_RESULT( err );

    vkCreateDebugUtilsMessengerEXT =
        reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(
            vkGetInstanceProcAddr( vk_instance, "vkCreateDebugUtilsMessengerEXT" ) );
    vkDestroyDebugUtilsMessengerEXT =
        reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(
            vkGetInstanceProcAddr( vk_instance, "vkDestroyDebugUtilsMessengerEXT" ) );

    VkDebugUtilsMessengerCreateInfoEXT vkdebugUtilsMessengerCI{};
    vkdebugUtilsMessengerCI.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    vkdebugUtilsMessengerCI.messageSeverity =
        //VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    vkdebugUtilsMessengerCI.messageType =
        VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
    vkdebugUtilsMessengerCI.pfnUserCallback = vkDebugUtilsMessengerCallback;
    VK_CHECK_RESULT( vkCreateDebugUtilsMessengerEXT(
                         vk_instance,
                         &vkdebugUtilsMessengerCI,
                         nullptr,
                         &vkDebugUtilsMessenger ) );

    //////////////
    PFN_xrGetVulkanGraphicsDevice2KHR xrGetVulkanGraphicsDevice2KHR = nullptr;
    XR_CHECK_RESULT( xrGetInstanceProcAddr( xr_instance, "xrGetVulkanGraphicsDevice2KHR",
                                            reinterpret_cast<PFN_xrVoidFunction*>( &xrGetVulkanGraphicsDevice2KHR ) ) );

    XrVulkanGraphicsDeviceGetInfoKHR deviceGetInfo{};
    deviceGetInfo.type = XR_TYPE_VULKAN_GRAPHICS_DEVICE_GET_INFO_KHR;
    deviceGetInfo.systemId = xr_system_id;
    deviceGetInfo.vulkanInstance = vk_instance;

    XR_CHECK_RESULT( xrGetVulkanGraphicsDevice2KHR( xr_instance, &deviceGetInfo,
                     &vk_physical_device ) );


    /////////
    float queuePriorities = 0;
    VkDeviceQueueCreateInfo queueInfo{};
    queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueInfo.queueCount = 1;
    queueInfo.pQueuePriorities = &queuePriorities;

    std::vector<const char*> deviceExtensions;
    VkPhysicalDeviceFeatures features{};

    VkDeviceCreateInfo deviceInfo{};
    deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceInfo.queueCreateInfoCount = 1;
    deviceInfo.pQueueCreateInfos = &queueInfo;
    deviceInfo.enabledLayerCount = 0;
    deviceInfo.ppEnabledLayerNames = nullptr;
    deviceInfo.enabledExtensionCount = ( uint32_t )deviceExtensions.size();
    deviceInfo.ppEnabledExtensionNames = deviceExtensions.empty() ? nullptr : deviceExtensions.data();
    deviceInfo.pEnabledFeatures = &features;

    XrVulkanDeviceCreateInfoKHR deviceCreateInfo{};
    deviceCreateInfo.type = XR_TYPE_VULKAN_DEVICE_CREATE_INFO_KHR;
    deviceCreateInfo.systemId = xr_system_id;
    deviceCreateInfo.pfnGetInstanceProcAddr = &vkGetInstanceProcAddr;
    deviceCreateInfo.vulkanCreateInfo = &deviceInfo;
    deviceCreateInfo.vulkanPhysicalDevice = vk_physical_device;
    deviceCreateInfo.vulkanAllocator = nullptr;

    PFN_xrCreateVulkanDeviceKHR xrCreateVulkanDeviceKHR = nullptr;
    XR_CHECK_RESULT( xrGetInstanceProcAddr( xr_instance, "xrCreateVulkanDeviceKHR",
                                            reinterpret_cast<PFN_xrVoidFunction*>( &xrCreateVulkanDeviceKHR ) ) );

    XR_CHECK_RESULT( xrCreateVulkanDeviceKHR( xr_instance, &deviceCreateInfo, &vk_device, &err ) );
    VK_CHECK_RESULT( err );

    //////////////
    XrGraphicsBindingVulkan2KHR graphics_binding {};
    graphics_binding.type = XR_TYPE_GRAPHICS_BINDING_VULKAN2_KHR;
    graphics_binding.instance = vk_instance;
    graphics_binding.physicalDevice = vk_physical_device;
    graphics_binding.device = vk_device;
    graphics_binding.queueFamilyIndex = 0;
    graphics_binding.queueIndex = 0;

    XrSessionCreateInfo session_create_info{};
    session_create_info.type = XR_TYPE_SESSION_CREATE_INFO;
    session_create_info.next = &graphics_binding;
    session_create_info.systemId = xr_system_id;

    XR_CHECK_RESULT( xrCreateSession( xr_instance, &session_create_info, &xr_session ) );
}

/**************************************************************************************************/
Xr::~Xr()
{
    if( xr_session != XR_NULL_HANDLE )
    {
        XR_CHECK_RESULT( xrDestroySession( xr_session ) );
    }
    if( vkDebugUtilsMessenger != VK_NULL_HANDLE )
    {
        vkDestroyDebugUtilsMessengerEXT( vk_instance,
                                         vkDebugUtilsMessenger,
                                         nullptr );
    }

    if( xrDebugUtilsMessenger != VK_NULL_HANDLE )
    {
        xrDestroyDebugUtilsMessengerEXT( xrDebugUtilsMessenger );
    }
    XR_CHECK_RESULT( xrDestroyInstance( xr_instance ) );
}

/**************************************************************************************************/
void Xr::run()
{
    std::cout << "Running" << std::endl;
    while( xr_session_state != XR_SESSION_STATE_EXITING )
    {
        xr_session_state = XR_SESSION_STATE_EXITING;
    }
}
