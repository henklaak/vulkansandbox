#pragma once
#include <vulkan/vulkan.hpp>
#include <imgui/imgui.h>
#include <glm/glm.hpp>
#include "../core/renderer.h"

class Device;
class Buffer;
class Image;
class Swapchain;
class GlRenderModel;

class OffscreenPass;
class SwapchainPass;

class GlRenderer : public Renderer
{
public:
    GlRenderer( std::shared_ptr<Device> device,
                    std::shared_ptr<Swapchain> swapchain,
                    std::shared_ptr<GlRenderModel> renderModel );
    ~GlRenderer();

    void render( uint32_t frameNr );

private:
    std::shared_ptr<GlRenderModel> m_pRenderModel;
    std::shared_ptr<OffscreenPass> m_pOffscreenPass;
    std::shared_ptr<SwapchainPass> m_pSwapchainPass;

    VkSampleCountFlagBits m_msaa =  VK_SAMPLE_COUNT_2_BIT;
};
