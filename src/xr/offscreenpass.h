#pragma once
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>

class Device;
class Image;
class Swapchain;
class GlRenderModel;

class OffscreenPass
{
public:
    OffscreenPass( std::shared_ptr<Device> pDevice,
                       std::shared_ptr<Swapchain> pSwapchain,
                       std::shared_ptr<GlRenderModel> pRenderModel );
    virtual ~OffscreenPass();

    void update();
    VkSemaphore render( VkSemaphore waitSemaphore );
    std::shared_ptr<Image>  albedoResolved()
    {
        return m_albedoResolved;
    };

private:
    void initRenderPass();
    void initFrameBuffer();
    void initPipeline();
    void initCommandBuffers();
    void initFences();
    void initSemaphores();

    std::shared_ptr<Device> m_pDevice;
    std::shared_ptr<Swapchain> m_pSwapchain;
    std::shared_ptr<GlRenderModel> m_pRenderModel;

    uint32_t m_width = 16;
    uint32_t m_height = 16;
    VkSampleCountFlagBits m_msaa =  VK_SAMPLE_COUNT_2_BIT;

    std::shared_ptr<Image>  m_albedo;
    std::shared_ptr<Image>  m_depth;
    std::shared_ptr<Image>  m_albedoResolved;

    VkPipelineLayout m_pipelineLayout = VK_NULL_HANDLE;
    VkPipeline m_pipeline = VK_NULL_HANDLE;

    VkFramebuffer m_frameBuffer  = VK_NULL_HANDLE;
    VkRenderPass m_renderPass  = VK_NULL_HANDLE;
    VkCommandPool m_commandPool = VK_NULL_HANDLE;
    VkCommandBuffer m_commandBuffer = VK_NULL_HANDLE;
    VkFence m_commandFence = VK_NULL_HANDLE;
    VkSemaphore m_finishedSemaphore = VK_NULL_HANDLE;
};
