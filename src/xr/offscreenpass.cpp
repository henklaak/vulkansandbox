#include "offscreenpass.h"
#include "../core/vulkancore.h"
#include "../core/device.h"
#include "../core/image.h"
#include "../core/swapchain.h"
#include "glrendermodel.h"

/**************************************************************************************************/
OffscreenPass::OffscreenPass( std::shared_ptr<Device> pDevice,
                                      std::shared_ptr<Swapchain> pSwapchain,
                                      std::shared_ptr<GlRenderModel> pRenderModel )
    : m_pDevice( pDevice )
    , m_pSwapchain( pSwapchain )
    , m_pRenderModel( pRenderModel )
{
    m_msaa = pDevice->getMaxMsaa();

    m_width = m_pSwapchain->width();
    m_height = m_pSwapchain->height();

    initRenderPass();
    initFrameBuffer();
    initPipeline();
    initCommandBuffers();
    initFences();
    initSemaphores();
}

/**************************************************************************************************/
OffscreenPass::~OffscreenPass()
{
    vkDestroySemaphore( m_pDevice->device(), m_finishedSemaphore, nullptr );
    vkDestroyFence( m_pDevice->device(), m_commandFence, nullptr );
    vkDestroyPipelineLayout( m_pDevice->device(), m_pipelineLayout, nullptr );
    vkDestroyPipeline( m_pDevice->device(), m_pipeline, nullptr );
    vkDestroyRenderPass( m_pDevice->device(), m_renderPass, nullptr );
    vkDestroyFramebuffer( m_pDevice->device(), m_frameBuffer, nullptr );
}


/**************************************************************************************************/
void OffscreenPass::initRenderPass()
{
    VkAttachmentDescription albedoAttachment{};
    albedoAttachment.format = VK_FORMAT_R8G8B8A8_SRGB;
    albedoAttachment.samples = m_msaa;
    albedoAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    albedoAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    albedoAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    albedoAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    albedoAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    albedoAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentDescription depthAttachment{};
    depthAttachment.format = m_pDevice->findDepthFormat();
    depthAttachment.samples = m_msaa;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentDescription albedoResolvedAttachment{};
    albedoResolvedAttachment.format = VK_FORMAT_R8G8B8A8_SRGB;
    albedoResolvedAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    albedoResolvedAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    albedoResolvedAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    albedoResolvedAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    albedoResolvedAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    albedoResolvedAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    albedoResolvedAttachment.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkAttachmentReference albedoAttachmentRef{};
    albedoAttachmentRef.attachment = 0;
    albedoAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthAttachmentRef{};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference albedoResolvedAttachmentRef{};
    albedoResolvedAttachmentRef.attachment = 2;
    albedoResolvedAttachmentRef.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    std::array<VkAttachmentReference, 1> colorRefs =
    {
        albedoAttachmentRef,
    };

    std::array<VkAttachmentReference, 1> colorResolvedRefs =
    {
        albedoResolvedAttachmentRef,
    };


    VkSubpassDescription subpassDescription{};
    subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDescription.colorAttachmentCount = colorRefs.size();
    subpassDescription.pColorAttachments = colorRefs.data();
    subpassDescription.pResolveAttachments = colorResolvedRefs.data();
    subpassDescription.pDepthStencilAttachment = &depthAttachmentRef;

    std::array<VkAttachmentDescription, 3> attachmentDescriptions =
    {
        albedoAttachment,
        depthAttachment,
        albedoResolvedAttachment,
    };

    std::array<VkSubpassDependency, 2> dependencies;

    dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[0].dstSubpass = 0;
    dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                                    VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    dependencies[1].srcSubpass = 0;
    dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                                    VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    VkRenderPassCreateInfo renderPassCI{};
    renderPassCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassCI.attachmentCount = attachmentDescriptions.size();
    renderPassCI.pAttachments = attachmentDescriptions.data();
    renderPassCI.subpassCount = 1;
    renderPassCI.pSubpasses = &subpassDescription;
    renderPassCI.dependencyCount = dependencies.size();
    renderPassCI.pDependencies = dependencies.data();

    VK_CHECK_RESULT( vkCreateRenderPass( m_pDevice->device(),
                                         &renderPassCI,
                                         nullptr,
                                         &m_renderPass ) );
}

/**************************************************************************************************/
void OffscreenPass::initFrameBuffer()
{

    m_albedo = Image::create(
                   m_pDevice,
                   m_width,
                   m_height,
                   1,
                   1,
                   m_msaa,
                   VK_FORMAT_R8G8B8A8_SRGB,
                   VK_IMAGE_TILING_OPTIMAL,
                   VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                   VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                   VK_IMAGE_ASPECT_COLOR_BIT );
    m_depth = Image::create(
                  m_pDevice,
                  m_width,
                  m_height,
                  1,
                  1,
                  m_msaa,
                  m_pDevice->findDepthFormat(),
                  VK_IMAGE_TILING_OPTIMAL,
                  VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                  VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                  VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT );
    m_albedoResolved = Image::create(
                           m_pDevice,
                           m_width,
                           m_height,
                           1,
                           1,
                           VK_SAMPLE_COUNT_1_BIT,
                           VK_FORMAT_R8G8B8A8_SRGB,
                           VK_IMAGE_TILING_OPTIMAL,
                           VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                           VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                           VK_IMAGE_ASPECT_COLOR_BIT );

    std::vector<VkImageView> attachments =
    {
        m_albedo->view,
        m_depth->view,
        m_albedoResolved->view,
    };
    VkFramebufferCreateInfo framebufferCI{};
    framebufferCI.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferCI.renderPass = m_renderPass;
    framebufferCI.attachmentCount = attachments.size();
    framebufferCI.pAttachments = attachments.data();
    framebufferCI.width = m_width;
    framebufferCI.height = m_height;
    framebufferCI.layers = 1;

    VK_CHECK_RESULT( vkCreateFramebuffer(
                         m_pDevice->device(),
                         &framebufferCI,
                         nullptr,
                         &m_frameBuffer ) );
}

/**************************************************************************************************/
void OffscreenPass::initPipeline()
{
    // Pipeline layout
    std::array<VkDescriptorSetLayout, 3> dstLayouts;
    dstLayouts[0] = m_pRenderModel->sceneDstLayout();
    dstLayouts[1] = m_pRenderModel->localDstLayout();
    dstLayouts[2] = m_pRenderModel->textureDstLayout();

    VkPipelineLayoutCreateInfo pipelineLayoutCI{};
    pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCI.setLayoutCount = dstLayouts.size();
    pipelineLayoutCI.pSetLayouts = dstLayouts.data();
    pipelineLayoutCI.pushConstantRangeCount = 0;
    pipelineLayoutCI.pPushConstantRanges = nullptr;

    VK_CHECK_RESULT( vkCreatePipelineLayout(
                         m_pDevice->device(),
                         &pipelineLayoutCI,
                         nullptr,
                         &m_pipelineLayout ) );

    auto vertShaderCode = m_pDevice->readFile( "offscreen.vert.spv" );
    auto fragShaderCode = m_pDevice->readFile( "offscreen.frag.spv" );

    VkShaderModule vertShaderModule = m_pDevice->createShaderModule( vertShaderCode );
    VkShaderModule fragShaderModule = m_pDevice->createShaderModule( fragShaderCode );

    VkPipelineShaderStageCreateInfo vertShaderStageCI{};
    vertShaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageCI.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageCI.module = vertShaderModule;
    vertShaderStageCI.pName = "main";

    VkPipelineShaderStageCreateInfo fragShaderStageCI{};
    fragShaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageCI.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageCI.module = fragShaderModule;
    fragShaderStageCI.pName = "main";

    VkPipelineShaderStageCreateInfo shaderStageCI[] =
    {
        vertShaderStageCI,
        fragShaderStageCI
    };

    std::vector<VkVertexInputBindingDescription> vertexBindingDescriptions =
        m_pRenderModel->vertexBindingDescriptions();
    std::vector<VkVertexInputAttributeDescription> vertexInputAttributeDescriptions =
        m_pRenderModel->vertexInputAttributeDescriptions();

    VkPipelineVertexInputStateCreateInfo vertexInputStateCI{};
    vertexInputStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStateCI.vertexBindingDescriptionCount = vertexBindingDescriptions.size();
    vertexInputStateCI.pVertexBindingDescriptions = vertexBindingDescriptions.data();
    vertexInputStateCI.vertexAttributeDescriptionCount = vertexInputAttributeDescriptions.size();
    vertexInputStateCI.pVertexAttributeDescriptions = vertexInputAttributeDescriptions.data();

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCI{};
    inputAssemblyStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssemblyStateCI.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssemblyStateCI.primitiveRestartEnable = VK_FALSE;

    VkViewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = ( float ) m_width;
    viewport.height = ( float ) m_height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor{};
    scissor.offset = {0, 0};
    scissor.extent.width = m_width;
    scissor.extent.height = m_height;

    VkPipelineViewportStateCreateInfo viewportStateCI{};
    viewportStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportStateCI.viewportCount = 1;
    viewportStateCI.pViewports = &viewport;
    viewportStateCI.scissorCount = 1;
    viewportStateCI.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizationStateCI{};
    rasterizationStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizationStateCI.depthClampEnable = VK_FALSE;
    rasterizationStateCI.rasterizerDiscardEnable = VK_FALSE;
    rasterizationStateCI.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizationStateCI.lineWidth = 1.0f;
    rasterizationStateCI.cullMode = VK_CULL_MODE_NONE;
    rasterizationStateCI.frontFace = VK_FRONT_FACE_CLOCKWISE;
    rasterizationStateCI.depthBiasEnable = VK_FALSE;

    VkPipelineMultisampleStateCreateInfo multisampleStateCI{};
    multisampleStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampleStateCI.sampleShadingEnable = VK_TRUE;
    multisampleStateCI.minSampleShading = 0.2f;
    multisampleStateCI.rasterizationSamples = m_msaa;

    std::array<VkPipelineColorBlendAttachmentState, 1> colorBlendAttachments{};
    colorBlendAttachments[0].colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachments[0].blendEnable = VK_FALSE;

    VkPipelineDepthStencilStateCreateInfo depthStencil{};
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.stencilTestEnable = VK_FALSE;

    std::vector<VkDynamicState> dynamicStateEnables =
    {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };
    VkPipelineDynamicStateCreateInfo dynamicStateCI{};
    dynamicStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicStateCI.dynamicStateCount = dynamicStateEnables.size();
    dynamicStateCI.pDynamicStates = dynamicStateEnables.data();

    VkPipelineColorBlendStateCreateInfo colorBlendStateCI{};
    colorBlendStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlendStateCI.logicOpEnable = VK_FALSE;
    colorBlendStateCI.logicOp = VK_LOGIC_OP_CLEAR;
    colorBlendStateCI.attachmentCount = colorBlendAttachments.size();
    colorBlendStateCI.pAttachments = colorBlendAttachments.data();
    colorBlendStateCI.blendConstants[0] = 0.0f;
    colorBlendStateCI.blendConstants[1] = 0.0f;
    colorBlendStateCI.blendConstants[2] = 0.0f;
    colorBlendStateCI.blendConstants[3] = 0.0f;

    VkGraphicsPipelineCreateInfo pipelineCI{};
    pipelineCI.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCI.stageCount = 2;
    pipelineCI.pStages = shaderStageCI;
    pipelineCI.pVertexInputState = &vertexInputStateCI;
    pipelineCI.pInputAssemblyState = &inputAssemblyStateCI;
    pipelineCI.pViewportState = &viewportStateCI;
    pipelineCI.pRasterizationState = &rasterizationStateCI;
    pipelineCI.pMultisampleState = &multisampleStateCI;
    pipelineCI.pDepthStencilState = &depthStencil;
    pipelineCI.pDynamicState = &dynamicStateCI;
    pipelineCI.pColorBlendState = &colorBlendStateCI;
    pipelineCI.layout = m_pipelineLayout;
    pipelineCI.renderPass = m_renderPass;
    pipelineCI.subpass = 0;
    pipelineCI.basePipelineHandle = VK_NULL_HANDLE;

    VK_CHECK_RESULT( vkCreateGraphicsPipelines(
                         m_pDevice->device(),
                         VK_NULL_HANDLE,
                         1,
                         &pipelineCI,
                         nullptr,
                         &m_pipeline ) );

    vkDestroyShaderModule( m_pDevice->device(), vertShaderModule, nullptr );
    vkDestroyShaderModule( m_pDevice->device(), fragShaderModule, nullptr );

}

/**************************************************************************************************/
void OffscreenPass::initCommandBuffers()
{
    VkCommandBufferAllocateInfo allocateInfo{};
    allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocateInfo.commandPool = m_pDevice->commandPool();
    allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocateInfo.commandBufferCount = 1;

    VK_CHECK_RESULT( vkAllocateCommandBuffers(
                         m_pDevice->device(),
                         &allocateInfo,
                         &m_commandBuffer ) );

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

    VK_CHECK_RESULT( vkBeginCommandBuffer( m_commandBuffer,
                                           &beginInfo ) );
    std::array<VkClearValue, 2> clearValues{};
    clearValues[0].color = {{0.2f, 0.3f, 0.4f, 1.0f}};
    clearValues[1].depthStencil = {1.0f, 0};

    VkRenderPassBeginInfo renderPassBeginInfo{};
    renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.renderPass = m_renderPass;
    renderPassBeginInfo.framebuffer = m_frameBuffer;
    renderPassBeginInfo.renderArea.offset = {0, 0};
    renderPassBeginInfo.renderArea.extent.width = m_width ;
    renderPassBeginInfo.renderArea.extent.height = m_height;
    renderPassBeginInfo.clearValueCount = clearValues.size();
    renderPassBeginInfo.pClearValues = clearValues.data();

    vkCmdBeginRenderPass( m_commandBuffer,
                          &renderPassBeginInfo,
                          VK_SUBPASS_CONTENTS_INLINE );

    VkViewport viewport{};
    VkRect2D scissor{};
    viewport.width = m_width;
    viewport.height = m_height;
    viewport.minDepth = 0.0;
    viewport.maxDepth = 1.0;
    scissor.extent.width = m_width;
    scissor.extent.height = m_height;
    vkCmdSetViewport( m_commandBuffer, 0, 1, &viewport );
    vkCmdSetScissor( m_commandBuffer, 0, 1, &scissor );

    vkCmdBindPipeline( m_commandBuffer,
                       VK_PIPELINE_BIND_POINT_GRAPHICS,
                       m_pipeline );

    m_pRenderModel->renderToCommandBuffer( m_commandBuffer,
                                           m_pipelineLayout );

    vkCmdEndRenderPass( m_commandBuffer );
    VK_CHECK_RESULT( vkEndCommandBuffer( m_commandBuffer ) );
}

/**************************************************************************************************/
void OffscreenPass::initFences()
{
    VkFenceCreateInfo fenceCI{};
    fenceCI.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCI.flags = VK_FENCE_CREATE_SIGNALED_BIT;
    vkCreateFence( m_pDevice->device(),
                   &fenceCI,
                   nullptr,
                   &m_commandFence );
}

/**************************************************************************************************/
void OffscreenPass::initSemaphores()
{
    VkSemaphoreCreateInfo semaphoreCI{};
    semaphoreCI.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    vkCreateSemaphore( m_pDevice->device(),
                       &semaphoreCI,
                       nullptr,
                       &m_finishedSemaphore );

}

/**************************************************************************************************/
void OffscreenPass::update()
{

}

/**************************************************************************************************/
VkSemaphore OffscreenPass::render( VkSemaphore waitSemaphore )
{
    // Swapchain pass
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT};

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &waitSemaphore;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &m_finishedSemaphore;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &m_commandBuffer;

    VK_CHECK_RESULT( vkWaitForFences( m_pDevice->device(), 1, &m_commandFence, VK_TRUE, UINT64_MAX ) );
    VK_CHECK_RESULT( vkResetFences( m_pDevice->device(), 1, &m_commandFence ) );

    VK_CHECK_RESULT( vkQueueSubmit( m_pDevice->graphicsQueue(),
                                    1,
                                    &submitInfo,
                                    m_commandFence ) );

    return m_finishedSemaphore;
}
