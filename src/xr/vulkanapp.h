#pragma once
#include <memory>

class Instance;
class Camera;
class Window;
class PhysicalDevice;
class Device;
class Swapchain;
class GlRenderer;
class GlRenderModel;
class Xr;

class App
{
public:
    App();
    virtual ~App();
    void run();

private:
    bool m_exitRequested = false;


    std::shared_ptr<Xr> m_pXr;
    std::shared_ptr<Instance> m_pInstance;
    std::shared_ptr<Camera> m_pCamera;
    std::shared_ptr<Window> m_pWindow;
    std::shared_ptr<PhysicalDevice> m_pPhysicalDevice;
    std::shared_ptr<Device> m_pDevice;
    std::shared_ptr<Swapchain> m_pSwapchain;

    std::shared_ptr<GlRenderer> m_pGlRenderer;
    std::shared_ptr<GlRenderModel> m_pGlRenderModel;

    uint32_t m_currentFrame = 0;
};
