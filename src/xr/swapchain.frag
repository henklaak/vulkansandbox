#version 460
#extension GL_GOOGLE_include_directive : require
#include "shader_constants.h"

layout (location = 0) in vec2 inUV;

layout (binding = BINDING_ALBEDO) uniform sampler2D albedo;

layout (location = 0) out vec4 outFragcolor;

void main()
{
    vec4 albedo = texture(albedo, inUV).rgba;
    outFragcolor = albedo.rgba;
}
