#pragma once
#include <vulkan/vulkan.h>
#include <vulkan/vulkan.h>
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>
#include <iostream>
#include <cassert>
#include <vector>

class Xr
{
public:
    Xr();
    ~Xr();

    void run();

private:
    XrInstance xr_instance = XR_NULL_HANDLE;
    XrSystemId xr_system_id = XR_NULL_SYSTEM_ID;
    XrSession xr_session = XR_NULL_HANDLE;
    XrDebugUtilsMessengerEXT xrDebugUtilsMessenger = VK_NULL_HANDLE;
    PFN_xrCreateDebugUtilsMessengerEXT xrCreateDebugUtilsMessengerEXT;
    PFN_xrDestroyDebugUtilsMessengerEXT xrDestroyDebugUtilsMessengerEXT;
    XrSessionState xr_session_state = XR_SESSION_STATE_UNKNOWN;

    VkInstance vk_instance = VK_NULL_HANDLE;
    VkPhysicalDevice vk_physical_device = VK_NULL_HANDLE;
    VkDevice vk_device = VK_NULL_HANDLE;
    VkDebugUtilsMessengerEXT vkDebugUtilsMessenger = VK_NULL_HANDLE;
    PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT;
    PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT;
};

#define XR_CHECK_RESULT(f)                                                                         \
{                                                                                                  \
    XrResult res = (f);                                                                            \
    if( res != XR_SUCCESS )                                                                        \
    {                                                                                              \
        std::cerr << "Fatal : XrResult is \"" << res << "\"" <<                                    \
                     " in " << __FILE__ <<                                                         \
                     " at line " << __LINE__ << "\n";                                              \
        assert( res == XR_SUCCESS );                                                               \
    }                                                                                              \
}

#define VK_CHECK_RESULT(f)                                                                         \
{                                                                                                  \
    VkResult res = (f);                                                                            \
    if( res != VK_SUCCESS )                                                                        \
    {                                                                                              \
        std::cerr << "Fatal : VkResult is \"" << res << "\"" <<                                    \
                     " in " << __FILE__ <<                                                         \
                     " at line " << __LINE__ << "\n";                                              \
        assert( res == VK_SUCCESS );                                                               \
    }                                                                                              \
}

