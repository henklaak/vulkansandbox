#ifndef SHADER_CONSTANTS_GL_H
#define SHADER_CONSTANTS_GL_H

#ifdef __cplusplus
#include "../core/shader_common.h"
#endif

// offscreen vert
#define LOC_VERTEX_POSITION       (0)
#define LOC_VERTEX_NORMAL         (1)
#define LOC_VERTEX_TANGENT        (2)
#define LOC_VERTEX_UV             (3)

#define LOC_FRAG_POSITION         (0)
#define LOC_FRAG_NORMAL           (1)
#define LOC_FRAG_UV               (2)

#define SET_WORLD                 (0)
#define BINDING_WORLD_TRANSFORM    (0)
#define BINDING_LIGHTS             (1)

#define SET_MODEL                 (1)
#define BINDING_MODEL_TRANSFORM    (0)

#define SET_TEXTURES              (2)
#define BINDING_ALBEDO             (0)
#define BINDING_AO                 (1)
#define BINDING_NORMAL             (2)

struct Light
{
    vec4 position;
    vec3 color;
    float radius;
};

#ifndef __cplusplus
#endif

#endif // SHADER_CONSTANTS_GL_H
