#version 460
#extension GL_GOOGLE_include_directive : require
#include "shader_constants.h"

layout (location = LOC_FRAG_POSITION) in vec3 inPos;
layout (location = LOC_FRAG_NORMAL) in vec3 inNormal;
layout (location = LOC_FRAG_UV) in vec2 inUV;

layout (set = SET_WORLD, binding = BINDING_LIGHTS) uniform Lights
{
    Light lights[8];
} ubo;

layout (set = SET_TEXTURES, binding = BINDING_ALBEDO) uniform sampler2D albedoMap;
layout (set = SET_TEXTURES, binding = BINDING_AO) uniform sampler2D aoMap;
layout (set = SET_TEXTURES, binding = BINDING_NORMAL) uniform sampler2D normalMap;

layout (location = 0) out vec4 outFragcolor;

vec4 viewPos = vec4(10.0,10.0,0.0,1.0);

void main()
{
    vec4 albedo = texture(albedoMap, inUV).rgba;
    vec4 AO = texture(aoMap, inUV).rgba;
    vec4 normalValue = texture(normalMap, inUV).rgba;

    #define ambient 0.00
    vec3 fragColor = albedo.rgb * ambient;

    for (int i=0; i<8; ++i)
    {
        vec3 L = ubo.lights[i].position.xyz - inPos;
        float dist = length(L);
        vec3 V = viewPos.xyz - inPos;
        V = normalize(V);
        L = normalize(L);

        float atten = ubo.lights[i].radius / (pow(dist, 2.0) + 1.0);

        //vec3 N = normalize(inNormal);
        vec3 N = normalize(normalValue.xyz);

        float NdotL = max(0.0, dot(N, L));
        vec3 diff = ubo.lights[i].color * albedo.rgb * NdotL * atten;

        vec3 R = reflect(-L, N);
        float NdotR = max(0.0, dot(R, V));
        vec3 spec = ubo.lights[i].color * albedo.a * pow(NdotR, 16.0) * atten;

        fragColor += diff * AO.rgb;//+ spec;
    }

    #define exposure 1.0
    fragColor = vec3(1.0) - exp(-fragColor * exposure);
    //fragColor = fragColor / (fragColor + vec3(1.0));

    outFragcolor = vec4(fragColor, 1.0);
}
