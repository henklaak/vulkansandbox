#include "rtrendermodel.h"
#include <chrono>
#include <iostream>
#include <random>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/io.hpp>

#include "../core/vulkancore.h"
#include "../core/camera.h"
#include "../core/device.h"
#include "../core/buffer.h"
#include "../core/image.h"
#include "../rt/shader_constants.h"

/**************************************************************************************************/
RtRenderModel::RtRenderModel( std::shared_ptr<Device> device,
                                      std::shared_ptr<Camera> camera )
    : m_pDevice( device )
    , m_pCamera( camera )
{
}

/**************************************************************************************************/
RtRenderModel::~RtRenderModel()
{
}

/**************************************************************************************************/
void RtRenderModel::addPrimitive( const glm::mat4 &localTransform,
                                      const std::vector<RawVertex> &vertices,
                                      const std::vector<uint16_t> &indices,
                                      const RawTexture &albedoTexture,
                                      const RawTexture &normalTexture,
                                      const RawTexture &ambientOcclusionTexture,
                                      const RawTexture &emissiveTexture )
{
    std::vector<glm::vec3> pos;

    for( auto &vertex : vertices )
    {
        pos.push_back( vec3( localTransform * glm::vec4( vertex.position.x,
                             vertex.position.y,
                             vertex.position.z,
                             1 ) ) );
    }

    PrimitiveModel pm{};

    pm.m_nrVertices = vertices.size();
    pm.pVertexBuffer = Buffer::create(
                           m_pDevice,
                           reinterpret_cast<uint8_t *>( pos.data() ),
                           pos.size() * sizeof( glm::vec3 ),
                           VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                           VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );


    std::vector<uint16_t> idx;
    for( auto &index : indices )
    {
        idx.push_back( index );
    }
    pm.m_nrIndices = indices.size();
    pm.m_nrFaces = indices.size() / 3;
    pm.pIndexBuffer = Buffer::create(
                          m_pDevice,
                          reinterpret_cast<uint8_t *>( idx.data() ),
                          idx.size() * sizeof( uint16_t ),
                          VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                          VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
                          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );


    uint32_t new_matid = m_primitiveModels.size();

    std::vector<uint32_t> m_matIds;
    m_matIds.resize( pm.m_nrFaces );
    for( size_t i = 0; i < pm.m_nrFaces; ++i )
    {
        m_matIds[i] = new_matid;
    }
    pm.m_matIdBuffer = Buffer::create(
                           m_pDevice,
                           reinterpret_cast<uint8_t *>( m_matIds.data() ),
                           m_matIds.size() * sizeof( uint32_t ),
                           VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

    std::vector<VertexAttribute> m_attribs;
    for( size_t i = 0; i < vertices.size(); ++i )
    {
        VertexAttribute attr{};

        attr.uv.x = vertices[i].uv.x;
        attr.uv.y = vertices[i].uv.y;
        attr.normal.x = vertices[i].normal.x;
        attr.normal.y = vertices[i].normal.y;
        attr.normal.z = vertices[i].normal.z;
        m_attribs.push_back( attr );
    }
    pm.m_attribBuffer = Buffer::create(
                            m_pDevice,
                            reinterpret_cast<uint8_t *>( m_attribs.data() ),
                            m_attribs.size() * sizeof( VertexAttribute ),
                            VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

    std::vector<glm::uvec4> m_faces;
    for( size_t i = 0; i < indices.size(); i += 3 )
    {
        m_faces.push_back( glm::uvec4( indices[i + 0],
                                       indices[i + 1],
                                       indices[i + 2],
                                       0 ) );
    }
    pm.m_faceBuffer = Buffer::create(
                          m_pDevice,
                          reinterpret_cast<uint8_t *>( m_faces.data() ),
                          m_faces.size() * sizeof( glm::uvec4 ),
                          VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

    // albedo
    {
        const size_t numLayers = 1;
        const size_t numMipLevels = std::floor( std::log2( std::max(
                albedoTexture.nrcols,
                albedoTexture.nrrows ) ) ) + 1;

        std::shared_ptr<Buffer> pStagingBuffer = Buffer::create(
                    m_pDevice,
                    albedoTexture.rawData,
                    albedoTexture.nrcols * albedoTexture.nrrows * 4,
                    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

        pm.m_albedoImage = Image::create( m_pDevice,
                                              albedoTexture.nrcols,
                                              albedoTexture.nrrows,
                                              numLayers,
                                              numMipLevels,
                                              VK_SAMPLE_COUNT_1_BIT,
                                              VK_FORMAT_R8G8B8A8_SRGB,
                                              VK_IMAGE_TILING_OPTIMAL,
                                              VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                                              VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                              VK_IMAGE_ASPECT_COLOR_BIT );

        pm.m_albedoImage->fillFromBuffer( pStagingBuffer->buffer );
        pm.m_albedoImage->generateMipMaps( numMipLevels );
    }

    m_primitiveModels.push_back( pm );
}

/**************************************************************************************************/
void RtRenderModel::setCamera( const glm::mat4 &localTransform,
                                   double yFov,
                                   double aspectRatio,
                                   double zNear,
                                   double zFar )
{
    pCameraMvpBuffer = Buffer::create( m_pDevice,
                                           nullptr,
                                           sizeof( MVP ),
                                           VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                           VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );
    // Use this before cmdBuffer building
    glm::mat4 model = glm::mat4( 1.0f ); // scene transform
    glm::mat4 view = glm::inverse( localTransform );

    glm::mat4 projection = glm::perspective(
                               yFov,
                               aspectRatio,
                               zNear,
                               zFar );
    projection[1][1] *= -1;

    m_pCamera->setView( view );
    m_pCamera->setProjection( projection );
}

/**************************************************************************************************/
void RtRenderModel::updateCamera( float dt )
{
    m_pCamera->update( dt );

//    std::random_device rd;
//    std::mt19937 e2(rd());
//    std::normal_distribution<> dist(0.0, 0.0001);

//    glm::mat4 a = glm::translate(m_pCamera->view(), glm::vec3(dist(e2), dist(e2), dist(e2)));

    m_cameraMvp.viewInverse = glm::inverse( m_pCamera->view() );
    m_cameraMvp.projInverse = glm::inverse( m_pCamera->projection() );
    void* data = nullptr;



    data = pCameraMvpBuffer->map();
    memcpy( data, &m_cameraMvp, sizeof( MVP ) );
    pCameraMvpBuffer->unmap();
}

/**************************************************************************************************/
void RtRenderModel::addPointLight( const glm::mat4 &localTransform,
                                       const glm::vec3 &color,
                                       double intensity )
{
    std::cout << "TODO addPointLight" << std::endl;
}
