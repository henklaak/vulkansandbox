#pragma once
#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>

#include "../core/renderer.h"
#include "shader_constants.h"

class Device;
class Buffer;
class Image;
class Swapchain;
class RtRenderModel;

class RtRenderer : public Renderer
{
public:
    RtRenderer( std::shared_ptr<Device> pDevice,
                    std::shared_ptr<Swapchain> pSwapchain,
                    std::shared_ptr<RtRenderModel> pRenderModel );
    virtual ~RtRenderer();

    void render( uint32_t frameNr ) override;


private:
    struct AccelerationStructure
    {
        VkAccelerationStructureKHR handle = VK_NULL_HANDLE;
        VkDeviceAddress address = 0;
        std::shared_ptr<Buffer> pBuffer;
    };
    void getDeviceProcs();

    void prepare();

    void createBottomLevelAccelerationStructure();
    void createTopLevelAccelerationStructure();
    void createStorageImage();
    void createRayTracingPipeline();
    void createShaderBindingTable();
    void createDescriptorSets();

    void initSyncObjects();
    void destroySyncObjects();

    void initCommandBuffers();
    void destroyCommandBuffers();

    std::shared_ptr<RtRenderModel> m_pRenderModel;
    VkDescriptorPool m_descriptorPool = VK_NULL_HANDLE;

    struct OffscreenRenderPass
    {
        std::vector<AccelerationStructure> m_bottomLevelAS{};
        AccelerationStructure m_topLevelAS{};

        std::shared_ptr<Image> pStorageImage;
        std::vector<VkFence> m_commandFences{};
        std::vector<VkCommandBuffer> m_commandBuffers{};

        std::shared_ptr<Buffer> pRaygenShaderBindingTableBuffer;
        std::shared_ptr<Buffer> pMissShaderBindingTableBuffer;
        std::shared_ptr<Buffer> pHitShaderBindingTableBuffer;
        std::vector<VkRayTracingShaderGroupCreateInfoKHR> m_shaderGroups{};

        std::vector<VkDescriptorSetLayout> m_descriptorSetLayouts{};
        std::vector<VkDescriptorSet> m_descriptorSets{};

        VkPipelineLayout m_pipelineLayout = VK_NULL_HANDLE;
        VkPipeline m_pipeline = VK_NULL_HANDLE;
    } m_offscreenRenderPass{};

    PFN_vkCmdTraceRaysKHR vkCmdTraceRaysKHR;
    PFN_vkGetAccelerationStructureBuildSizesKHR vkGetAccelerationStructureBuildSizesKHR;
    PFN_vkCreateAccelerationStructureKHR vkCreateAccelerationStructureKHR;
    PFN_vkDestroyAccelerationStructureKHR vkDestroyAccelerationStructureKHR;
    PFN_vkCmdBuildAccelerationStructuresKHR vkCmdBuildAccelerationStructuresKHR;
    PFN_vkGetAccelerationStructureDeviceAddressKHR vkGetAccelerationStructureDeviceAddressKHR;
    PFN_vkCreateRayTracingPipelinesKHR vkCreateRayTracingPipelinesKHR;
    PFN_vkGetRayTracingShaderGroupHandlesKHR vkGetRayTracingShaderGroupHandlesKHR;
};
