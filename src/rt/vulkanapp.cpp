// https://docs.blender.org/manual/en/2.92/addons/import_export/scene_gltf2.html

#include "vulkanapp.h"
#include <unistd.h>
#include <glm/gtx/io.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../core/instance.h"
#include "../core/physicaldevice.h"
#include "../core/window.h"
#include "../core/device.h"
#include "../core/swapchain.h"
#include "../core/modelloader.h"
#include "../core/vrloader.h"
#include "rtrenderer.h"
#include "rtrendermodel.h"

std::string modelFilename = "cube_textured.glb" ;

/**************************************************************************************************/
void __assertmsg( const char* expr_str, bool expr, const char* file, int line, const char* msg )
{
    if( !expr )
    {
        std::cerr << "Assert failed:\t" << msg << "\n"
                  << "Expected:\t" << expr_str << "\n"
                  << "Source:\t\t" << file << ", line " << line << "\n";
        abort();
    }
}

/**************************************************************************************************/
App::App()
{
    m_pInstance.reset( new Instance );
    m_pCamera.reset( new Camera );
    m_pWindow.reset( new Window( m_pInstance, m_pCamera ) );
    m_pPhysicalDevice.reset( new PhysicalDevice( m_pInstance,
                             m_pWindow ) );
    m_pDevice.reset( new Device( m_pInstance,
                                     m_pPhysicalDevice ) );
    m_pSwapchain.reset( new Swapchain( m_pDevice,
                                           m_pWindow ) );

    m_pRtRenderModel.reset( new RtRenderModel( m_pDevice,
                            m_pCamera ) );


    ModelLoader modelLoader( modelFilename );
    modelLoader.load( m_pRtRenderModel );

    VrLoader vrLoader;
    vrLoader.load( m_pRtRenderModel );

    m_pRtRenderer.reset( new RtRenderer( m_pDevice, m_pSwapchain, m_pRtRenderModel ) );
}

/**************************************************************************************************/
App::~App()
{
}

/**************************************************************************************************/
void App::run()
{
    while( !glfwWindowShouldClose( m_pWindow->window() ) && !m_exitRequested )
    {
        glfwPollEvents();

        m_currentFrame = m_pSwapchain->acquireNext();
        m_pRtRenderer->render( m_currentFrame );
        m_pSwapchain->present( m_currentFrame );
    }
}

/**************************************************************************************************/
int main( const int, const char *[] )
{
    App app;
    app.run();
    return 0;
}


/*

https://github.com/KhronosGroup/GLSL/tree/master/extensions
https://iorange.github.io/p02/TeapotAndBunny.html
https://github.com/iOrange/rtxON/blob/7b618576125ecb30a2444c278351c27a96873504/src/rtxApp.cpp
https://github.com/iOrange/rtxON/blob/740f7bdd151a6ac0dfc7b2add74fdc0194e126c0/src/shared_with_shaders.h

*/
