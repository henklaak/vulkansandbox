#version 450

layout (location = 0) in vec2 inUV;

layout (binding = 0) uniform sampler2D albedo;

layout (location = 0) out vec4 outFragcolor;


void main()
{
    vec4 albedo = texture(albedo, inUV).rgba;
    outFragcolor = albedo.rgba;
}
