#ifndef SHADER_CONSTANTS_H
#define SHADER_CONSTANTS_H

#ifdef __cplusplus
#include "shader_common.h"
#endif

#define SWS_SCENE_AS_SET                0
#define SWS_SCENE_AS_BINDING            0

#define SWS_RESULT_IMAGE_SET            0
#define SWS_RESULT_IMAGE_BINDING        1

#define SWS_CAMDATA_SET                 0
#define SWS_CAMDATA_BINDING             2

#define SWS_MATIDS_SET                  1
#define SWS_ATTRIBS_SET                 2
#define SWS_FACES_SET                   3
#define SWS_TEXTURES_SET                4
#define SWS_NUM_SETS                    5

#define SWS_LOC_PRIMARY_RAY             0
#define SWS_PRIMARY_HIT_SHADERS_IDX     0
#define SWS_PRIMARY_MISS_SHADERS_IDX    0

#define SWS_MAX_RECURSION               31


struct VertexAttribute {
    vec4 normal;
    vec4 uv;
};

struct RayPayload {
    vec4 colorAndDist;
    vec4 normalAndObjId;
};

#ifndef __cplusplus
float LinearToSrgb(float channel) {
    if (channel <= 0.0031308f) {
        return 12.92f * channel;
    } else {
        return 1.055f * pow(channel, 1.0f / 2.4f) - 0.055f;
    }
}

vec3 LinearToSrgb(vec3 linear) {
    return vec3(LinearToSrgb(linear.r), LinearToSrgb(linear.g), LinearToSrgb(linear.b));
}
#endif

#endif // SHADER_CONSTANTS_H
