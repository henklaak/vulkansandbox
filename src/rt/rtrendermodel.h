#pragma once
#include <string>
#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <tiny_gltf.h>
#include <memory>
#include "../core/rendermodel.h"

class Device;
class Camera;
class Image;
class Buffer;

struct PrimitiveModel
{
    size_t m_nrVertices;
    size_t m_nrIndices;
    size_t m_nrFaces;
    std::shared_ptr<Buffer> pVertexBuffer;
    std::shared_ptr<Buffer> pIndexBuffer;
    std::shared_ptr<Buffer> m_matIdBuffer;
    std::shared_ptr<Buffer> m_attribBuffer;
    std::shared_ptr<Buffer> m_faceBuffer;
    std::shared_ptr<Image> m_albedoImage;
};


class RtRenderModel : public RenderModel
{
public:
    RtRenderModel( std::shared_ptr<Device> device ,
                       std::shared_ptr<Camera> camera );
    virtual ~RtRenderModel();

    void addPrimitive( const glm::mat4 &localTransform,
                       const std::vector<RawVertex> &vertices,
                       const std::vector<uint16_t> &indices,
                       const RawTexture &albedoTexture,
                       const RawTexture &normalTexture,
                       const RawTexture &ambientOcclusionTexture,
                       const RawTexture &emissiveTexture ) override;

    void setCamera( const glm::mat4 &localTransform,
                    double yFov,
                    double aspectRatio,
                    double zNear,
                    double zFar ) override;

    void addPointLight( const glm::mat4 &localTransform,
                        const glm::vec3 &color,
                        double intensity ) override;

    void updateCamera(float dt);

    std::vector<PrimitiveModel> m_primitiveModels;
    std::shared_ptr<Buffer> pCameraMvpBuffer;

private:
    struct MVP
    {
        glm::mat4 viewInverse = glm::mat4( 1.0f );
        glm::mat4 projInverse = glm::mat4( 1.0f );
    };
    MVP m_cameraMvp;

    std::shared_ptr<Device> m_pDevice;
    std::shared_ptr<Camera> m_pCamera;
};
