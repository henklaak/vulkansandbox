#version 460
#extension GL_EXT_ray_tracing : enable
#extension GL_GOOGLE_include_directive : require

#include "shader_constants.h"

layout(location = SWS_LOC_PRIMARY_RAY) rayPayloadInEXT RayPayload PrimaryRay;

const float MY_PI = 3.1415926535897932384626433832795;
const float MY_INV_PI  = 1.0 / MY_PI;

vec2 DirToLatLong(vec3 dir) {
    float phi = atan(dir.x, dir.z);
    float theta = acos(dir.y);

    return vec2((MY_PI + phi) * (0.5 / MY_PI), theta * MY_INV_PI);
}

void main() {
    // View-independent background gradient to simulate a basic sky background
    const vec3 gradientStart = vec3(0.0,0.4,1.0);
    const vec3 gradientEnd = vec3(1.0,1.0,1.0);
    vec3 unitDir = normalize(gl_WorldRayDirectionEXT);
    float t = 0.5 * (unitDir.y + 1.0);
    vec3 color = (1.0-t) * gradientStart + t * gradientEnd;

    PrimaryRay.colorAndDist = vec4(color, -1.0f);
}
