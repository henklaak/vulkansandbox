#include "rtrenderer.h"
#include "../core/vulkancore.h"
#include "../core/device.h"
#include "../core/buffer.h"
#include "../core/image.h"
#include "../core/swapchain.h"
#include "rtrendermodel.h"


/**************************************************************************************************/
RtRenderer::RtRenderer( std::shared_ptr<Device> device,
                                std::shared_ptr<Swapchain> swapchain,
                                std::shared_ptr<RtRenderModel> renderModel )
    : Renderer( device, swapchain )
    , m_pRenderModel( renderModel )
{
    getDeviceProcs();
    prepare();
}

/**************************************************************************************************/
RtRenderer::~RtRenderer()
{
    vkDeviceWaitIdle( m_pDevice->device() );

    for( size_t i = 0; i < m_pSwapchain->nrImages(); ++i )
    {
        vkDestroyFence( m_pDevice->device(), m_offscreenRenderPass.m_commandFences[i], nullptr );
    }
    vkDestroyPipelineLayout( m_pDevice->device(), m_offscreenRenderPass.m_pipelineLayout, nullptr );
    vkDestroyPipeline( m_pDevice->device(), m_offscreenRenderPass.m_pipeline, nullptr );
    for( size_t i = 0; i < m_offscreenRenderPass.m_descriptorSetLayouts.size(); ++i )
    {
        vkDestroyDescriptorSetLayout( m_pDevice->device(), m_offscreenRenderPass.m_descriptorSetLayouts[i],
                                      nullptr );
    }
    vkDestroyDescriptorPool( m_pDevice->device(), m_descriptorPool, nullptr );

    for( size_t i = 0; i < m_offscreenRenderPass.m_bottomLevelAS.size(); ++i )
    {
        vkDestroyAccelerationStructureKHR( m_pDevice->device(),
                                           m_offscreenRenderPass.m_bottomLevelAS[i].handle, nullptr );
    }
    vkDestroyAccelerationStructureKHR( m_pDevice->device(), m_offscreenRenderPass.m_topLevelAS.handle,
                                       nullptr );
}

/**************************************************************************************************/
void RtRenderer::getDeviceProcs()
{
    vkCmdTraceRaysKHR =
        reinterpret_cast<PFN_vkCmdTraceRaysKHR>( vkGetDeviceProcAddr(
                    m_pDevice->device(), "vkCmdTraceRaysKHR" ) );
    vkCreateAccelerationStructureKHR =
        reinterpret_cast<PFN_vkCreateAccelerationStructureKHR>( vkGetDeviceProcAddr(
                    m_pDevice->device(), "vkCreateAccelerationStructureKHR" ) );
    vkGetAccelerationStructureBuildSizesKHR =
        reinterpret_cast<PFN_vkGetAccelerationStructureBuildSizesKHR>( vkGetDeviceProcAddr(
                    m_pDevice->device(), "vkGetAccelerationStructureBuildSizesKHR" ) );
    vkCmdBuildAccelerationStructuresKHR =
        reinterpret_cast<PFN_vkCmdBuildAccelerationStructuresKHR>( vkGetDeviceProcAddr(
                    m_pDevice->device(), "vkCmdBuildAccelerationStructuresKHR" ) );
    vkGetAccelerationStructureDeviceAddressKHR =
        reinterpret_cast<PFN_vkGetAccelerationStructureDeviceAddressKHR>( vkGetDeviceProcAddr(
                    m_pDevice->device(), "vkGetAccelerationStructureDeviceAddressKHR" ) );
    vkCreateRayTracingPipelinesKHR =
        reinterpret_cast<PFN_vkCreateRayTracingPipelinesKHR>( vkGetDeviceProcAddr(
                    m_pDevice->device(), "vkCreateRayTracingPipelinesKHR" ) );
    vkGetRayTracingShaderGroupHandlesKHR =
        reinterpret_cast<PFN_vkGetRayTracingShaderGroupHandlesKHR>( vkGetDeviceProcAddr(
                    m_pDevice->device(), "vkGetRayTracingShaderGroupHandlesKHR" ) );
    vkDestroyAccelerationStructureKHR =
        reinterpret_cast<PFN_vkDestroyAccelerationStructureKHR>( vkGetDeviceProcAddr(
                    m_pDevice->device(), "vkDestroyAccelerationStructureKHR" ) );
}


/**************************************************************************************************/
void RtRenderer::prepare()
{
    initSyncObjects();
    createBottomLevelAccelerationStructure();
    createTopLevelAccelerationStructure();
    createStorageImage();
    createRayTracingPipeline();
    createShaderBindingTable();
    createDescriptorSets();
    initCommandBuffers();
}


/**************************************************************************************************/
void RtRenderer::createBottomLevelAccelerationStructure()
{
    // Build
    const size_t numMeshes = m_pRenderModel->m_primitiveModels.size();

    std::vector<VkAccelerationStructureGeometryKHR> geometries;
    std::vector<VkAccelerationStructureBuildRangeInfoKHR> ranges;
    std::vector<VkAccelerationStructureBuildGeometryInfoKHR> buildInfos;
    std::vector<VkAccelerationStructureBuildSizesInfoKHR> sizeInfos;

    geometries.resize( numMeshes );
    ranges.resize( numMeshes );
    buildInfos.resize( numMeshes );
    sizeInfos.resize( numMeshes );

    for( size_t i = 0; i < numMeshes; ++i )
    {
        PrimitiveModel &pm = m_pRenderModel->m_primitiveModels[i];

        VkAccelerationStructureGeometryKHR &geometry = geometries[i];
        VkAccelerationStructureBuildRangeInfoKHR &range = ranges[i];
        VkAccelerationStructureBuildGeometryInfoKHR &buildInfo = buildInfos[i];
        VkAccelerationStructureBuildSizesInfoKHR &sizeInfo = sizeInfos[i];

        geometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
        geometry.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
        geometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
        geometry.geometry.triangles.sType =
            VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
        geometry.geometry.triangles.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
        geometry.geometry.triangles.vertexData.deviceAddress = pm.pVertexBuffer->getDeviceAddress();
        geometry.geometry.triangles.vertexStride = sizeof( glm::vec3 );
        geometry.geometry.triangles.maxVertex = pm.m_nrVertices;
        geometry.geometry.triangles.indexData.deviceAddress = pm.pIndexBuffer->getDeviceAddress();
        geometry.geometry.triangles.indexType = VK_INDEX_TYPE_UINT16;

        range.primitiveCount = pm.m_nrFaces;

        buildInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
        buildInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
        buildInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        buildInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
        buildInfo.geometryCount = 1;
        buildInfo.pGeometries = &geometry;

        sizeInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;

        vkGetAccelerationStructureBuildSizesKHR(
            m_pDevice->device(),
            VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
            &buildInfo,
            &range.primitiveCount,
            &sizeInfo );
    }

    VkDeviceSize maximumBlasSize = 0;
    for( const auto& sizeInfo : sizeInfos )
    {
        maximumBlasSize = Max( sizeInfo.buildScratchSize, maximumBlasSize );
    }

    std::shared_ptr<Buffer> scratchBuffer;

    scratchBuffer = Buffer::create(
                        m_pDevice,
                        nullptr,
                        maximumBlasSize,
                        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );

    VkCommandBuffer commandBuffer = m_pDevice->beginSingleTimeCommands();

    VkMemoryBarrier memoryBarrier = {};
    memoryBarrier.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER;
    memoryBarrier.srcAccessMask = VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
    memoryBarrier.dstAccessMask = VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_KHR;

    m_offscreenRenderPass.m_bottomLevelAS.resize( numMeshes );
    for( size_t i = 0; i < numMeshes; ++i )
    {
        PrimitiveModel &pm = m_pRenderModel->m_primitiveModels[i];

        m_offscreenRenderPass.m_bottomLevelAS[i].pBuffer = Buffer::create(
                    m_pDevice,
                    nullptr,
                    sizeInfos[i].accelerationStructureSize,
                    VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                    VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR,
                    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );


        VkAccelerationStructureCreateInfoKHR createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
        createInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
        createInfo.size = sizeInfos[i].accelerationStructureSize;
        createInfo.buffer = m_offscreenRenderPass.m_bottomLevelAS[i].pBuffer->buffer;

        vkCreateAccelerationStructureKHR( m_pDevice->device(),
                                          &createInfo,
                                          nullptr,
                                          &m_offscreenRenderPass.m_bottomLevelAS[i].handle );

        VkAccelerationStructureBuildGeometryInfoKHR &buildInfo = buildInfos[i];
        buildInfo.scratchData.deviceAddress = scratchBuffer->getDeviceAddress();
        buildInfo.srcAccelerationStructure = VK_NULL_HANDLE;
        buildInfo.dstAccelerationStructure = m_offscreenRenderPass.m_bottomLevelAS[i].handle;

        const VkAccelerationStructureBuildRangeInfoKHR* range[1] = { &ranges[i] };

        vkCmdBuildAccelerationStructuresKHR( commandBuffer, 1, &buildInfo, range );

        // guard our scratch buffer
        vkCmdPipelineBarrier( commandBuffer,
                              VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR,
                              VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR,
                              0, 1, &memoryBarrier, 0, nullptr, 0, nullptr );
    }

    m_pDevice->endSingleTimeCommands( commandBuffer );

    // get handles
    for( size_t i = 0; i < numMeshes; ++i )
    {
        VkAccelerationStructureDeviceAddressInfoKHR addressInfo = {};
        addressInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
        addressInfo.accelerationStructure = m_offscreenRenderPass.m_bottomLevelAS[i].handle;
        m_offscreenRenderPass.m_bottomLevelAS[i].address = vkGetAccelerationStructureDeviceAddressKHR(
                    m_pDevice->device(),
                    &addressInfo );
    }
}

/**************************************************************************************************/
void RtRenderer::createTopLevelAccelerationStructure()
{
    const VkTransformMatrixKHR transform =
    {
        {
            {1.0f, 0.0f, 0.0f, 0.0f},
            {0.0f, 1.0f, 0.0f, 0.0f},
            {0.0f, 0.0f, 1.0f, 0.0f}
        }
    };

    const size_t numMeshes = m_pRenderModel->m_primitiveModels.size();

    std::vector<VkAccelerationStructureInstanceKHR> instances;

    instances.resize( numMeshes );

    for( size_t i = 0; i < numMeshes; ++i )
    {
        VkAccelerationStructureInstanceKHR& instance = instances[i];
        instance.transform = transform;
        instance.instanceCustomIndex =  i;
        instance.mask = 0xff;
        instance.instanceShaderBindingTableRecordOffset = 0;
        instance.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR;
        instance.accelerationStructureReference = m_offscreenRenderPass.m_bottomLevelAS[i].address;
    }
    std::shared_ptr<Buffer> pInstancesBuffer;
    pInstancesBuffer = Buffer::create(
                           m_pDevice,
                           reinterpret_cast<uint8_t *>( instances.data() ),
                           instances.size() * sizeof( VkAccelerationStructureInstanceKHR ),
                           VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                           VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );

    VkAccelerationStructureGeometryInstancesDataKHR tlasInstancesInfo = {};
    tlasInstancesInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
    tlasInstancesInfo.data.deviceAddress = pInstancesBuffer->getDeviceAddress();

    VkAccelerationStructureGeometryKHR  tlasGeoInfo = {};
    tlasGeoInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    tlasGeoInfo.geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    tlasGeoInfo.geometry.instances = tlasInstancesInfo;

    VkAccelerationStructureBuildGeometryInfoKHR buildInfo = {};
    buildInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    buildInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    buildInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    buildInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    buildInfo.geometryCount = 1;
    buildInfo.pGeometries = &tlasGeoInfo;


    const uint32_t numInstances = static_cast<uint32_t>( instances.size() );

    VkAccelerationStructureBuildSizesInfoKHR sizeInfo{};
    sizeInfo.sType =  VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    vkGetAccelerationStructureBuildSizesKHR(
        m_pDevice->device(),
        VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &buildInfo, &numInstances, &sizeInfo );

    m_offscreenRenderPass.m_topLevelAS.pBuffer = Buffer::create(
                m_pDevice,
                nullptr,
                sizeInfo.accelerationStructureSize,
                VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );

    VkAccelerationStructureCreateInfoKHR createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    createInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    createInfo.size = sizeInfo.accelerationStructureSize;
    createInfo.buffer = m_offscreenRenderPass.m_topLevelAS.pBuffer->buffer;

    VK_CHECK_RESULT( vkCreateAccelerationStructureKHR(
                         m_pDevice->device(),
                         &createInfo, nullptr,
                         &m_offscreenRenderPass.m_topLevelAS.handle ) );

    std::shared_ptr<Buffer> scratchBuffer;

    scratchBuffer = Buffer::create(
                        m_pDevice,
                        nullptr,
                        sizeInfo.buildScratchSize,
                        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );

    buildInfo.scratchData.deviceAddress = scratchBuffer->getDeviceAddress();
    buildInfo.srcAccelerationStructure = VK_NULL_HANDLE;
    buildInfo.dstAccelerationStructure = m_offscreenRenderPass.m_topLevelAS.handle;

    VkCommandBuffer commandBuffer = m_pDevice->beginSingleTimeCommands();

    VkAccelerationStructureBuildRangeInfoKHR range = {};
    range.primitiveCount = numInstances;

    const VkAccelerationStructureBuildRangeInfoKHR * ranges[1] = { &range };

    vkCmdBuildAccelerationStructuresKHR( commandBuffer, 1, &buildInfo, ranges );

    m_pDevice->endSingleTimeCommands( commandBuffer );

    VkAccelerationStructureDeviceAddressInfoKHR addressInfo = {};
    addressInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    addressInfo.accelerationStructure = m_offscreenRenderPass.m_topLevelAS.handle;
    m_offscreenRenderPass.m_topLevelAS.address = vkGetAccelerationStructureDeviceAddressKHR(
                m_pDevice->device(),
                &addressInfo );
}

/**************************************************************************************************/
void RtRenderer::createStorageImage()
{
    m_offscreenRenderPass.pStorageImage = Image::create(
            m_pDevice,
            m_pSwapchain->width(),
            m_pSwapchain->height(),
            1,
            1,
            VK_SAMPLE_COUNT_1_BIT,
            m_pSwapchain->swapchainImageFormat,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            VK_IMAGE_ASPECT_COLOR_BIT );

    VkCommandBuffer cmdBuffer = m_pDevice->beginSingleTimeCommands();

    VkImageSubresourceRange subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

    m_offscreenRenderPass.pStorageImage->setLayout(
        cmdBuffer,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_GENERAL,
        subresourceRange );

    m_pDevice->endSingleTimeCommands( cmdBuffer );
}

/**************************************************************************************************/
void RtRenderer::createRayTracingPipeline()
{
    const uint32_t nr_meshes = m_pRenderModel->m_primitiveModels.size();

    m_offscreenRenderPass.m_descriptorSetLayouts.resize( SWS_NUM_SETS );

    // set 0
    VkDescriptorSetLayoutBinding accelerationStructureLayoutBinding{};
    accelerationStructureLayoutBinding.binding = SWS_SCENE_AS_BINDING;
    accelerationStructureLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    accelerationStructureLayoutBinding.descriptorCount = 1;
    accelerationStructureLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR;

    VkDescriptorSetLayoutBinding resultImageLayoutBinding{};
    resultImageLayoutBinding.binding = SWS_RESULT_IMAGE_BINDING;
    resultImageLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    resultImageLayoutBinding.descriptorCount = 1;
    resultImageLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR;

    VkDescriptorSetLayoutBinding uniformBufferBinding{};
    uniformBufferBinding.binding = SWS_CAMDATA_BINDING;
    uniformBufferBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uniformBufferBinding.descriptorCount = 1;
    uniformBufferBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR;

    std::vector<VkDescriptorSetLayoutBinding> bindings(
    {
        accelerationStructureLayoutBinding,
        resultImageLayoutBinding,
        uniformBufferBinding
    } );

    VkDescriptorSetLayoutCreateInfo descriptorSetlayoutCI{};
    descriptorSetlayoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetlayoutCI.bindingCount = static_cast<uint32_t>( bindings.size() );
    descriptorSetlayoutCI.pBindings = bindings.data();
    VK_CHECK_RESULT( vkCreateDescriptorSetLayout(
                         m_pDevice->device(),
                         &descriptorSetlayoutCI,
                         nullptr,
                         &m_offscreenRenderPass.m_descriptorSetLayouts[SWS_SCENE_AS_SET] ) );

    // Set 1
    const VkDescriptorBindingFlags flag = VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT;
    VkDescriptorSetLayoutBindingFlagsCreateInfo bindingFlags{};
    bindingFlags.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
    bindingFlags.pNext = nullptr;
    bindingFlags.pBindingFlags = &flag;
    bindingFlags.bindingCount = 1;

    VkDescriptorSetLayoutBinding ssboBinding{};
    ssboBinding.binding = 0;
    ssboBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    ssboBinding.descriptorCount = nr_meshes;
    ssboBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
    ssboBinding.pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutCreateInfo set1LayoutInfo{};
    set1LayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    set1LayoutInfo.pNext = &bindingFlags;
    set1LayoutInfo.flags = 0;
    set1LayoutInfo.bindingCount = 1;
    set1LayoutInfo.pBindings = &ssboBinding;

    VK_CHECK_RESULT( vkCreateDescriptorSetLayout(
                         m_pDevice->device(),
                         &set1LayoutInfo,
                         nullptr,
                         &m_offscreenRenderPass.m_descriptorSetLayouts[SWS_MATIDS_SET] ) );

    // Set 2
    VK_CHECK_RESULT( vkCreateDescriptorSetLayout(
                         m_pDevice->device(),
                         &set1LayoutInfo,
                         nullptr,
                         &m_offscreenRenderPass.m_descriptorSetLayouts[SWS_ATTRIBS_SET] ) );

    // Set 3
    VK_CHECK_RESULT( vkCreateDescriptorSetLayout(
                         m_pDevice->device(),
                         &set1LayoutInfo,
                         nullptr,
                         &m_offscreenRenderPass.m_descriptorSetLayouts[SWS_FACES_SET] ) );

    // Set 4
    VkDescriptorSetLayoutBinding textureBinding{};
    textureBinding.binding = 0;
    textureBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    textureBinding.descriptorCount = nr_meshes;
    textureBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
    set1LayoutInfo.pBindings = &textureBinding;

    VK_CHECK_RESULT( vkCreateDescriptorSetLayout(
                         m_pDevice->device(),
                         &set1LayoutInfo,
                         nullptr,
                         &m_offscreenRenderPass.m_descriptorSetLayouts[SWS_TEXTURES_SET] ) );


    // pipeline
    VkPipelineLayoutCreateInfo pipelineLayoutCI{};
    pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCI.setLayoutCount = m_offscreenRenderPass.m_descriptorSetLayouts.size();
    pipelineLayoutCI.pSetLayouts = m_offscreenRenderPass.m_descriptorSetLayouts.data();
    VK_CHECK_RESULT( vkCreatePipelineLayout(
                         m_pDevice->device(),
                         &pipelineLayoutCI,
                         nullptr,
                         &m_offscreenRenderPass.m_pipelineLayout ) );

    /*
        Setup ray tracing shader groups
    */
    std::vector<VkPipelineShaderStageCreateInfo> shaderStages;

    // Ray generation group
    VkShaderModule raygenShaderModule = VK_NULL_HANDLE;
    {
        auto raygenShaderCode = m_pDevice->readFile( "raygen.rgen.spv" );
        raygenShaderModule = m_pDevice->createShaderModule( raygenShaderCode );
        VkPipelineShaderStageCreateInfo shaderStageCI{};
        shaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStageCI.stage = VK_SHADER_STAGE_RAYGEN_BIT_KHR;
        shaderStageCI.module = raygenShaderModule;
        shaderStageCI.pName = "main";
        shaderStages.push_back( shaderStageCI );

        VkRayTracingShaderGroupCreateInfoKHR shaderGroup{};
        shaderGroup.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
        shaderGroup.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
        shaderGroup.generalShader = static_cast<uint32_t>( shaderStages.size() ) - 1;
        shaderGroup.closestHitShader = VK_SHADER_UNUSED_KHR;
        shaderGroup.anyHitShader = VK_SHADER_UNUSED_KHR;
        shaderGroup.intersectionShader = VK_SHADER_UNUSED_KHR;
        m_offscreenRenderPass.m_shaderGroups.push_back( shaderGroup );
    }

    // Miss group
    VkShaderModule missShaderModule = VK_NULL_HANDLE;
    {
        auto missShaderCode = m_pDevice->readFile( "miss.rmiss.spv" );
        missShaderModule = m_pDevice->createShaderModule( missShaderCode );
        VkPipelineShaderStageCreateInfo shaderStageCI{};
        shaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStageCI.stage = VK_SHADER_STAGE_MISS_BIT_KHR;
        shaderStageCI.module = missShaderModule;
        shaderStageCI.pName = "main";
        shaderStages.push_back( shaderStageCI );

        VkRayTracingShaderGroupCreateInfoKHR shaderGroup{};
        shaderGroup.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
        shaderGroup.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
        shaderGroup.generalShader = static_cast<uint32_t>( shaderStages.size() ) - 1;
        shaderGroup.closestHitShader = VK_SHADER_UNUSED_KHR;
        shaderGroup.anyHitShader = VK_SHADER_UNUSED_KHR;
        shaderGroup.intersectionShader = VK_SHADER_UNUSED_KHR;
        m_offscreenRenderPass.m_shaderGroups.push_back( shaderGroup );
    }

    // Closest hit group
    VkShaderModule hitShaderModule = VK_NULL_HANDLE;
    {
        auto hitShaderCode = m_pDevice->readFile( "closesthit.rchit.spv" );
        hitShaderModule = m_pDevice->createShaderModule( hitShaderCode );
        VkPipelineShaderStageCreateInfo shaderStageCI{};
        shaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStageCI.stage = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
        shaderStageCI.module = hitShaderModule;
        shaderStageCI.pName = "main";
        shaderStages.push_back( shaderStageCI );

        VkRayTracingShaderGroupCreateInfoKHR shaderGroup{};
        shaderGroup.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
        shaderGroup.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR;
        shaderGroup.generalShader = VK_SHADER_UNUSED_KHR;
        shaderGroup.closestHitShader = static_cast<uint32_t>( shaderStages.size() ) - 1;
        shaderGroup.anyHitShader = VK_SHADER_UNUSED_KHR;
        shaderGroup.intersectionShader = VK_SHADER_UNUSED_KHR;
        m_offscreenRenderPass.m_shaderGroups.push_back( shaderGroup );
    }

    /*
        Create the ray tracing pipeline
    */
    VkRayTracingPipelineCreateInfoKHR rayTracingPipelineCI{};
    rayTracingPipelineCI.sType = VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR;
    rayTracingPipelineCI.stageCount = shaderStages.size();
    rayTracingPipelineCI.pStages = shaderStages.data();
    rayTracingPipelineCI.groupCount = m_offscreenRenderPass.m_shaderGroups.size();
    rayTracingPipelineCI.pGroups = m_offscreenRenderPass.m_shaderGroups.data();
    rayTracingPipelineCI.maxPipelineRayRecursionDepth = 31;
    rayTracingPipelineCI.layout = m_offscreenRenderPass.m_pipelineLayout;
    VK_CHECK_RESULT( vkCreateRayTracingPipelinesKHR(
                         m_pDevice->device(),
                         VK_NULL_HANDLE,
                         VK_NULL_HANDLE, 1,
                         &rayTracingPipelineCI,
                         nullptr,
                         &m_offscreenRenderPass.m_pipeline ) );

    vkDestroyShaderModule( m_pDevice->device(), raygenShaderModule, nullptr );
    vkDestroyShaderModule( m_pDevice->device(), hitShaderModule, nullptr );
    vkDestroyShaderModule( m_pDevice->device(), missShaderModule, nullptr );
}

/**************************************************************************************************/
void RtRenderer::createShaderBindingTable()
{
    auto pipelineProperties = m_pDevice->rtPipelineProperties();

    const uint32_t handleSize = pipelineProperties.shaderGroupHandleSize;
    const uint32_t handleSizeAligned = alignedSize(
                                           pipelineProperties.shaderGroupHandleSize,
                                           pipelineProperties.shaderGroupHandleAlignment );
    const uint32_t groupCount = static_cast<uint32_t>( m_offscreenRenderPass.m_shaderGroups.size() );
    const uint32_t sbtSize = groupCount * handleSizeAligned;

    std::vector<uint8_t> shaderHandleStorage( sbtSize );
    VK_CHECK_RESULT( vkGetRayTracingShaderGroupHandlesKHR(
                         m_pDevice->device(),
                         m_offscreenRenderPass.m_pipeline,
                         0,
                         groupCount,
                         sbtSize,
                         shaderHandleStorage.data() ) );

    const VkBufferUsageFlags bufferUsageFlags =
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
        VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR ;
    const VkMemoryPropertyFlags memoryUsageFlags =
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

    m_offscreenRenderPass.pRaygenShaderBindingTableBuffer = Buffer::create(
                m_pDevice,
                shaderHandleStorage.data() + 0 * handleSize,
                handleSize,
                bufferUsageFlags,
                memoryUsageFlags );

    m_offscreenRenderPass.pMissShaderBindingTableBuffer = Buffer::create(
                m_pDevice,
                shaderHandleStorage.data() + 1 * handleSize,
                handleSize,
                bufferUsageFlags,
                memoryUsageFlags );

    m_offscreenRenderPass.pHitShaderBindingTableBuffer = Buffer::create(
                m_pDevice,
                shaderHandleStorage.data() + 2 * handleSize,
                handleSize,
                bufferUsageFlags,
                memoryUsageFlags );
}

/**************************************************************************************************/
void RtRenderer::createDescriptorSets()
{
    std::vector<VkDescriptorPoolSize> poolSizes =
    {
        { VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, 1 },
    };
    VkDescriptorPoolCreateInfo descriptorPoolCreateInfo{};
    descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCreateInfo.poolSizeCount = poolSizes.size();
    descriptorPoolCreateInfo.pPoolSizes = poolSizes.data();
    descriptorPoolCreateInfo.maxSets = SWS_NUM_SETS;
    VK_CHECK_RESULT( vkCreateDescriptorPool(
                         m_pDevice->device(),
                         &descriptorPoolCreateInfo, nullptr, &m_descriptorPool ) );

    m_offscreenRenderPass.m_descriptorSets.resize( SWS_NUM_SETS );

    uint32_t nr_meshes =  m_pRenderModel->m_primitiveModels.size();
    Array<uint32_t> variableDescriptorCounts(
    {
        0,      // n/a
        nr_meshes,      // per-face material IDs for each mesh
        nr_meshes,      // vertex attribs for each mesh
        nr_meshes,      // faces buffer for each mesh
        nr_meshes       // textures for each material
    } );

    VkDescriptorSetVariableDescriptorCountAllocateInfo dci{};
    dci.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO;
    dci.descriptorSetCount = SWS_NUM_SETS;
    dci.pDescriptorCounts = variableDescriptorCounts.data();

    VkDescriptorSetAllocateInfo dstai{};
    dstai.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    dstai.pNext = &dci;
    dstai.descriptorPool = m_descriptorPool;
    dstai.descriptorSetCount = SWS_NUM_SETS;
    dstai.pSetLayouts = m_offscreenRenderPass.m_descriptorSetLayouts.data();

    VK_CHECK_RESULT( vkAllocateDescriptorSets(
                         m_pDevice->device(),
                         &dstai,
                         m_offscreenRenderPass.m_descriptorSets.data() ) );

    // Set 0
    VkWriteDescriptorSetAccelerationStructureKHR dasi{};
    dasi.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
    dasi.accelerationStructureCount = 1;
    dasi.pAccelerationStructures = &m_offscreenRenderPass.m_topLevelAS.handle;

    VkWriteDescriptorSet accelerationStructureWrite{};
    accelerationStructureWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    accelerationStructureWrite.pNext = &dasi;
    accelerationStructureWrite.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    accelerationStructureWrite.dstSet = m_offscreenRenderPass.m_descriptorSets[SWS_SCENE_AS_SET];
    accelerationStructureWrite.dstBinding = SWS_SCENE_AS_BINDING;
    accelerationStructureWrite.descriptorCount = 1;

    // Set 0
    VkDescriptorImageInfo storageImageDescriptor{};
    storageImageDescriptor.sampler = VK_NULL_HANDLE;
    storageImageDescriptor.imageView = m_offscreenRenderPass.pStorageImage->view;
    storageImageDescriptor.imageLayout = VK_IMAGE_LAYOUT_GENERAL;

    VkWriteDescriptorSet resultImageWrite{};
    resultImageWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    resultImageWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    resultImageWrite.dstSet = m_offscreenRenderPass.m_descriptorSets[SWS_RESULT_IMAGE_SET];
    resultImageWrite.dstBinding = SWS_RESULT_IMAGE_BINDING;
    resultImageWrite.descriptorCount = 1;
    resultImageWrite.pImageInfo = &storageImageDescriptor;

    // Set 0
    VkWriteDescriptorSet uniformBufferWrite{};
    uniformBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    uniformBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uniformBufferWrite.dstSet = m_offscreenRenderPass.m_descriptorSets[SWS_CAMDATA_SET];
    uniformBufferWrite.dstBinding = SWS_CAMDATA_BINDING;
    uniformBufferWrite.descriptorCount = 1;
    uniformBufferWrite.pBufferInfo = &m_pRenderModel->pCameraMvpBuffer->descriptor;

    // Set 1
    std::vector<VkDescriptorBufferInfo> tmp1;
    for( size_t i = 0; i < m_pRenderModel->m_primitiveModels.size(); ++i )
    {
        auto &pm = m_pRenderModel->m_primitiveModels[i];
        tmp1.push_back( pm.m_matIdBuffer->descriptor );
    }
    VkWriteDescriptorSet matIDsBufferWrite{};
    matIDsBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    matIDsBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    matIDsBufferWrite.dstSet = m_offscreenRenderPass.m_descriptorSets[SWS_MATIDS_SET];
    matIDsBufferWrite.dstBinding = 0;
    matIDsBufferWrite.descriptorCount = tmp1.size();
    matIDsBufferWrite.pBufferInfo = tmp1.data();

    // Set 2
    std::vector<VkDescriptorBufferInfo> tmp2;
    for( size_t i = 0; i < m_pRenderModel->m_primitiveModels.size(); ++i )
    {
        auto &pm = m_pRenderModel->m_primitiveModels[i];
        tmp2.push_back( pm.m_attribBuffer->descriptor );
    }
    VkWriteDescriptorSet attribsBufferWrite{};
    attribsBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    attribsBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    attribsBufferWrite.dstSet = m_offscreenRenderPass.m_descriptorSets[SWS_ATTRIBS_SET];
    attribsBufferWrite.dstBinding = 0;
    attribsBufferWrite.descriptorCount = tmp2.size();
    attribsBufferWrite.pBufferInfo = tmp2.data();

    // Set 3
    std::vector<VkDescriptorBufferInfo> tmp3;
    for( size_t i = 0; i < m_pRenderModel->m_primitiveModels.size(); ++i )
    {
        auto &pm = m_pRenderModel->m_primitiveModels[i];
        tmp3.push_back( pm.m_faceBuffer->descriptor );
    }
    VkWriteDescriptorSet facesBufferWrite{};
    facesBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    facesBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    facesBufferWrite.dstSet = m_offscreenRenderPass.m_descriptorSets[SWS_FACES_SET];
    facesBufferWrite.dstBinding = 0;
    facesBufferWrite.descriptorCount = tmp3.size();
    facesBufferWrite.pBufferInfo = tmp3.data();

    // Set 4
    std::vector<VkDescriptorImageInfo> tmp4;
    for( size_t i = 0; i < m_pRenderModel->m_primitiveModels.size(); ++i )
    {
        auto &pm = m_pRenderModel->m_primitiveModels[i];
        tmp4.push_back( pm.m_albedoImage->descriptor );
    }
    VkWriteDescriptorSet texturesBufferWrite{};
    texturesBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    texturesBufferWrite.dstSet = m_offscreenRenderPass.m_descriptorSets[SWS_TEXTURES_SET];
    texturesBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    texturesBufferWrite.dstBinding = 0;
    texturesBufferWrite.descriptorCount = tmp4.size();
    texturesBufferWrite.pImageInfo = tmp4.data();

    std::vector<VkWriteDescriptorSet> writeDescriptorSets =
    {
        accelerationStructureWrite,
        resultImageWrite,
        uniformBufferWrite,
        matIDsBufferWrite,
        attribsBufferWrite,
        facesBufferWrite,
        texturesBufferWrite
    };
    vkUpdateDescriptorSets( m_pDevice->device(),
                            writeDescriptorSets.size(),
                            writeDescriptorSets.data(),
                            0,
                            VK_NULL_HANDLE );
}

/**************************************************************************************************/
void RtRenderer::initSyncObjects()
{
    m_offscreenRenderPass.m_commandFences.resize( m_pSwapchain->nrImages() );
    for( size_t i = 0; i < m_pSwapchain->nrImages(); ++i )
    {
        VkFenceCreateInfo fenceCI{};
        fenceCI.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fenceCI.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        vkCreateFence( m_pDevice->device(),
                       &fenceCI, nullptr,
                       &m_offscreenRenderPass.m_commandFences[i] );
    }
}
/**************************************************************************************************/
void RtRenderer::destroySyncObjects()
{
    for( size_t i = 0; i < m_pSwapchain->nrImages(); ++i )
    {
        vkDestroyFence( m_pDevice->device(), m_offscreenRenderPass.m_commandFences[i], nullptr );
    }
}

/**************************************************************************************************/
void RtRenderer::initCommandBuffers()
{
    m_offscreenRenderPass.m_commandBuffers.resize( m_pSwapchain->nrImages() );

    VkCommandBufferAllocateInfo allocateInfo{};
    allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocateInfo.commandPool = m_pDevice->commandPool();
    allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocateInfo.commandBufferCount = m_pSwapchain->nrImages();
    VK_CHECK_RESULT( vkAllocateCommandBuffers(
                         m_pDevice->device(),
                         &allocateInfo,
                         m_offscreenRenderPass.m_commandBuffers.data() ) );

    /****************/

    VkCommandBufferBeginInfo cmdBufInfo{};
    cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    VkImageSubresourceRange subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

    for( size_t i = 0; i < m_pSwapchain->nrImages(); ++i )
    {
        VkCommandBuffer &commandBuffer = m_offscreenRenderPass.m_commandBuffers[i];

        VK_CHECK_RESULT( vkBeginCommandBuffer( commandBuffer, &cmdBufInfo ) );

        /*
            Setup the buffer regions pointing to the shaders in our shader binding table
        */

        const uint32_t handleSizeAligned = alignedSize(
                                               m_pDevice->rtPipelineProperties().shaderGroupHandleSize,
                                               m_pDevice->rtPipelineProperties().shaderGroupHandleAlignment );

        VkStridedDeviceAddressRegionKHR raygenShaderSbtEntry{};
        raygenShaderSbtEntry.deviceAddress =
            m_offscreenRenderPass.pRaygenShaderBindingTableBuffer->getDeviceAddress();
        raygenShaderSbtEntry.stride = handleSizeAligned;
        raygenShaderSbtEntry.size = handleSizeAligned;

        VkStridedDeviceAddressRegionKHR missShaderSbtEntry{};
        missShaderSbtEntry.deviceAddress =
            m_offscreenRenderPass.pMissShaderBindingTableBuffer->getDeviceAddress();
        missShaderSbtEntry.stride = handleSizeAligned;
        missShaderSbtEntry.size = handleSizeAligned;

        VkStridedDeviceAddressRegionKHR hitShaderSbtEntry{};
        hitShaderSbtEntry.deviceAddress =
            m_offscreenRenderPass.pHitShaderBindingTableBuffer->getDeviceAddress();
        hitShaderSbtEntry.stride = handleSizeAligned;
        hitShaderSbtEntry.size = handleSizeAligned;

        VkStridedDeviceAddressRegionKHR callableShaderSbtEntry{};

        /*
            Dispatch the ray tracing commands
        */
        vkCmdBindPipeline( commandBuffer,
                           VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR,
                           m_offscreenRenderPass.m_pipeline );
        vkCmdBindDescriptorSets( commandBuffer,
                                 VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR,
                                 m_offscreenRenderPass.m_pipelineLayout,
                                 0,
                                 m_offscreenRenderPass.m_descriptorSets.size(),
                                 m_offscreenRenderPass.m_descriptorSets.data(),
                                 0,
                                 0 );

        vkCmdTraceRaysKHR( commandBuffer,
                           &raygenShaderSbtEntry,
                           &missShaderSbtEntry,
                           &hitShaderSbtEntry,
                           &callableShaderSbtEntry,
                           m_pSwapchain->width(),
                           m_pSwapchain->height(),
                           1 );

        /*
            Copy ray tracing output to swap chain image
        */

        // Prepare current swap chain image as transfer destination
        m_pSwapchain->m_swapchainImages[i]->setLayout(
            commandBuffer,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            subresourceRange );

        // Prepare ray tracing output image as transfer source
        m_offscreenRenderPass.pStorageImage->setLayout(
            commandBuffer,
            VK_IMAGE_LAYOUT_GENERAL,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            subresourceRange );

        VkImageCopy copyRegion{};
        copyRegion.srcSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };
        copyRegion.srcOffset = { 0, 0, 0 };
        copyRegion.dstSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };
        copyRegion.dstOffset = { 0, 0, 0 };
        copyRegion.extent = { m_pSwapchain->width(), m_pSwapchain->height(), 1 };
        vkCmdCopyImage( commandBuffer,
                        m_offscreenRenderPass.pStorageImage->image,
                        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                        m_pSwapchain->m_swapchainImages[i]->image,
                        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                        1,
                        &copyRegion );

        // Transition swap chain image back for presentation
        m_pSwapchain->m_swapchainImages[i]->setLayout(
            commandBuffer,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            subresourceRange );

        // Transition ray tracing output image back to general layout
        m_offscreenRenderPass.pStorageImage->setLayout(
            commandBuffer,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            VK_IMAGE_LAYOUT_GENERAL,
            subresourceRange );

        VK_CHECK_RESULT( vkEndCommandBuffer( commandBuffer ) );
    }
}

/**************************************************************************************************/
void RtRenderer::destroyCommandBuffers()
{
}

/**************************************************************************************************/
void RtRenderer::render( uint32_t currentFrame )
{
    assertmsg( currentFrame < m_offscreenRenderPass.m_commandBuffers.size(), "cmdBuffers empty" );

    static float prevTime = glfwGetTime();
    static float curTime = glfwGetTime();
    static float dt = curTime - prevTime;

    curTime = glfwGetTime();
    dt = curTime - prevTime;
    prevTime = curTime;

    m_pRenderModel->updateCamera( dt );

    // Swapchain pass
    {
        VkSubmitInfo submitInfo{};

        std::array<VkCommandBuffer, 1> cmdBuffers{m_offscreenRenderPass.m_commandBuffers[currentFrame]};

        VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT};
        std::array<VkSemaphore, 1> waitSemaphores{m_pSwapchain->imageAvailableSemaphore};
        std::array<VkSemaphore, 1> signalSemaphores{m_pSwapchain->renderFinishedSemaphore};

        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = waitSemaphores.size();
        submitInfo.pWaitSemaphores = waitSemaphores.data();
        submitInfo.pWaitDstStageMask = waitStages;
        submitInfo.signalSemaphoreCount = signalSemaphores.size();
        submitInfo.pSignalSemaphores = signalSemaphores.data();
        submitInfo.commandBufferCount = cmdBuffers.size();
        submitInfo.pCommandBuffers = cmdBuffers.data();

        VkFence waitFence = m_offscreenRenderPass.m_commandFences[currentFrame];
        VK_CHECK_RESULT( vkWaitForFences( m_pDevice->device(), 1, &waitFence, VK_TRUE, UINT64_MAX ) );
        VK_CHECK_RESULT( vkResetFences( m_pDevice->device(), 1, &waitFence ) );
        VK_CHECK_RESULT( vkQueueSubmit( m_pDevice->graphicsQueue(),
                                        1,
                                        &submitInfo,
                                        waitFence ) );
    }
}

