#pragma once
#include <vulkan/vulkan.hpp>
#include <cassert>
#include <iostream>
#include <sstream>

#define VK_CHECK_RESULT(f)                                                                              \
{                                                                                                       \
    VkResult res = (f);                                                                                 \
    if (res != VK_SUCCESS)                                                                              \
    {                                                                                                   \
        std::cerr << "Fatal : VkResult is \"" << res << "\" in " << __FILE__ << " at line " << __LINE__ << "\n"; \
        assert(res == VK_SUCCESS);                                                                      \
    }                                                                                                   \
}

#ifndef NDEBUG
void __assertmsg( const char* expr_str, bool expr, const char* file, int line, const char* msg );
#define assertmsg(Expr, Msg) \
    __assertmsg(#Expr, Expr, __FILE__, __LINE__, Msg)
#else
#define assertmsg(Expr, Msg) ;
#endif

class Instance;
class Camera;
class PhysicalDevice;
class Device;
class Window;
class Swapchain;
class GlRenderer;
class GlRenderModel;
class RtRenderer;
class RtRenderModel;
class TriRenderer;
class TriRenderModel;

class App
{
public:
    App();
    ~App();
    void run();

private:
    bool m_exitRequested = false;

    std::shared_ptr<Instance> m_pInstance;
    std::shared_ptr<Camera> m_pCamera;
    std::shared_ptr<PhysicalDevice> m_pPhysicalDevice;
    std::shared_ptr<Device> m_pDevice;
    std::shared_ptr<Window> m_pWindow;
    std::shared_ptr<Swapchain> m_pSwapchain;

    std::shared_ptr<TriRenderer> m_pTriRenderer;
    std::shared_ptr<TriRenderModel> m_pTriRenderModel;

    std::shared_ptr<GlRenderer> m_pGlRenderer;
    std::shared_ptr<GlRenderModel> m_pGlRenderModel;

    std::shared_ptr<RtRenderer> m_pRtRenderer;
    std::shared_ptr<RtRenderModel> m_pRtRenderModel;

    uint32_t m_currentFrame = 0;
};
