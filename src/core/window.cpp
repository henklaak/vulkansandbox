#include "window.h"
#include <glm/glm.hpp>
#include "vulkancore.h"
#include "device.h"
#include "instance.h"
#include "camera.h"

static const float sRotateSpeed = 0.25f;

/**************************************************************************************************/
Window::Window( CameraPtr pCamera )
    : m_pCamera( pCamera )
{
    glfwInit();
    glfwWindowHint( GLFW_CLIENT_API, GLFW_NO_API );

    m_window = glfwCreateWindow( m_width, m_height, "Vulkan", nullptr, nullptr );

    glfwSetWindowUserPointer( m_window, this );

    glfwSetKeyCallback( m_window, []( GLFWwindow * wnd, int key, int scancode, int action, int mods )
    {
        Window* pWindow = reinterpret_cast<Window*>( glfwGetWindowUserPointer( wnd ) );
        pWindow->onKey( key, scancode, action, mods );
    } );
    glfwSetMouseButtonCallback( m_window, []( GLFWwindow * wnd, int button, int action, int mods )
    {
        Window* pWindow = reinterpret_cast<Window*>( glfwGetWindowUserPointer( wnd ) );
        pWindow->onMouseButton( button, action, mods );
    } );
    glfwSetCursorPosCallback( m_window, []( GLFWwindow * wnd, double x, double y )
    {
        Window* pWindow = reinterpret_cast<Window*>( glfwGetWindowUserPointer( wnd ) );
        pWindow->onMouseMove( static_cast<float>( x ), static_cast<float>( y ) );
    } );
    glfwSetFramebufferSizeCallback( m_window, []( GLFWwindow * wnd, int width, int height )
    {
        Window* pWindow = reinterpret_cast<Window*>( glfwGetWindowUserPointer( wnd ) );
        pWindow->onMouseMove( width, height );
    } );
}

/**************************************************************************************************/
Window::~Window()
{
    glfwDestroyWindow( m_window );
    glfwTerminate();
}

/**************************************************************************************************/
std::vector< std::string > Window::getRequiredInstanceExtensions()
{
    std::vector< std::string > instanceExtensionList;

    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions( &glfwExtensionCount );

    for( uint32_t i = 0; i < glfwExtensionCount; ++i )
    {
        instanceExtensionList.push_back( std::string( glfwExtensions[i] ) );
    }

    return instanceExtensionList;
}


/**************************************************************************************************/
VkExtent2D Window::extent() const
{
    int width = 0;
    int height = 0;
    glfwGetWindowSize( m_window, &width, &height );

    VkExtent2D extent{};
    extent.width = width;
    extent.height = height;
    return extent;
}
/**************************************************************************************************/
void Window::onMouseMove( const float x, const float y )
{
    glm::vec2 newPos( x, y );
    glm::vec2 delta = mCursorPos - newPos;

    if( mLMBDown )
    {
        m_pCamera->rotate( delta.x * sRotateSpeed, delta.y * sRotateSpeed );
    }

    mCursorPos = newPos;
}

/**************************************************************************************************/
void Window::onMouseButton( const int button, const int action, const int mods )
{
    if( 0 == button && GLFW_PRESS == action )
    {
        mLMBDown = true;
    }
    else if( 0 == button && GLFW_RELEASE == action )
    {
        mLMBDown = false;
    }
}

/**************************************************************************************************/
void Window::onKey( const int key, const int scancode, const int action, const int mods )
{
    if( GLFW_PRESS == action )
    {
        switch( key )
        {
        case GLFW_KEY_W:
            mWKeyDown = true;
            break;
        case GLFW_KEY_A:
            mAKeyDown = true;
            break;
        case GLFW_KEY_S:
            mSKeyDown = true;
            break;
        case GLFW_KEY_D:
            mDKeyDown = true;
            break;

        case GLFW_KEY_LEFT_SHIFT:
        case GLFW_KEY_RIGHT_SHIFT:
            mShiftDown = true;
            break;
        }
    }
    else if( GLFW_RELEASE == action )
    {
        switch( key )
        {
        case GLFW_KEY_W:
            mWKeyDown = false;
            break;
        case GLFW_KEY_A:
            mAKeyDown = false;
            break;
        case GLFW_KEY_S:
            mSKeyDown = false;
            break;
        case GLFW_KEY_D:
            mDKeyDown = false;
            break;

        case GLFW_KEY_LEFT_SHIFT:
        case GLFW_KEY_RIGHT_SHIFT:
            mShiftDown = false;
            break;
        }
    }
}

/**************************************************************************************************/
void Window::onFrameBufferResize( const int width, const int height )
{

}
