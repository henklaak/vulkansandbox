#pragma once
#include <memory>
#include <vector>

#include <glm/glm.hpp>

extern unsigned char defaultAlbedoMap[4];
extern unsigned char defaultNormalMap[4];
extern unsigned char defaultAmbientOcclusionMap[4];
extern unsigned char defaultEmissiveMap[4];

struct RawVertex
{
    glm::vec3 position{};
    glm::vec3 normal{};
    glm::vec4 tangent{};
    glm::vec2 uv{};
};

struct RawTexture
{
    const uint8_t *rawData = defaultAlbedoMap;
    uint32_t nrcols = 1;
    uint32_t nrrows = 1;
    uint32_t rowstride = 1;
    uint32_t colstride = 1;
};

class RenderModel;
typedef std::shared_ptr<RenderModel> RenderModelPtr;


class RenderModel
{
public:
    RenderModel();
    virtual ~RenderModel() = 0;

    virtual void setCamera( const glm::mat4 &localTransform,
                            double yFov,
                            double aspectRatio,
                            double zNear,
                            double zFar ) = 0;

    virtual void addPrimitive( const glm::mat4 &localTransform,
                               const std::vector<RawVertex> &vertices,
                               const std::vector<uint16_t> &indices,
                               const RawTexture &albedoTexture,
                               const RawTexture &normalTexture,
                               const RawTexture &ambientOcclusionTexture,
                               const RawTexture &emissiveTexture ) = 0;

    virtual void addPointLight( const glm::mat4 &localTransform,
                                const glm::vec3 &color,
                                double intensity ) = 0;
};
