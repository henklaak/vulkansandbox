#include "debugtools.h"
#include <stdlib.h>
#include <iostream>

void inspect( const glm::mat4 &matrix )
{
    return;

    // Blender     Vulkan    -RotX90
    //   x           x
    //   y          -z
    //   z           y

    // Vulkan      Blender    RotX90
    //   x           x
    //   y           z
    //   z          -y
    const glm::mat4 I = glm::mat4( 1.0f );
    const glm::mat4 B2V = glm::rotate( I, glm::radians( -90.0f ), glm::vec3( 1.0f, 0.0f, 0.0f ) );
    const glm::mat4 V2B = glm::inverse( B2V );

    glm::mat4 mat = V2B * matrix;

    glm::quat q = glm::quat(mat);
    glm::vec3 a2 = glm::degrees(glm::eulerAngles(q));
    std::cout << a2 << std::endl;
}
