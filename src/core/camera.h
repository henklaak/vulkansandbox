#pragma once
#include <memory>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

class Camera;
typedef std::shared_ptr<Camera> CameraPtr;


class Camera
{
public:
    Camera();
    virtual ~Camera();

    void rotate( const float angleX, const float angleY );
    void update( float dt );

    void setView( const glm::mat4 &view );
    void setProjection( const glm::mat4 &projection );

    glm::mat4 view() const
    {
        return m_view;
    }
    glm::mat4 projection() const
    {
        return m_projection;
    }

private:
    glm::mat4 m_view;
    glm::mat4 m_projection;

    float m_angleX = 0.0f;
    float m_angleY = 0.0f;
};

