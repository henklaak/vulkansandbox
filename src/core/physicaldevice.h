#pragma once
#include <memory>
#include <optional>
#include <vulkan/vulkan.hpp>

#include "instance.h"
#include "surface.h"

class PhysicalDevice;
typedef std::shared_ptr<PhysicalDevice> PhysicalDevicePtr;



class PhysicalDevice
{
public:
    struct QueueFamilyIndices
    {
        std::optional<uint32_t> graphics;
        std::optional<uint32_t> compute;
        std::optional<uint32_t> transfer;
        std::optional<uint32_t> present;
    };

    PhysicalDevice( InstancePtr pInstance, SurfacePtr pSurface );
    virtual ~PhysicalDevice();

    uint32_t getQueueFamilyIndex( VkQueueFlagBits queueFlags );
    uint32_t findMemoryType( uint32_t typeFilter, VkMemoryPropertyFlags properties ) const;
    VkFormat findSupportedFormat( const std::vector<VkFormat>& candidates,
                                  VkImageTiling tiling,
                                  VkFormatFeatureFlags features ) const;
    VkFormat findDepthFormat() const;

    VkPhysicalDevice physicalDevice() const
    {
        return m_physicalDevice;
    }
    VkPhysicalDeviceFeatures2 availableDeviceFeatures2() const
    {
        return m_deviceFeatures2;
    }
    std::vector<VkExtensionProperties> availableExtensions() const
    {
        return m_availableExtensions;
    }
    QueueFamilyIndices queueFamilyIndices() const
    {
        return m_queueFamilyIndices;
    }
    VkPhysicalDeviceRayTracingPipelinePropertiesKHR rtPipelineProperties() const
    {
        return m_rtPipelineProperties;
    }

    VkPhysicalDeviceProperties2 deviceProperties2() const
    {
        return m_deviceProperties2;
    };

private:
    InstancePtr m_pInstance;
    SurfacePtr m_pSurface;

    VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;

    // Properties
    VkPhysicalDeviceProperties2 m_deviceProperties2{};
    VkPhysicalDeviceVulkan12Properties m_vulkan12Properties{};
    VkPhysicalDeviceRayTracingPipelinePropertiesKHR m_rtPipelineProperties{};
    VkPhysicalDeviceAccelerationStructurePropertiesKHR m_asProperties{};
    VkPhysicalDeviceMultiviewProperties m_multiviewProperties{};
    VkPhysicalDeviceDescriptorIndexingProperties m_indexingProperties{};

    // Features
    VkPhysicalDeviceFeatures2 m_deviceFeatures2{};
    VkPhysicalDeviceAccelerationStructureFeaturesKHR m_asFeatures{};
    VkPhysicalDeviceRayTracingPipelineFeaturesKHR m_rtFeatures{};
    VkPhysicalDeviceRayQueryFeaturesKHR m_rayFeatures{};

    // Extensions
    std::vector<VkExtensionProperties> m_availableExtensions;

    // Memory
    VkPhysicalDeviceMemoryProperties2 m_deviceMemoryProperties2{};

    // Queues
    QueueFamilyIndices m_queueFamilyIndices{};
};

