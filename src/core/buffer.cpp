#include "buffer.h"
#include "device.h"
#include "vulkancore.h"

/**************************************************************************************************/
Buffer::Buffer( DevicePtr pDevice )
    : m_pDevice( pDevice )
{
}

/**************************************************************************************************/
Buffer::~Buffer()
{
    vkDestroyBuffer( m_pDevice->device(), buffer, nullptr );
    vkFreeMemory( m_pDevice->device(), memory, nullptr );
}

/**************************************************************************************************/
void *Buffer::map() const
{
    void *data;
    VK_CHECK_RESULT( vkMapMemory( m_pDevice->device(), memory, 0, size, 0, &data ) );
    return data;
}

/**************************************************************************************************/
void Buffer::unmap() const
{
    vkUnmapMemory( m_pDevice->device(), memory );
}

/**************************************************************************************************/
BufferPtr Buffer::create( DevicePtr pDevice,
                          const uint8_t *initialData,
                          VkDeviceSize size,
                          VkBufferUsageFlags usage,
                          VkMemoryPropertyFlags properties )
{
    BufferPtr pBuffer = std::make_shared<Buffer>( pDevice );

    VkBufferCreateInfo bufferCI{};
    bufferCI.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCI.size = size;
    bufferCI.usage = usage;
    bufferCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VK_CHECK_RESULT( vkCreateBuffer( pBuffer->m_pDevice->device(),
                                     &bufferCI,
                                     nullptr,
                                     &pBuffer->buffer ) );
    pBuffer->size = size;


    VkMemoryRequirements memoryRequirements;
    vkGetBufferMemoryRequirements( pBuffer->m_pDevice->device(),
                                   pBuffer->buffer,
                                   &memoryRequirements );

    VkMemoryAllocateFlagsInfo memoryAllocateFlagsInfo{};
    memoryAllocateFlagsInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
    memoryAllocateFlagsInfo.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT;

    VkMemoryAllocateInfo allocationInfo{};
    allocationInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    if( usage & VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT )
    {
        allocationInfo.pNext = &memoryAllocateFlagsInfo;
    }
    allocationInfo.allocationSize = memoryRequirements.size;
    allocationInfo.memoryTypeIndex = pBuffer->m_pDevice->findMemoryType(
                                         memoryRequirements.memoryTypeBits,
                                         properties );

    VK_CHECK_RESULT( vkAllocateMemory( pBuffer->m_pDevice->device(),
                                       &allocationInfo,
                                       nullptr,
                                       &pBuffer->memory ) );

    vkBindBufferMemory( pBuffer->m_pDevice->device(),
                        pBuffer->buffer,
                        pBuffer->memory, 0 );

    VkDescriptorBufferInfo &descriptor = pBuffer->descriptor;
    descriptor.buffer = pBuffer->buffer;
    descriptor.offset = 0;
    descriptor.range = pBuffer->size;

    if( initialData )
    {
        void* data = pBuffer->map();
        memcpy( data, initialData, size );
        pBuffer->unmap();
    }

    return pBuffer;
}

/**************************************************************************************************/
void Buffer::copyBuffer( VkBuffer srcBuffer,
                         VkBuffer dstBuffer,
                         VkDeviceSize size )
{
    VkCommandBuffer commandBuffer = m_pDevice->beginSingleTimeCommands();

    VkBufferCopy copyRegion{};
    copyRegion.size = size;
    vkCmdCopyBuffer( commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion );

    m_pDevice->endSingleTimeCommands( commandBuffer );
}

/**************************************************************************************************/
uint64_t Buffer::getDeviceAddress() const
{
    VkBufferDeviceAddressInfoKHR bufferDeviceAI{};
    bufferDeviceAI.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    bufferDeviceAI.buffer = buffer;
    return vkGetBufferDeviceAddress( m_pDevice->device(), &bufferDeviceAI );
}
