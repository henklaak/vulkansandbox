#pragma once
#include <memory>
#include <vector>

#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "instance.h"
#include "camera.h"

class Window;
typedef std::shared_ptr<Window> WindowPtr;

class Window
{
public:


    Window( CameraPtr pCamera );
    virtual ~Window();
    std::vector< std::string > getRequiredInstanceExtensions();
    VkExtent2D extent() const;

    GLFWwindow *window() const
    {
        return m_window;
    }


private:
    void onMouseMove( const float x, const float y );
    void onMouseButton( const int button, const int action, const int mods );
    void onKey( const int key, const int scancode, const int action, const int mods );
    void onFrameBufferResize( const int width, const int height );

    CameraPtr m_pCamera;

    int m_width = 800;
    int m_height = 800;

    GLFWwindow *m_window = nullptr;
    bool m_framebufferResized = false;

    glm::vec2 mCursorPos;
    bool mWKeyDown = false;
    bool mAKeyDown = false;
    bool mSKeyDown = false;
    bool mDKeyDown = false;
    bool mShiftDown = false;
    bool mLMBDown = false;
};

