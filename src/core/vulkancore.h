#pragma once
#include <vulkan/vulkan.hpp>
#include <iostream>

#define VK_CHECK_RESULT(f)                                                                              \
{                                                                                                       \
    VkResult res = (f);                                                                                 \
    if (res != VK_SUCCESS)                                                                              \
    {                                                                                                   \
        std::cerr << "Fatal : VkResult is \"" << res << "\" in " << __FILE__ << " at line " << __LINE__ << "\n"; \
        assert(res == VK_SUCCESS);                                                                      \
    }                                                                                                   \
}

#ifndef NDEBUG
void __assertmsg( const char* expr_str, bool expr, const char* file, int line, const char* msg );
#define assertmsg(Expr, Msg) \
    __assertmsg(#Expr, Expr, __FILE__, __LINE__, Msg)
#else
#define assertmsg(Expr, Msg) ;
#endif


inline uint32_t alignedSize( uint32_t value, uint32_t alignment )
{
    return ( value + alignment - 1 ) & ~( alignment - 1 );
}


