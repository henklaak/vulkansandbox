#include "surface.h"
#include "vulkancore.h"

#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

/**************************************************************************************************/
Surface::Surface( InstancePtr pInstance, WindowPtr pWindow )
    : m_pInstance( pInstance )
{
    VK_CHECK_RESULT( glfwCreateWindowSurface( m_pInstance->instance(), pWindow->window(), nullptr, &m_surface ) );
}

/**************************************************************************************************/
Surface::~Surface()
{
    vkDestroySurfaceKHR( m_pInstance->instance(), m_surface, nullptr );
}
