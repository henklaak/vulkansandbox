#include "device.h"
#include <fstream>
#include <memory.h>
#include <strings.h>
#include "vulkancore.h"
#include "instance.h"
#include "physicaldevice.h"

#define DEFAULT_FENCE_TIMEOUT (100000000000)

/**************************************************************************************************/
bool stringlistContains( std::vector<const char *> haystack,
                         const char *needle,
                         const size_t maxlen = VK_MAX_EXTENSION_NAME_SIZE )
{
    bool available = false;
    for( auto &hay : haystack )
    {
        if( 0 == strncmp( hay, needle, maxlen ) )
        {
            available = true;
        }
    }
    return  available;
}

/**************************************************************************************************/
void addToChain( void *link, void *entry )
{
    struct Link
    {
        VkStructureType                    sType;
        void*                              pNext;
    };

    Link *pLink = reinterpret_cast<Link *>( link );
    Link *pEntry = reinterpret_cast<Link *>( entry );
    Link* pRemainder = reinterpret_cast<Link *>( pLink->pNext );

    assertmsg( pEntry->pNext == nullptr, "Link issue" );

    pEntry->pNext = pRemainder;
    pLink->pNext = pEntry;
}

/**************************************************************************************************/
Device::Device( InstancePtr pInstance,
                PhysicalDevicePtr pPhysicalDevice )
    : m_pInstance( pInstance )
    , m_pPhysicalDevice( pPhysicalDevice )
{
    /**********/
    /* Config */
    /**********/

    VkPhysicalDeviceFeatures requestedFeatures{};
    requestedFeatures.samplerAnisotropy = VK_TRUE;
    requestedFeatures.sampleRateShading = VK_TRUE;

    std::vector<const char *> requestedExtensionNames
    {
        VK_KHR_MULTIVIEW_EXTENSION_NAME,
        VK_KHR_SPIRV_1_4_EXTENSION_NAME,
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME,
        VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME,
        VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME,
    };

    /*******************/
    /* Device features */
    /*******************/

    VkPhysicalDeviceFeatures enabledFeatures = requestedFeatures;
    if( !m_pPhysicalDevice->availableDeviceFeatures2().features.samplerAnisotropy )
    {
        enabledFeatures.samplerAnisotropy = false;
    }
    if( !m_pPhysicalDevice->availableDeviceFeatures2().features.sampleRateShading )
    {
        enabledFeatures.sampleRateShading  = false;
    }

    /*********************/
    /* Device extensions */
    /*********************/

    std::vector<const char *> enabledExtensionNames;
    for( auto &requestedExtensionName : requestedExtensionNames )
    {
        bool available = false;
        for( auto &availableExtension : m_pPhysicalDevice->availableExtensions() )
        {
            if( 0 == strncmp( requestedExtensionName,
                              availableExtension.extensionName,
                              VK_MAX_EXTENSION_NAME_SIZE ) )
            {
                available = true;
            }
        }

        if( !available )
        {
            std::cerr << std::string( "Unsupported device extension " ) + requestedExtensionName << std::endl;
        }
        else
        {
            enabledExtensionNames.push_back( requestedExtensionName );
        }
    }


    // VK_VERSION_1_1
    VkPhysicalDeviceFeatures2 features2 = { };
    features2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    features2.features = enabledFeatures;

    // VK_VERSION_1_2
    VkPhysicalDeviceBufferDeviceAddressFeatures bufferDeviceAddress = {};
    bufferDeviceAddress.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES;
    bufferDeviceAddress.bufferDeviceAddress = VK_TRUE;

    // VK_VERSION_1_2
    VkPhysicalDeviceDescriptorIndexingFeatures descriptorIndexing = { };
    descriptorIndexing.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES;
    descriptorIndexing.runtimeDescriptorArray = VK_TRUE;
    descriptorIndexing.descriptorBindingVariableDescriptorCount = VK_TRUE;
    descriptorIndexing.shaderStorageBufferArrayNonUniformIndexing = VK_TRUE;

    // VK_KHR_acceleration_structure
    VkPhysicalDeviceAccelerationStructureFeaturesKHR rayTracingStructure = {};
    rayTracingStructure.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
    rayTracingStructure.accelerationStructure = VK_TRUE;

    // VK_KHR_ray_tracing_pipeline
    VkPhysicalDeviceRayTracingPipelineFeaturesKHR rayTracingPipeline = {};
    rayTracingPipeline.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR;
    rayTracingPipeline.rayTracingPipeline = VK_TRUE;

    addToChain( &features2, &descriptorIndexing );
    //addToChain( &features2, &bufferDeviceAddress );
    if( stringlistContains( enabledExtensionNames, VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME ) )
    {
        addToChain( &features2, &rayTracingStructure );
    }
    if( stringlistContains( enabledExtensionNames, VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME ) )
    {
        addToChain( &features2, &rayTracingPipeline );
    }

    VkDeviceCreateInfo deviceCI = {};
    deviceCI.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCI.pEnabledFeatures = nullptr;
    deviceCI.enabledExtensionCount  = enabledExtensionNames.size();
    deviceCI.ppEnabledExtensionNames = enabledExtensionNames.data();
    deviceCI.pNext = &features2;

    /*****************/
    /* Create Queues */
    /*****************/

    PhysicalDevice::QueueFamilyIndices queueFamilyIndices = m_pPhysicalDevice->queueFamilyIndices();

    std::vector<VkDeviceQueueCreateInfo> queueCIs{};
    const float defaultQueuePriority( 0.0f );

    VkDeviceQueueCreateInfo queueCI{};
    queueCI.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCI.queueFamilyIndex = queueFamilyIndices.graphics.value();
    queueCI.queueCount = 1;
    queueCI.pQueuePriorities = &defaultQueuePriority;
    queueCIs.push_back( queueCI );

    if( queueFamilyIndices.compute.value() != queueFamilyIndices.graphics.value() )
    {
        // If compute family index differs, we need an additional queue create info for the compute queue
        VkDeviceQueueCreateInfo queueInfo{};
        queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueInfo.queueFamilyIndex = queueFamilyIndices.compute.value();
        queueInfo.queueCount = 1;
        queueInfo.pQueuePriorities = &defaultQueuePriority;
        queueCIs.push_back( queueInfo );
    }

    if( ( queueFamilyIndices.transfer.value() != queueFamilyIndices.graphics.value() )
            && ( queueFamilyIndices.transfer != queueFamilyIndices.compute ) )
    {
        // If compute family index differs, we need an additional queue create info for the compute queue
        VkDeviceQueueCreateInfo queueInfo{};
        queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueInfo.queueFamilyIndex = queueFamilyIndices.transfer.value();
        queueInfo.queueCount = 1;
        queueInfo.pQueuePriorities = &defaultQueuePriority;
        queueCIs.push_back( queueInfo );
    }
    deviceCI.queueCreateInfoCount = queueCIs.size();
    deviceCI.pQueueCreateInfos = queueCIs.data();

    VK_CHECK_RESULT( vkCreateDevice( m_pPhysicalDevice->physicalDevice(),
                                     &deviceCI,
                                     nullptr,
                                     &m_device ) );

    // Create a default cmd pool
    VkCommandPoolCreateInfo commandPoolCI = {};
    commandPoolCI.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolCI.queueFamilyIndex = queueFamilyIndices.graphics.value();
    commandPoolCI.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    VK_CHECK_RESULT( vkCreateCommandPool(
                         m_device,
                         &commandPoolCI,
                         nullptr,
                         &m_commandPool ) ) ;

    vkGetDeviceQueue( m_device,
                      queueFamilyIndices.graphics.value(),
                      0,
                      &m_graphicsQueue );

    vkGetDeviceQueue( m_device,
                      queueFamilyIndices.present.value(),
                      0,
                      &m_presentQueue );
}

/**************************************************************************************************/
Device::~Device()
{
    vkDeviceWaitIdle( m_device );

    vkDestroyCommandPool( m_device, m_commandPool, nullptr );
    vkDestroyDevice( m_device, nullptr );
}

/**************************************************************************************************/
VkSampleCountFlagBits Device::getMaxMsaa() const
{
    auto limits = m_pPhysicalDevice->deviceProperties2().properties.limits;

    auto c = limits.framebufferColorSampleCounts;
    auto d = limits.framebufferDepthSampleCounts;
    auto s = limits.framebufferStencilSampleCounts;

    if( c & d & s & 4 )
    {
        return VK_SAMPLE_COUNT_4_BIT;
    }
    else
    {
        return VK_SAMPLE_COUNT_1_BIT;
    }
}

/**************************************************************************************************/
VkCommandBuffer Device::beginSingleTimeCommands()
{
    VkCommandBufferAllocateInfo allocateInfo{};
    allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocateInfo.commandPool = m_commandPool;
    allocateInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers( m_device,
                              &allocateInfo,
                              &commandBuffer );

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer( commandBuffer,
                          &beginInfo );
    return commandBuffer;
}

/**************************************************************************************************/
void Device::endSingleTimeCommands( VkCommandBuffer commandBuffer )
{
    vkEndCommandBuffer( commandBuffer );

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    VkFence fence;
    VK_CHECK_RESULT( vkCreateFence( m_device, &fenceInfo, nullptr, &fence ) );

    vkQueueSubmit( m_graphicsQueue,
                   1,
                   &submitInfo,
                   fence );
    vkQueueWaitIdle( m_graphicsQueue );

    VK_CHECK_RESULT( vkWaitForFences( m_device, 1, &fence, VK_TRUE, DEFAULT_FENCE_TIMEOUT ) );
    vkDestroyFence( m_device, fence, nullptr );

    vkFreeCommandBuffers( m_device,
                          m_commandPool,
                          1,
                          &commandBuffer );
}

/**************************************************************************************************/
std::vector<char> Device::readFile( const std::string& filename )
{
    std::ifstream file( filename, std::ios::ate | std::ios::binary );

    if( !file.is_open() )
    {
        throw std::runtime_error( "failed to open file!" );
    }

    size_t fileSize = ( size_t ) file.tellg();
    std::vector<char> buffer( fileSize );

    file.seekg( 0 );
    file.read( buffer.data(), fileSize );

    file.close();

    return buffer;
}

/**************************************************************************************************/
VkShaderModule Device::createShaderModule( const std::vector<char>& code )
{
    VkShaderModuleCreateInfo shaderModuleCI{};
    shaderModuleCI.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shaderModuleCI.codeSize = code.size();
    shaderModuleCI.pCode = reinterpret_cast<const uint32_t*>( code.data() );

    VkShaderModule shaderModule;
    VK_CHECK_RESULT( vkCreateShaderModule( m_device,
                                           &shaderModuleCI,
                                           nullptr,
                                           &shaderModule ) );

    return shaderModule;
}
