#include "swapchain.h"
#include <optional>
#include <algorithm>
#include "vulkancore.h"
#include "image.h"
#include "device.h"
#include "window.h"

/**************************************************************************************************/
Swapchain::Swapchain( DevicePtr pDevice, SurfacePtr pSurface )
    : m_pDevice( pDevice )
    , m_pSurface( pSurface )
{
    SwapChainSupportDetails supportDetails = querySwapChainSupport();

    VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat( supportDetails.formats );
    VkPresentModeKHR presentMode = chooseSwapPresentMode( supportDetails.presentModes );
    m_swapchainExtent = chooseSwapExtent( supportDetails.capabilities );
    swapchainImageFormat = surfaceFormat.format;

    uint32_t imageCount = supportDetails.capabilities.minImageCount + 1;
    if( supportDetails.capabilities.maxImageCount > 0
            && imageCount > supportDetails.capabilities.maxImageCount )
    {
        imageCount = supportDetails.capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR swapchainCI{};
    swapchainCI.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchainCI.surface = m_pSurface->surface();
    swapchainCI.minImageCount = imageCount;
    swapchainCI.imageFormat = surfaceFormat.format;
    swapchainCI.imageColorSpace = surfaceFormat.colorSpace;
    swapchainCI.imageExtent = m_swapchainExtent;
    swapchainCI.imageArrayLayers = 1;
    swapchainCI.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT |
                             VK_IMAGE_USAGE_TRANSFER_SRC_BIT |
                             VK_IMAGE_USAGE_TRANSFER_DST_BIT;

    PhysicalDevice::QueueFamilyIndices indices = m_pDevice->queueFamilyIndices();
    uint32_t queueFamilyIndices[] =
    {
        indices.graphics.value(),
        indices.present.value()
    };

    if( indices.graphics.value() != indices.present.value() )
    {
        swapchainCI.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        swapchainCI.queueFamilyIndexCount = 2;
        swapchainCI.pQueueFamilyIndices = queueFamilyIndices;
    }
    else
    {
        swapchainCI.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    }
    swapchainCI.preTransform = supportDetails.capabilities.currentTransform;
    swapchainCI.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchainCI.presentMode = presentMode;
    swapchainCI.clipped = VK_TRUE;
    swapchainCI.oldSwapchain = VK_NULL_HANDLE;

    VK_CHECK_RESULT( vkCreateSwapchainKHR( m_pDevice->device(),
                                           &swapchainCI,
                                           nullptr,
                                           &swapchain ) );

    std::vector<VkImage> swapchainImages;
    vkGetSwapchainImagesKHR( m_pDevice->device(),
                             swapchain,
                             &imageCount,
                             nullptr );
    swapchainImages.resize( imageCount );
    vkGetSwapchainImagesKHR( m_pDevice->device(),
                             swapchain,
                             &imageCount,
                             swapchainImages.data() );
    m_swapchainImages.resize( imageCount );
    for( uint32_t i = 0; i < imageCount; ++i )
    {
        m_swapchainImages[i] = Image::createFromImage(
                                   m_pDevice,
                                   swapchainImages[i],
                                   m_swapchainExtent.height,
                                   m_swapchainExtent.width,
                                   1,
                                   1,
                                   VK_SAMPLE_COUNT_1_BIT,
                                   surfaceFormat.format,
                                   VK_IMAGE_ASPECT_COLOR_BIT );
    }

    VkSemaphoreCreateInfo semaphoreCI{};
    semaphoreCI.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    VK_CHECK_RESULT( vkCreateSemaphore(
                         m_pDevice->device(),
                         &semaphoreCI,
                         nullptr,
                         &imageAvailableSemaphore ) );

    VK_CHECK_RESULT( vkCreateSemaphore(
                         m_pDevice->device(),
                         &semaphoreCI,
                         nullptr,
                         &renderFinishedSemaphore ) );
}


/**************************************************************************************************/
Swapchain::~Swapchain()
{
    vkDestroySemaphore( m_pDevice->device(), renderFinishedSemaphore, nullptr );
    vkDestroySemaphore( m_pDevice->device(), imageAvailableSemaphore, nullptr );

    vkDestroySwapchainKHR( m_pDevice->device(), swapchain, nullptr );
}

/**************************************************************************************************/
uint32_t Swapchain::acquireNext()
{
    uint32_t currentFrame = -1;

    vkAcquireNextImageKHR( m_pDevice->device(),
                           swapchain,
                           UINT64_MAX,
                           imageAvailableSemaphore,
                           VK_NULL_HANDLE,
                           &currentFrame );
    return currentFrame;
}

/**************************************************************************************************/
void Swapchain::present( uint32_t frameNr )
{
    VkPresentInfoKHR presentInfo{};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &renderFinishedSemaphore;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = &swapchain;
    presentInfo.pImageIndices = &frameNr;
    vkQueuePresentKHR( m_pDevice->presentQueue(), &presentInfo );
}

/**************************************************************************************************/
VkSurfaceFormatKHR Swapchain::chooseSwapSurfaceFormat(
    const std::vector<VkSurfaceFormatKHR> &availableFormats )
{
    for( const auto& availableFormat : availableFormats )
    {
        if( availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM
                && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR )
        {
            return availableFormat;
        }
    }

    return availableFormats[0];
}

/**************************************************************************************************/
VkPresentModeKHR Swapchain::chooseSwapPresentMode(
    const std::vector<VkPresentModeKHR> &availablePresentModes )
{
    if( not m_vsync )
    {

        for( const auto& availablePresentMode : availablePresentModes )
        {
            if( availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR )
            {
                return availablePresentMode;
            }
        }
    }

    return VK_PRESENT_MODE_FIFO_KHR;
}

/**************************************************************************************************/
VkExtent2D Swapchain::chooseSwapExtent( const VkSurfaceCapabilitiesKHR& capabilities )
{
    if( capabilities.currentExtent.width != UINT32_MAX )
    {
        return capabilities.currentExtent;
    }
    else
    {
        VkExtent2D actualExtent = m_pSurface->extent();

        actualExtent.width = std::clamp( actualExtent.width,
                                         capabilities.minImageExtent.width,
                                         capabilities.maxImageExtent.width );
        actualExtent.height = std::clamp( actualExtent.height,
                                          capabilities.minImageExtent.height,
                                          capabilities.maxImageExtent.height );

        return actualExtent;
    }
}

/**************************************************************************************************/
Swapchain::SwapChainSupportDetails Swapchain::querySwapChainSupport()
{
    SwapChainSupportDetails swapChainSupportDetails{};

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
        m_pDevice->physicalDevice(),
        m_pSurface->surface(),
        &swapChainSupportDetails.capabilities );

    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(
        m_pDevice->physicalDevice(),
        m_pSurface->surface(),
        &formatCount,
        nullptr );

    if( formatCount != 0 )
    {
        swapChainSupportDetails.formats.resize( formatCount );
        vkGetPhysicalDeviceSurfaceFormatsKHR(
            m_pDevice->physicalDevice(),
            m_pSurface->surface(),
            &formatCount,
            swapChainSupportDetails.formats.data() );
    }

    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(
        m_pDevice->physicalDevice(),
        m_pSurface->surface(),
        &presentModeCount,
        nullptr );

    if( presentModeCount != 0 )
    {
        swapChainSupportDetails.presentModes.resize( presentModeCount );
        vkGetPhysicalDeviceSurfacePresentModesKHR(
            m_pDevice->physicalDevice(),
            m_pSurface->surface(),
            &presentModeCount,
            swapChainSupportDetails.presentModes.data() );
    }

    return swapChainSupportDetails;
}
