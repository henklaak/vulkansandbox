#pragma once
#include "instance.h"
#include "window.h"

class Surface;
typedef std::shared_ptr<Surface> SurfacePtr;

class Surface
{
public:
    Surface( InstancePtr pInstance,
             WindowPtr pWindow );
    virtual ~Surface();

    VkSurfaceKHR surface() const
    {
        return m_surface;
    }

    VkExtent2D extent() const
    {
        return m_pWindow->extent();
    }

private:
    InstancePtr m_pInstance;
    WindowPtr m_pWindow;
    VkExtent2D m_extent;

    VkSurfaceKHR m_surface;
};

