#include "physicaldevice.h"
#include <vector>
#include "vulkancore.h"
#include "instance.h"
#include "window.h"

/**************************************************************************************************/
PhysicalDevice::PhysicalDevice( InstancePtr pInstance, SurfacePtr pSurface )
    : m_pInstance( pInstance )
    , m_pSurface( pSurface )
{
    // Just select first device

    uint32_t deviceCount = 0;
    VK_CHECK_RESULT( vkEnumeratePhysicalDevices( m_pInstance->instance(), &deviceCount, nullptr ) );
    std::vector<VkPhysicalDevice> physicalDevices( deviceCount );
    vkEnumeratePhysicalDevices( m_pInstance->instance(), &deviceCount, physicalDevices.data() );

    m_physicalDevice = physicalDevices[0];

    // Properties

    m_deviceProperties2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    m_deviceProperties2.pNext =  &m_vulkan12Properties;

    m_vulkan12Properties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_PROPERTIES;
    m_vulkan12Properties.pNext = &m_rtPipelineProperties;

    m_rtPipelineProperties.sType =
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;
    m_rtPipelineProperties.pNext = &m_asProperties;

    m_asProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_PROPERTIES_KHR;
    m_asProperties.pNext = &m_multiviewProperties;

    m_multiviewProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES;
    m_multiviewProperties.pNext = &m_indexingProperties;

    m_indexingProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES;
    m_indexingProperties.pNext = nullptr;

    vkGetPhysicalDeviceProperties2( m_physicalDevice, &m_deviceProperties2 );

    // Features

    m_deviceFeatures2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    m_deviceFeatures2.pNext = &m_asFeatures;

    m_asFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
    m_asFeatures.pNext = &m_rtFeatures;

    m_rtFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR;
    m_rtFeatures.pNext = &m_rayFeatures;

    m_rayFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
    m_rayFeatures.pNext = nullptr;

    vkGetPhysicalDeviceFeatures2( m_physicalDevice, &m_deviceFeatures2 );

    // Extensions

    uint32_t nrAvailableExtensions = 0;
    VK_CHECK_RESULT( vkEnumerateDeviceExtensionProperties( m_physicalDevice, nullptr, &nrAvailableExtensions, nullptr ) );
    m_availableExtensions.resize( nrAvailableExtensions );
    VK_CHECK_RESULT( vkEnumerateDeviceExtensionProperties( m_physicalDevice, nullptr, &nrAvailableExtensions,
                     m_availableExtensions.data() ) );

    // Memory

    m_deviceMemoryProperties2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
    m_deviceMemoryProperties2.pNext = nullptr;
    vkGetPhysicalDeviceMemoryProperties2( m_physicalDevice, &m_deviceMemoryProperties2 );

    // Queues
    std::vector<VkQueueFamilyProperties> queueFamilyProperties;

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties( m_physicalDevice, &queueFamilyCount, nullptr );
    assert( queueFamilyCount > 0 );
    queueFamilyProperties.resize( queueFamilyCount );
    vkGetPhysicalDeviceQueueFamilyProperties( m_physicalDevice, &queueFamilyCount, queueFamilyProperties.data() );

    int i = 0;
    for( const auto& queueFamily : queueFamilyProperties )
    {
        if( !m_queueFamilyIndices.graphics.has_value() )
            if( queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT )
            {
                m_queueFamilyIndices.graphics = i;
            }

        if( !m_queueFamilyIndices.compute.has_value() )
            if( queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT )
            {
                m_queueFamilyIndices.compute = i;
            }

        if( !m_queueFamilyIndices.transfer.has_value() )
            if( queueFamily.queueFlags & VK_QUEUE_TRANSFER_BIT )
            {
                m_queueFamilyIndices.transfer = i;
            }

        if( !m_queueFamilyIndices.present.has_value() )
        {
            VkBool32 presentSupport = false;
            vkGetPhysicalDeviceSurfaceSupportKHR( m_physicalDevice, i, m_pSurface->surface(), &presentSupport );

            if( presentSupport )
            {
                m_queueFamilyIndices.present = i;
            }
        }

        i++;
    }
}

/**************************************************************************************************/
PhysicalDevice::~PhysicalDevice()
{

}

/**************************************************************************************************/
uint32_t PhysicalDevice::findMemoryType(
    uint32_t typeFilter,
    VkMemoryPropertyFlags properties ) const
{
    for( uint32_t i = 0; i < m_deviceMemoryProperties2.memoryProperties.memoryTypeCount; i++ )
    {
        if( ( typeFilter & ( 1 << i ) )
                && ( m_deviceMemoryProperties2.memoryProperties.memoryTypes[i].propertyFlags & properties ) ==
                properties )
        {
            return i;
        }
    }

    throw std::runtime_error( "failed to find suitable memory type!" );
}

///**************************************************************************************************/
//uint32_t VkSbPhysicalDevice::getQueueFamilyIndex( VkQueueFlagBits queueFlags )
//{
//    uint32_t queueFamilyCount = 0;
//    vkGetPhysicalDeviceQueueFamilyProperties(
//        m_physicalDevice,
//        &queueFamilyCount,
//        nullptr );
//    assert( queueFamilyCount > 0 );
//    m_queueFamilyProperties.resize( queueFamilyCount );
//    vkGetPhysicalDeviceQueueFamilyProperties(
//        m_physicalDevice,
//        &queueFamilyCount,
//        m_queueFamilyProperties.data() );

//    // Dedicated queue for compute
//    // Try to find a queue family index that supports compute but not graphics
//    if( queueFlags & VK_QUEUE_COMPUTE_BIT )
//    {
//        for( uint32_t i = 0; i < static_cast<uint32_t>( m_queueFamilyProperties.size() );
//                i++ )
//        {
//            if( ( m_queueFamilyProperties[i].queueFlags & queueFlags )
//                    && ( ( m_queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT ) == 0 ) )
//            {
//                return i;
//            }
//        }
//    }

//    // Dedicated queue for transfer
//    // Try to find a queue family index that supports transfer but not graphics and compute
//    if( queueFlags & VK_QUEUE_TRANSFER_BIT )
//    {
//        for( uint32_t i = 0; i < static_cast<uint32_t>( m_queueFamilyProperties.size() );
//                i++ )
//        {
//            if( ( m_queueFamilyProperties[i].queueFlags & queueFlags )
//                    && ( ( m_queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT ) == 0 )
//                    && ( ( m_queueFamilyProperties[i].queueFlags & VK_QUEUE_COMPUTE_BIT ) == 0 ) )
//            {
//                return i;
//            }
//        }
//    }

//    // For other queue types or if no separate compute queue is present, return the first one to support the requested flags
//    for( uint32_t i = 0; i < static_cast<uint32_t>( m_queueFamilyProperties.size() );
//            i++ )
//    {
//        if( m_queueFamilyProperties[i].queueFlags & queueFlags )
//        {
//            return i;
//        }
//    }

//    throw std::runtime_error( "Could not find a matching queue family index" );
//}

/**************************************************************************************************/
VkFormat PhysicalDevice::findSupportedFormat(
    const std::vector<VkFormat>& candidates,
    VkImageTiling tiling,
    VkFormatFeatureFlags features ) const
{
    for( VkFormat format : candidates )
    {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties( m_physicalDevice,
                                             format,
                                             &props );

        if( tiling == VK_IMAGE_TILING_LINEAR && ( props.linearTilingFeatures & features ) == features )
        {
            return format;
        }
        else if( tiling == VK_IMAGE_TILING_OPTIMAL
                 && ( props.optimalTilingFeatures & features ) == features )
        {
            return format;
        }
    }

    throw std::runtime_error( "failed to find supported format!" );
}

/**************************************************************************************************/
VkFormat PhysicalDevice::findDepthFormat() const
{
    return findSupportedFormat(
    {
        VK_FORMAT_D24_UNORM_S8_UINT,
        VK_FORMAT_D32_SFLOAT_S8_UINT,
        VK_FORMAT_D32_SFLOAT
    },
    VK_IMAGE_TILING_OPTIMAL,
    VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    );
}

