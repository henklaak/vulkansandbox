#include "instance.h"
#include <algorithm>
#include <ranges>
#include <stdexcept>
#include <string.h>
#include "vulkancore.h"

VkBool32 debugUtilsMessengerCallback( VkDebugUtilsMessageSeverityFlagBitsEXT,
                                      VkDebugUtilsMessageTypeFlagsEXT,
                                      const VkDebugUtilsMessengerCallbackDataEXT*,
                                      void* );

/**************************************************************************************************/
Instance::Instance( const std::vector<std::string> &requiredExtensions )
{
    /**********/
    /* Config */
    /**********/

    std::vector<std::string> requestedLayerNames =
    {
        "VK_LAYER_KHRONOS_validation",
    };

    std::vector<std::string> requestedExtensionNames =
    {
        VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
        VK_KHR_SURFACE_EXTENSION_NAME,
        VK_KHR_XCB_SURFACE_EXTENSION_NAME,
    };

    std::copy( std::begin( requiredExtensions ), std::end( requiredExtensions ), std::back_inserter( requestedExtensionNames ) );

    /****************************/
    /* Determine enabled layers */
    /****************************/

    uint32_t nrAvailableLayers = 0;
    VK_CHECK_RESULT( vkEnumerateInstanceLayerProperties(
                         &nrAvailableLayers,
                         nullptr ) );
    std::vector<VkLayerProperties> availableLayers( nrAvailableLayers );
    VK_CHECK_RESULT( vkEnumerateInstanceLayerProperties(
                         &nrAvailableLayers,
                         availableLayers.data() ) );


    std::vector<const char *> enabledLayerNames;
    for( std::string &requestedLayerName : requestedLayerNames)
    {
        bool available = false;
        for( auto &availableLayer : availableLayers )
        {
            if( requestedLayerName == availableLayer.layerName )
            {
                available = true;
            }
        }

        if( !available )
        {
            throw std::runtime_error( std::string( "Unsupported 3 " ) + requestedLayerName );
        }
        enabledLayerNames.push_back( requestedLayerName.c_str() );
    }
    for( const auto &enabledLayerName : enabledLayerNames )
    {
        std::cout << enabledLayerName << std::endl;
    }

    /********************************/
    /* Determine enabled extensions */
    /********************************/

    uint32_t nrAvailableExtensions = 0;
    VK_CHECK_RESULT( vkEnumerateInstanceExtensionProperties(
                         nullptr,
                         &nrAvailableExtensions,
                         nullptr ) );
    std::vector<VkExtensionProperties> availableExtensions( nrAvailableExtensions );
    VK_CHECK_RESULT( vkEnumerateInstanceExtensionProperties(
                         nullptr,
                         &nrAvailableExtensions,
                         availableExtensions.data() ) );

    std::vector<const char *> enabledExtensionNames;
    for( std::string &requestedExtensionName : requestedExtensionNames )
    {
        bool available = false;
        for( auto &availableExtension : availableExtensions )
        {
            if( requestedExtensionName == availableExtension.extensionName )
            {
                available = true;
            }
        }

        if( !available )
        {
            throw std::runtime_error( std::string( "Unsupported 2 " ) + requestedExtensionName );
        }
        enabledExtensionNames.push_back( requestedExtensionName.c_str() );
    }
    for( const auto &enabledExtensionName : enabledExtensionNames )
    {
        std::cout << enabledExtensionName << std::endl;
    }

    /*******************/
    /* Create instance */
    /*******************/

    VkApplicationInfo applicationInfo = {};
    applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    applicationInfo.pApplicationName = "VulkanSandBox";
    applicationInfo.pEngineName =  "VulkanSandBox";
    applicationInfo.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo instanceCI{};
    instanceCI.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCI.pApplicationInfo = &applicationInfo;
    instanceCI.enabledExtensionCount = enabledExtensionNames.size();
    instanceCI.ppEnabledExtensionNames = enabledExtensionNames.data();
    instanceCI.enabledLayerCount = enabledLayerNames.size();
    instanceCI.ppEnabledLayerNames = enabledLayerNames.data();

    VK_CHECK_RESULT( vkCreateInstance( &instanceCI, nullptr, &m_instance ) );

    /******************/
    /* Init debugging */
    /******************/

    initDebugging();
}

/**************************************************************************************************/
Instance::~Instance()
{
    destroyDebugging();

    vkDestroyInstance( m_instance, nullptr );
}

/**************************************************************************************************/
void Instance::initDebugging()
{
    PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT =
        reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(
            vkGetInstanceProcAddr( m_instance, "vkCreateDebugUtilsMessengerEXT" ) );

    VkDebugUtilsMessengerCreateInfoEXT debugUtilsMessengerCI{};
    debugUtilsMessengerCI.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debugUtilsMessengerCI.messageSeverity =
        //VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
        //VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT  |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debugUtilsMessengerCI.messageType =
        VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
    debugUtilsMessengerCI.pfnUserCallback = debugUtilsMessengerCallback;
    VK_CHECK_RESULT( vkCreateDebugUtilsMessengerEXT(
                         m_instance,
                         &debugUtilsMessengerCI,
                         nullptr,
                         &m_debugUtilsMessenger ) );

}

/**************************************************************************************************/
void Instance::destroyDebugging()
{
    PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT =
        reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(
            vkGetInstanceProcAddr( m_instance, "vkDestroyDebugUtilsMessengerEXT" ) );

    vkDestroyDebugUtilsMessengerEXT( m_instance, m_debugUtilsMessenger, nullptr );
}

/**************************************************************************************************/
VKAPI_ATTR VkBool32 VKAPI_CALL debugUtilsMessengerCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* )
{
    // Select prefix depending on flags passed to the callback
    std::string prefix( "" );

    if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT )
    {
        prefix = "VERBOSE: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT )
    {
        prefix = "INFO: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT )
    {
        prefix = "WARNING: ";
    }
    else if( messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT )
    {
        prefix = "ERROR: ";
    }


    // Display message to default output (console/logcat)
    std::stringstream debugMessage;
    debugMessage << "-----------------------------------------" << std::endl
                 << prefix << std::endl
                 << pCallbackData->messageIdNumber << std::endl
                 << pCallbackData->pMessageIdName << std::endl
                 << pCallbackData->pMessage;

    if( messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT )
    {
        std::cerr << debugMessage.str() << "\n";
    }
    else
    {
        std::cout << debugMessage.str() << "\n";
    }
    fflush( stdout );

    // The return value of this callback controls whether the Vulkan call that caused the validation message will be aborted or not
    // We return VK_FALSE as we DON'T want Vulkan calls that cause a validation message to abort
    // If you instead want to have calls abort, pass in VK_TRUE and the function will return VK_ERROR_VALIDATION_FAILED_EXT

    return VK_FALSE;
}

