#pragma once
#include <vulkan/vulkan.hpp>

class Instance;
typedef std::shared_ptr<Instance> InstancePtr;

class Instance
{
public:
    Instance(const std::vector<std::string> &requiredExtensions);
    virtual ~Instance();

    VkInstance instance() const
    {
        return m_instance;
    }

private:
    void initDebugging();
    void destroyDebugging();

    VkInstance m_instance = VK_NULL_HANDLE;
    VkDebugUtilsMessengerEXT m_debugUtilsMessenger = VK_NULL_HANDLE;

};

typedef std::shared_ptr<Instance> InstancePtr;

