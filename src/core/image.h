#pragma once
#include <memory>
#include <vulkan/vulkan.hpp>
#include "device.h"

class Image;
typedef std::shared_ptr<Image> ImagePtr;

class Image
{
public:
    Image( DevicePtr device );
    virtual ~Image();

    static ImagePtr create( DevicePtr pDevice,
                            uint32_t width,
                            uint32_t height,
                            uint32_t numLayers,
                            uint32_t numMipLevels,
                            VkSampleCountFlagBits numSamples,
                            VkFormat format,
                            VkImageTiling tiling,
                            VkImageUsageFlags usage,
                            VkMemoryPropertyFlags properties,
                            VkImageAspectFlags aspectFlags );

    static ImagePtr createFromImage( DevicePtr pDevice,
                                     VkImage image,
                                     uint32_t width,
                                     uint32_t height,
                                     uint32_t numLayers,
                                     uint32_t numMipLevels,
                                     VkSampleCountFlagBits numSamples,
                                     VkFormat format,
                                     VkImageAspectFlags aspectFlags );

    void createImageView( VkImageAspectFlags aspectFlags );

    void fillFromBuffer( VkBuffer );

    void generateMipMaps( size_t numMipLevels );

    void setLayout( VkCommandBuffer cmdbuffer,
                    VkImageLayout oldImageLayout,
                    VkImageLayout newImageLayout,
                    VkImageSubresourceRange subresourceRange,
                    VkPipelineStageFlags srcStageMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                    VkPipelineStageFlags dstStageMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT );

    VkImageView imageView()
    {
        return view;
    }

private:
    DevicePtr m_pDevice;

    VkImage image = VK_NULL_HANDLE;
    VkDeviceMemory memory = VK_NULL_HANDLE;
    VkImageView view = VK_NULL_HANDLE;
    uint32_t width = 1;
    uint32_t height = 1;
    uint32_t numLayers = 1;
    uint32_t numMipLevels = 1;
    VkSampleCountFlagBits numSamples = VK_SAMPLE_COUNT_1_BIT;
    VkFormat format = {};
    VkDescriptorImageInfo descriptor{};

    VkSampler m_sampler = VK_NULL_HANDLE;
    bool m_ownImage = true;
};

typedef std::shared_ptr<Image> ImagePtr;
