#include "modelloader.h"
#include "vulkancore.h"

#include <iostream>
#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include <tiny_gltf.h>
#undef STB_IMAGE_IMPLEMENTATION
#undef TINYGLTF_IMPLEMENTATION


#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
//#include <glm/gtx/io.hpp>


/**************************************************************************************************/
ModelLoader::ModelLoader( const std::string &filename )
    : m_filename( filename )
{
}

/**************************************************************************************************/
ModelLoader::~ModelLoader()
{
}

/**************************************************************************************************/
void ModelLoader::load( RenderModelPtr pRenderModel )
{
    m_pRenderModel = pRenderModel;
    tinygltf::TinyGLTF context{};
    std::string err;
    std::string warn;
    std::string path;
    size_t pos = m_filename.find_last_of( '/' );
    path = m_filename.substr( 0, pos );

    assert( context.LoadBinaryFromFile( &m_model, &err, &warn, m_filename ) );

    // Preliminary checks
    assertmsg( m_model.scenes.size() >= 1, "At least 1 Scene needed" );
    assertmsg( m_model.nodes.size() >= 1, "At least 1 Node needed" );
    assertmsg( m_model.meshes.size() >= 1, "At least 1 Mesh needed" );
    assertmsg( m_model.cameras.size() == 1, "Exactly 1 Camera needed" );
    assertmsg( m_model.lights.size() >= 1, "At least 1 Light needed" );

    int sceneIndex = m_model.defaultScene;
    loadScene( sceneIndex );
}

/**************************************************************************************************/
void ModelLoader::loadScene( int sceneIndex )
{
    if( sceneIndex < 0 || sceneIndex >= ( int )m_model.scenes.size() )
    {
        return;
    }

    tinygltf::Scene &scene = m_model.scenes[sceneIndex];

    glm::mat4 worldTransform( 1.0 );

    for( int &nodeIndex : scene.nodes )
    {
        loadNode( nodeIndex, worldTransform );
    }
}

/**************************************************************************************************/
void ModelLoader::loadNode( int nodeIndex,
                                const glm::mat4 &parentTransform )
{
    if( nodeIndex < 0 || nodeIndex >= ( int )m_model.nodes.size() )
    {
        return;
    }

    tinygltf::Node &node = m_model.nodes[nodeIndex];

    glm::mat4 I( 1.0 );
    glm::mat4 meshTransform = I;
    glm::mat4 translationTransform = I;
    glm::mat4 rotationTransform  = I;
    glm::mat4 scaleTransform  = I;
    if( !node.matrix.empty() )
    {
        meshTransform = glm::make_mat4x4( node.matrix.data() );
    }
    if( !node.translation.empty() )
    {
        glm::vec3 translationVector = glm::make_vec3( node.translation.data() );
        translationTransform = glm::translate( I, translationVector );
    }
    if( !node.rotation.empty() )
    {
        glm::quat rotationQuat = glm::make_quat( node.rotation.data() );
        rotationTransform = glm::mat4( rotationQuat );
    }
    if( !node.scale.empty() )
    {
        glm::vec3 scaleVector = glm::make_vec3( node.scale.data() );
        scaleTransform = glm::scale( I, scaleVector );
    }

    // TRS convention
    glm::mat4 localTransform = translationTransform * rotationTransform * scaleTransform *
                               meshTransform;

    int meshIndex = node.mesh;
    loadMesh( meshIndex, parentTransform * localTransform );

    int cameraIndex = node.camera;
    loadCamera( cameraIndex,  parentTransform * localTransform );

    auto it = node.extensions.find( "KHR_lights_punctual" );
    if( it != node.extensions.end() )
    {
        int lightIndex = it->second.Get( "light" ).GetNumberAsInt();
        loadLight( lightIndex, parentTransform * localTransform );
    }

    for( auto &childIndex : node.children )
    {
        loadNode( childIndex, parentTransform * localTransform );
    }
}

/**************************************************************************************************/
void ModelLoader::loadMesh( int meshIndex,
                                const glm::mat4 &parentTransform )
{
    if( meshIndex < 0 || meshIndex >= ( int )m_model.meshes.size() )
    {
        return;
    }

    tinygltf::Mesh &mesh = m_model.meshes[meshIndex];

    for( tinygltf::Primitive &primitive : mesh.primitives )
    {
        loadPrimitive( primitive, parentTransform );
    }
}

/**************************************************************************************************/
void ModelLoader::loadCamera( int cameraIndex,
                                  const glm::mat4 &parentTransform )
{
    if( cameraIndex < 0 || cameraIndex >= ( int )m_model.cameras.size() )
    {
        return;
    }

    tinygltf::Camera &camera = m_model.cameras[cameraIndex];
    assertmsg( camera.type == "perspective", "Only perspective cameras allowed" );

    double aspectRatio = camera.perspective.aspectRatio;
    double yFov = camera.perspective.yfov;
    double zNear = camera.perspective.znear;
    double zFar = camera.perspective.zfar;

    m_pRenderModel->setCamera( parentTransform,
                            yFov,
                            aspectRatio,
                            zNear,
                            zFar );
}

/**************************************************************************************************/
void ModelLoader::loadLight( int lightIndex,
                                 const glm::mat4 &parentTransform )
{
    if( lightIndex < 0 || lightIndex >= ( int )m_model.lights.size() )
    {
        return;
    }

    tinygltf::Light &light = m_model.lights[lightIndex];
    assertmsg( light.type == "point", "Only point lights allowed" );

    glm::vec3 color = glm::make_vec3( light.color.data() );
    double intensity = light.intensity;

    m_pRenderModel->addPointLight( parentTransform,
                                   color,
                                   intensity );
}

/**************************************************************************************************/
void ModelLoader::loadPrimitive( const tinygltf::Primitive &primitive,
                                     const glm::mat4 &parentTransform )
{
    /********************************************/
    assertmsg( primitive.attributes.contains( "POSITION" ), "Primitive needd POSITION attribute" );
    assertmsg( primitive.attributes.contains( "NORMAL" ), "Primitive needd NORMAL attribute" );
    assertmsg( primitive.attributes.contains( "TANGENT" ), "Primitive needd TANGENT attribute" );
    assertmsg( primitive.attributes.contains( "TEXCOORD_0" ), "Primitive needd TEXCOORD_0 attribute" );

    int positionAccIndex = primitive.attributes.find( "POSITION" )->second;
    int normalAccIndex = primitive.attributes.find( "NORMAL" )->second;
    int tangentAccIndex = primitive.attributes.find( "TANGENT" )->second;
    int uvAccIndex = primitive.attributes.find( "TEXCOORD_0" )->second;

    auto &positionAccessor = m_model.accessors[positionAccIndex];
    auto &normalAccessor = m_model.accessors[normalAccIndex];
    auto &tangentAccessor = m_model.accessors[tangentAccIndex];
    auto &uvAccessor = m_model.accessors[uvAccIndex];

    assert( positionAccessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT );
    assert( positionAccessor.type == TINYGLTF_TYPE_VEC3 );
    assert( normalAccessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT );
    assert( normalAccessor.type == TINYGLTF_TYPE_VEC3 );
    assert( tangentAccessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT );
    assert( tangentAccessor.type == TINYGLTF_TYPE_VEC4 );
    assert( uvAccessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT );
    assert( uvAccessor.type == TINYGLTF_TYPE_VEC2 );

    size_t nrVertices = positionAccessor.count;
    std::vector<RawVertex> vertices;
    vertices.resize( nrVertices );

    const tinygltf::BufferView &positionView = m_model.bufferViews[positionAccessor.bufferView];
    const tinygltf::Buffer &positionBuffer = m_model.buffers[positionView.buffer];
    size_t positionBufferOffset = positionAccessor.byteOffset + positionView.byteOffset;
    const glm::vec3 * positionData = reinterpret_cast<const glm::vec3 *>(
                                         &( positionBuffer.data[positionBufferOffset] ) );

    const tinygltf::BufferView &normalView = m_model.bufferViews[normalAccessor.bufferView];
    const tinygltf::Buffer &normalBuffer = m_model.buffers[normalView.buffer];
    size_t normalBufferOffset = normalAccessor.byteOffset + normalView.byteOffset;
    const glm::vec3 *normalData = reinterpret_cast<const glm::vec3 *>(
                                      & ( normalBuffer.data[normalBufferOffset] ) );

    const tinygltf::BufferView &tangentView = m_model.bufferViews[tangentAccessor.bufferView];
    const tinygltf::Buffer &tangentBuffer = m_model.buffers[tangentView.buffer];
    size_t tangentBufferOffset = tangentAccessor.byteOffset + tangentView.byteOffset;
    const glm::vec4 *tangentData = reinterpret_cast<const glm::vec4 *>(
                                       & ( tangentBuffer.data[tangentBufferOffset] ) );

    const tinygltf::BufferView &uvView = m_model.bufferViews[uvAccessor.bufferView];
    const tinygltf::Buffer &uvBuffer =  m_model.buffers[uvView.buffer];
    size_t uvBufferOffset = uvAccessor.byteOffset + uvView.byteOffset;
    const glm::vec2 *uvData =  reinterpret_cast<const glm::vec2 *>(
                                   & ( uvBuffer.data[uvBufferOffset] ) );

    for( size_t i = 0; i < nrVertices; ++i )
    {
        vertices[i].position = positionData[i];
        vertices[i].normal = normalData[i];
        vertices[i].tangent = tangentData[i];
        vertices[i].uv = uvData[i];

        vertices[i].position = parentTransform * glm::vec4( vertices[i].position, 1.0 );
    }

    /********************************************/
    int indexAccIndex = primitive.indices;
    auto &indexAccessor = m_model.accessors[indexAccIndex];

    assertmsg(indexAccessor.componentType == TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT, "uint16 indices expected");

    size_t nrIndices = indexAccessor.count;
    std::vector<uint16_t> indices;
    indices.resize( nrIndices );

    const tinygltf::BufferView &indexView = m_model.bufferViews[indexAccessor.bufferView];
    const tinygltf::Buffer &indexBuffer = m_model.buffers[indexView.buffer];
    size_t indexBufferOffset = indexAccessor.byteOffset + indexView.byteOffset;
    const uint16_t *indexData = reinterpret_cast<const uint16_t *>(
                                    &( indexBuffer.data[indexBufferOffset] ) );

    for( size_t i = 0; i < nrIndices; ++i )
    {
        indices[i] = indexData[i];
    }

    /********************************************/

    glm::mat4 localTransform = glm::scale( glm::mat4( 1.0f ), glm::vec3( 1.0f ) );

    /********************************************/
    int materialIndex = primitive.material;
    loadMaterial( materialIndex );
    m_pRenderModel->addPrimitive( localTransform,
                                  vertices,
                                  indices,
                                  m_albedoMap,
                                  m_normalMap,
                                  m_ambientOcclusionMap,
                                  m_emissiveMap);

}

/**************************************************************************************************/
void ModelLoader::loadMaterial( int materialIndex )
{
    if( materialIndex < 0 || materialIndex >= ( int )m_model.materials.size() )
    {
        return;
    }

    tinygltf::Material &material = m_model.materials[materialIndex];

    if( material.pbrMetallicRoughness.baseColorTexture.index >= 0 )
    {
        int albedoTextureIndex = material.pbrMetallicRoughness.baseColorTexture.index;
        int imageIndex = m_model.textures[albedoTextureIndex].source;
        tinygltf::Image &image = m_model.images[imageIndex];


        /********************************************/
        m_albedoMap.nrcols = image.width;
        m_albedoMap.nrrows = image.height;
        m_albedoMap.rawData = image.image.data();
    }
    else
    {
        m_albedoMap.nrcols = 1;
        m_albedoMap.nrrows = 1;
        m_albedoMap.rawData = defaultAlbedoMap;
    }

    if( material.pbrMetallicRoughness.metallicRoughnessTexture.index >= 0 )
    {
        //TODO
    }

    if( material.normalTexture.index >= 0 )
    {
        int textureIndex = material.normalTexture.index;
        int imageIndex = m_model.textures[textureIndex].source;
        tinygltf::Image &image = m_model.images[imageIndex];

        m_normalMap.nrcols = image.width;
        m_normalMap.nrrows = image.height;
        m_normalMap.rawData = image.image.data();
    }
    else
    {
        m_normalMap.nrcols = 1;
        m_normalMap.nrrows = 1;
        m_normalMap.rawData = defaultNormalMap;
    }

    if( material.occlusionTexture.index >= 0 )
    {
        int textureIndex = material.occlusionTexture.index;
        int imageIndex = m_model.textures[textureIndex].source;
        tinygltf::Image &image = m_model.images[imageIndex];

        /********************************************/
        m_ambientOcclusionMap.nrcols = image.width;
        m_ambientOcclusionMap.nrrows = image.height;
        m_ambientOcclusionMap.rawData = image.image.data();
    }
    else
    {
        m_ambientOcclusionMap.nrcols = 1;
        m_ambientOcclusionMap.nrrows = 1;
        m_ambientOcclusionMap.rawData = defaultAmbientOcclusionMap;
    }

    if( material.emissiveTexture.index >= 0)
    {
        //TODO
        m_emissiveMap.nrcols = 1;
        m_emissiveMap.nrrows = 1;
        m_emissiveMap.rawData = defaultEmissiveMap;
    }
    else
    {
        m_emissiveMap.nrcols = 1;
        m_emissiveMap.nrrows = 1;
        m_emissiveMap.rawData = defaultEmissiveMap;
    }
}
