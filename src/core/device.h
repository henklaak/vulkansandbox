#pragma once
#include <vulkan/vulkan.hpp>
#include <string>
#include <vector>
#include <iostream>
#include "instance.h"
#include "physicaldevice.h"

class Device;
typedef std::shared_ptr<Device> DevicePtr;

class Device
{
public:
    Device( InstancePtr pInstance,
            PhysicalDevicePtr pPhysicalDevice );
    virtual ~Device();

    VkDevice device() const
    {
        return m_device;
    }

    VkPhysicalDevice physicalDevice() const
    {
        return m_pPhysicalDevice->physicalDevice();
    }

    VkQueue graphicsQueue() const
    {
        return m_graphicsQueue;
    }

    VkQueue presentQueue() const
    {
        return m_presentQueue;
    }

    VkCommandPool commandPool() const
    {
        return m_commandPool;
    }

    uint32_t findMemoryType( uint32_t typeFilter, VkMemoryPropertyFlags properties ) const
    {
        return m_pPhysicalDevice->findMemoryType( typeFilter, properties );
    }

    VkPhysicalDeviceRayTracingPipelinePropertiesKHR rtPipelineProperties() const
    {
        return m_pPhysicalDevice->rtPipelineProperties();
    }

    PhysicalDevice::QueueFamilyIndices queueFamilyIndices() const
    {
        return m_pPhysicalDevice->queueFamilyIndices();
    }

    VkFormat findDepthFormat() const
    {
        return m_pPhysicalDevice->findDepthFormat();
    }

    VkSampleCountFlagBits getMaxMsaa() const;

    std::vector<char> readFile( const std::string& filename );
    VkShaderModule createShaderModule( const std::vector<char>& code );

    VkCommandBuffer beginSingleTimeCommands();
    void endSingleTimeCommands( VkCommandBuffer commandBuffer );

private:
    InstancePtr m_pInstance;
    PhysicalDevicePtr m_pPhysicalDevice;

    VkDevice m_device = VK_NULL_HANDLE;
    VkCommandPool m_commandPool = VK_NULL_HANDLE;
    VkQueue m_graphicsQueue = VK_NULL_HANDLE;
    VkQueue m_presentQueue = VK_NULL_HANDLE;
};
