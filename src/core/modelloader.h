#pragma once
#include <memory>
#include <tiny_gltf.h>
#include "rendermodel.h"

class ModelLoader
{
public:
    ModelLoader( const std::string &filename );
    virtual ~ModelLoader();

    void load( RenderModelPtr renderModel );
private:
    void loadTriangle( RenderModelPtr pRenderModel );
    void loadModel( RenderModelPtr pRenderModel );
    void loadScene( int sceneIndex );
    void loadNode( int nodeIndex, const glm::mat4 &parentTransform );
    void loadMesh( int meshIndex, const glm::mat4 &parentTransform );
    void loadPrimitive( const tinygltf::Primitive &primitive, const glm::mat4 &parentTransform );
    void loadMaterial( int materialIndex );
    void loadCamera( int cameraIndex, const glm::mat4 &parentTransform );
    void loadLight( int lightIndex, const glm::mat4 &parentTransform );


    std::string m_filename;
    tinygltf::Model m_model{};
    RenderModelPtr m_pRenderModel;

    RawTexture m_albedoMap{};
    RawTexture m_normalMap{};
    RawTexture m_ambientOcclusionMap{};
    RawTexture m_emissiveMap{};
};
