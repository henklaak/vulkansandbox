#pragma once
#include <memory>
#include <vulkan/vulkan.hpp>
#include "device.h"

class Buffer;
typedef std::shared_ptr<Buffer> BufferPtr;


class Buffer
{
public:
    Buffer( DevicePtr pDevice );
    virtual ~Buffer();

    void *map() const;
    void unmap() const;

    static BufferPtr create(
        DevicePtr pDevice,
        const uint8_t *initialData,
        VkDeviceSize size,
        VkBufferUsageFlags usage,
        VkMemoryPropertyFlags properties );

    uint64_t getDeviceAddress() const;

    VkDeviceSize size = 0;
    VkBuffer buffer = VK_NULL_HANDLE;
    VkDeviceMemory memory = VK_NULL_HANDLE;
    VkDescriptorBufferInfo descriptor{};

private:
    void copyBuffer( VkBuffer srcBuffer,
                     VkBuffer dstBuffer,
                     VkDeviceSize size );

    DevicePtr m_pDevice;
};


