#include "camera.h"
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

/**************************************************************************************************/
Camera::Camera()
{
}

/**************************************************************************************************/
Camera::~Camera()
{
}

/**************************************************************************************************/
void Camera::rotate( const float angleX, const float angleY )
{
    m_angleX = angleX;
    m_angleY = angleY;

    const glm::mat4 I = glm::mat4( 1.0f );

    m_view = glm::rotate( m_view,
                          glm::radians( -m_angleX ),
                          glm::vec3( 0.0f, 1.0f, 0.0f ) );
}

/**************************************************************************************************/
void Camera::setView( const glm::mat4 &view )
{
    m_view = view;
}
/**************************************************************************************************/
void Camera::setProjection( const glm::mat4 &projection )
{
    m_projection = projection;
}
/**************************************************************************************************/
void Camera::update( float dt )
{
}
