#include "vrloader.h"
#include <cassert>
#include <iostream>
#include <unistd.h>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include <openvr/openvr.h>
#include "rendermodel.h"

/**************************************************************************************************/
VrLoader::VrLoader()
{
}

/**************************************************************************************************/
VrLoader::~VrLoader()
{
}

/**************************************************************************************************/
void VrLoader::load( RenderModelPtr pRenderModel )
{
    vr::HmdError error = vr::VRInitError_None;

    vr::IVRSystem *system = nullptr;
    system = vr::VR_Init( &error, vr::VRApplication_Utility );
    assert( system );

    vr::IVRRenderModels *iRenderModels = nullptr;
    iRenderModels = vr::VRRenderModels();
    assert( iRenderModels );

    uint32_t count = iRenderModels->GetRenderModelCount();
    for( uint32_t modelIndex = 0; modelIndex < count; ++modelIndex )
    {
        char renderModelName[256];
        iRenderModels->GetRenderModelName( modelIndex, renderModelName, 256 );

        // 5 generic_hmd
        // 12 lh_basestation_valve_gen2
        // 13 lh_basestation_vive
        // 35 vr_controller_vive_1_5
        if( modelIndex != 5 )
        {
            continue;
        }


        vr::RenderModel_t *renderModel = nullptr;

        vr::EVRRenderModelError res = vr::VRRenderModelError_None;

        while( true )
        {
            res = iRenderModels->LoadRenderModel_Async( renderModelName, &renderModel );
            if( res == vr::VRRenderModelError_Loading )
            {
                usleep( 100 );
                continue;
            }
            break;
        }

        if( res == vr::VRRenderModelError_None )
        {
            std::vector<RawVertex> vertices;
            std::vector<uint16_t> indices;

            for( uint32_t i = 0; i < renderModel->unTriangleCount; ++i )
            {
                for( size_t j = 0; j < 3; ++j )
                {
                    auto a = renderModel->rIndexData[i * 3 + j];
                    indices.push_back( a );
                }
            }
            for( uint32_t i = 0; i < renderModel->unVertexCount; ++i )
            {
                auto vtx = renderModel->rVertexData[i];

                RawVertex vertex;

                vertex.position.x = vtx.vPosition.v[0];
                vertex.position.y = vtx.vPosition.v[1];
                vertex.position.z = vtx.vPosition.v[2];
                vertex.normal.x = vtx.vNormal.v[0];
                vertex.normal.y = vtx.vNormal.v[1];
                vertex.normal.z = vtx.vNormal.v[2];
                vertex.tangent.x = 0.0f;
                vertex.tangent.y = 1.0f;
                vertex.tangent.z = 0.0f;
                vertex.tangent.w = 1.0f;
                vertex.uv.x = vtx.rfTextureCoord[0];
                vertex.uv.y = vtx.rfTextureCoord[1];

                vertices.push_back( vertex );
            }

            RawTexture albedoMap{};

            vr::RenderModel_TextureMap_t *textureMap = nullptr;
            auto textureId = renderModel->diffuseTextureId;
            if( textureId >= 0 )
            {
                while( true )
                {

                    res = iRenderModels->LoadTexture_Async( textureId, &textureMap );
                    if( res == vr::VRRenderModelError_Loading )
                    {
                        usleep( 100 );
                        continue;
                    }
                    break;
                }

                if( res == vr::VRRenderModelError_None )
                {
                    assert( textureMap->format == vr::VRRenderModelTextureFormat_RGBA8_SRGB );
                    albedoMap.rawData = textureMap->rubTextureMapData;
                    albedoMap.nrcols = textureMap->unWidth;
                    albedoMap.nrrows = textureMap->unHeight;
                }
            }

            // TRS convention
            glm::mat4 I( 1.0 );
            glm::mat4 localMatrix = glm::scale(
                                        glm::rotate(
                                            glm::translate(
                                                I,
                                                glm::vec3( 2.0f, 1.5f, 2.0f ) ),
                                            glm::radians( 180.0f ),
                                            glm::vec3( 0.0f, 1.0f, 0.0f ) ),
                                        glm::vec3( 10.0f )
                                    );
            RawTexture normalMap{};
            normalMap.rawData = defaultNormalMap;

            RawTexture ambientOcclusionMap{};
            ambientOcclusionMap.rawData = defaultAmbientOcclusionMap;

            RawTexture emissiveMap{};
            emissiveMap.rawData = defaultEmissiveMap;

            pRenderModel->addPrimitive( localMatrix,
                                        vertices,
                                        indices,
                                        albedoMap,
                                        normalMap,
                                        ambientOcclusionMap,
                                        emissiveMap );

            iRenderModels->FreeTexture( textureMap );
        }
    }
}
