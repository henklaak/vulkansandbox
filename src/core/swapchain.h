#pragma once
#include <vulkan/vulkan.hpp>
#include <optional>
#include <vector>
#include "device.h"
#include "surface.h"
#include "image.h"


class Swapchain;
typedef std::shared_ptr<Swapchain> SwapchainPtr;

class Swapchain
{
public:
    Swapchain( DevicePtr pDevice,
               SurfacePtr pSurface );
    ~Swapchain();

    uint32_t acquireNext();
    void present( uint32_t frameNr );

    size_t nrImages() const
    {
        return m_swapchainImages.size();
    }

    uint32_t width() const
    {
        return m_swapchainExtent.width;
    }

    uint32_t height() const
    {
        return m_swapchainExtent.height;
    }

    VkFormat swapchainImageFormat{};
    VkSemaphore imageAvailableSemaphore = VK_NULL_HANDLE;
    VkSemaphore renderFinishedSemaphore = VK_NULL_HANDLE;


    std::vector<ImagePtr> m_swapchainImages;

private:
    VkSwapchainKHR swapchain = VK_NULL_HANDLE;

    VkSurfaceFormatKHR chooseSwapSurfaceFormat(
        const std::vector<VkSurfaceFormatKHR> &availableFormats );
    VkPresentModeKHR chooseSwapPresentMode(
        const std::vector<VkPresentModeKHR> &availablePresentModes );
    VkExtent2D chooseSwapExtent(
        const VkSurfaceCapabilitiesKHR& capabilities );

    struct SwapChainSupportDetails
    {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    };
    SwapChainSupportDetails querySwapChainSupport();

    DevicePtr m_pDevice;
    SurfacePtr m_pSurface;

    VkExtent2D m_swapchainExtent{};

    bool m_vsync = false;
};
