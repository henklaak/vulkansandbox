#pragma once
#include <memory>
#include "rendermodel.h"

class VrLoader
{
public:
    VrLoader();
    virtual ~VrLoader();

    void load( RenderModelPtr pRenderModel );
};
