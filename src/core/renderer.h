#pragma once
#include <memory>
#include "device.h"
#include "swapchain.h"

class Renderer
{
public:
    Renderer( DevicePtr device,
              SwapchainPtr swapchain );
    virtual ~Renderer() = 0;

    virtual void render( uint32_t frameNr ) = 0;

protected:
    DevicePtr m_pDevice;
    SwapchainPtr m_pSwapchain;
};
